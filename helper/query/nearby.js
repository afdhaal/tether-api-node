const Activity = require("../../modules/activity/models/activity");

exports.nearby = async params => {
  console.log(params);
  //   if (params.lat == 0 || params.lat == "" || params.lat === "undefined") {
  //     params.lat == -6.1753871;
  //   }
  //   if (params.long == 0 || params.long == "" || params.long === "undefined") {
  //     params.lat == 106.8249641;
  //   }
  return Activity.sequelize.query(
    `
    SELECT
	*
    FROM
        (
        SELECT
            *,
            round( CAST(float8 (
                3959 * acos(
                    cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                )
            ) as numeric)*1.60934, 2)
            AS distance
        FROM
            activities
        ) al
    ORDER BY
        distance
        LIMIT ${params.limit};
  `
  );
};
