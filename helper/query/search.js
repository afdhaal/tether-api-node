const Activity = require("../../modules/activity/models/activity");
const Users = require("../../modules/auth/models/users");
const ActivityImage = require("../../modules/activity/models/activity_image");
const ActivityTag = require("../../modules/activity/models/activity_tag");
const ActivityComment = require("../../modules/activity/models/activity_comment");
const ActivityDiscussion = require("../../modules/activity/models/activity_discussion");
const ActivityFav = require("../../modules/activity/models/activity_favorites");
const Category = require("../../modules/activity/models/category");
const sequelize = require("sequelize");
var moment = require("moment");
const Op = require("sequelize").Op;

exports.search_by_interest = async (params) => {
  console.log(params);

  let now = moment().format("YYYY-MM-DD HH:mm:ss");
  let order = "activity_id DESC";
  if (params.sort == "near") {
    order = "distance ASC, activity_id DESC";
  }
  if (params.sort == "like") {
    order = "favorite DESC, activity_id DESC";
  }
  if (params.sort == "comment") {
    order = "count_comment DESC, activity_id DESC";
  }
  if (params.sort == "view") {
    order = "view DESC, activity_id DESC";
  }
  if (params.sort == "lastest") {
    order = "activity_id DESC, activity_id DESC";
  }

  return await Activity.sequelize.query(`
       SELECT
        al.*,  districts.city_name, users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            where al.date >= '${now}'
            or (al.title = '%${params.keyword}%' OR al.content = '%${params.keyword}%')
            and al.category_id IN (${params.category_id})
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
};

exports.search_by_tag = async (params) => {
  console.log(params);

  let now = moment().format("YYYY-MM-DD HH:mm:ss");
  let order = "activity_id DESC";
  if (params.sort == "near") {
    order = "distance ASC, activity_id DESC";
  }
  if (params.sort == "like") {
    order = "favorite DESC, activity_id DESC";
  }
  if (params.sort == "comment") {
    order = "count_comment DESC, activity_id DESC";
  }
  if (params.sort == "view") {
    order = "view DESC, activity_id DESC";
  }
  if (params.sort == "lastest") {
    order = "activity_id DESC, activity_id DESC";
  }

  return await Activity.sequelize.query(`
       SELECT
        al.*,  districts.city_name, users.username as author_name, users.avatar, category as category_name,
        COALESCE(activity_tags.tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            join activity_tags as activetag on activetag.activity_id=al.activity_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            where al.date >= '${now}'
            and activetag.tag = '${params.tag}' and al.active = true
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
};
exports.search_activity = async (params) => {
  Users.hasMany(ActivityComment, { foreignKey: "user_id" });
  Activity.belongsTo(Users, {
    foreignKey: "author_id",
  });
  if (
    params.keyword != "" &&
    params.category_id != "" &&
    params.city_id != ""
  ) {
    // console.log(params.city_id);
    // throw "jos";
    return await Activity.findAll({
      include: [
        {
          model: Users,
          required: true,
          attributes: {
            exclude: [
              "password",
              "email",
              "phone",
              "token_fcm",
              "uid_phone",
              "token_app",
            ],
          },
        },
      ],
      where: {
        [Op.or]: [
          {
            title: {
              [Op.like]: `%${params.keyword}%`,
            },
          },
          {
            content: {
              [Op.like]: `%${params.keyword}%`,
            },
          },
        ],
        category_id: params.category_id,
        city_id: {
          [Op.in]: [params.city_id],
        },
      },
      order: [["activity_id", "DESC"]],
      offset: parseInt(params.page) * 10,
      limit: 10,
    });
  } else if (
    params.keyword != "" &&
    params.category_id == "" &&
    params.city_id == ""
  ) {
    return await Activity.findAll({
      include: [
        {
          model: Users,
          required: true,
          attributes: {
            exclude: [
              "password",
              "email",
              "phone",
              "token_fcm",
              "uid_phone",
              "token_app",
            ],
          },
        },
      ],
      where: {
        [Op.or]: [
          {
            title: {
              [Op.like]: `%${params.keyword}%`,
            },
          },
          {
            content: {
              [Op.like]: `%${params.keyword}%`,
            },
          },
        ],
      },
      order: [["activity_id", "DESC"]],
      offset: parseInt(params.page) * 10,
      limit: 10,
    });
  } else if (
    params.keyword != "" &&
    params.category_id != "" &&
    params.city_id == ""
  ) {
    return await Activity.findAll({
      include: [
        {
          model: Users,
          required: true,
          attributes: {
            exclude: [
              "password",
              "email",
              "phone",
              "token_fcm",
              "uid_phone",
              "token_app",
            ],
          },
        },
      ],
      where: {
        [Op.or]: [
          {
            title: {
              [Op.like]: `%${params.keyword}%`,
            },
          },
          {
            content: {
              [Op.like]: `%${params.keyword}%`,
            },
          },
        ],
        category_id: params.category_id,
      },
      order: [["activity_id", "DESC"]],
      offset: parseInt(params.page) * 10,
      limit: 10,
    });
  } else if (
    params.keyword != "" &&
    params.category_id == "" &&
    params.city_id != ""
  ) {
    return await Activity.findAll({
      include: [
        {
          model: Users,
          required: true,
          attributes: {
            exclude: [
              "password",
              "email",
              "phone",
              "token_fcm",
              "uid_phone",
              "token_app",
            ],
          },
        },
      ],
      where: {
        [Op.or]: [
          {
            title: {
              [Op.like]: `%${params.keyword}%`,
            },
          },
          {
            content: {
              [Op.like]: `%${params.keyword}%`,
            },
          },
        ],
        city_id: params.city_id,
      },
      order: [["activity_id", "DESC"]],
      offset: parseInt(params.page) * 10,
      limit: 10,
    });
  }
};

exports.search_activity_raw = async (params) => {
  console.log(params);
  Users.hasMany(ActivityComment, { foreignKey: "user_id" });
  Activity.belongsTo(Users, {
    foreignKey: "author_id",
  });
  if (params.filter == "trending") {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";
    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*, ( SELECT false )
        AS is_joined,
        ( SELECT false )
        AS is_favorited,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view + al.share
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
        ORDER BY
            count_all DESC,distance ASC
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  } else if (
    params.keyword != "" &&
    params.category_id != "" &&
    params.city_id != ""
  ) {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";
    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*, districts.city_name, ( SELECT false )
        AS is_joined,
        ( SELECT false )
        AS is_favorited,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
            and (al.title like '%${params.keyword}%' OR al.content like'%${params.keyword}%')
            and al.category_id IN ${params.category_id}
            and al.city_id IN ${params.city_id}
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  } else if (
    params.keyword != "" &&
    params.category_id == "" &&
    params.city_id == ""
  ) {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";
    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*, districts.city_name, ( SELECT false ) 
        AS is_joined,
        ( SELECT false ) 
        AS is_requested,
        ( SELECT false ) 
        AS is_favorited,
        ( SELECT 0 ) 
        AS count_request_join,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
            and (al.title like '%${params.keyword}%' OR al.content like'%${params.keyword}%')
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  } else if (
    params.keyword != "" &&
    params.category_id != "" &&
    params.city_id == ""
  ) {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";
    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*,districts.city_name, ( SELECT false ) 
        AS is_joined,
        ( SELECT false ) 
        AS is_requested,
        ( SELECT false ) 
        AS is_favorited,
        ( SELECT 0 ) 
        AS count_request_join,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
            and (al.title like '%${params.keyword}%' OR al.content like'%${params.keyword}%')
            and al.category_id IN ${params.category_id}
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  } else if (
    params.keyword != "" &&
    params.category_id == "" &&
    params.city_id != ""
  ) {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";
    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*, districts.city_name, ( SELECT false ) 
        AS is_joined,
        ( SELECT false ) 
        AS is_requested,
        ( SELECT false ) 
        AS is_favorited,
        ( SELECT 0 ) 
        AS count_request_join,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
            and (al.title like '%${params.keyword}%' OR al.content like'%${params.keyword}%')
            and al.city_id IN ${params.city_id}
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  } else if (
    params.keyword == "" &&
    params.category_id == "" &&
    params.city_id != ""
  ) {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";
    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*,  districts.city_name, ( SELECT false ) 
        AS is_joined,
        ( SELECT false ) 
        AS is_requested,
        ( SELECT false ) 
        AS is_favorited,
        ( SELECT 0 ) 
        AS count_request_join,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
            and al.city_id IN ${params.city_id}
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  } else if (
    params.keyword == "" &&
    params.category_id != "" &&
    params.city_id == ""
  ) {
    console.log("cek");

    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";

    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*,  districts.city_name, ( SELECT false ) 
        AS is_joined,
        ( SELECT false ) 
        AS is_requested,
        ( SELECT false ) 
        AS is_favorited,
        ( SELECT 0 ) 
        AS count_request_join,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
            and al.category_id IN ${params.category_id}
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  } else if (
    params.keyword == "" &&
    params.category_id != "" &&
    params.city_id != ""
  ) {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";
    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*, districts.city_name, ( SELECT false ) 
        AS is_joined,
        ( SELECT false ) 
        AS is_requested,
        ( SELECT false ) 
        AS is_favorited,
        ( SELECT 0 ) 
        AS count_request_join,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
            and al.category_id IN (${params.category_id})
            and al.city_id IN (${params.city_id})
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  } else {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let order = "activity_id DESC";
    if (params.filter == "near") {
      order = "distance ASC";
    }
    if (params.filter == "like") {
      order = "favorite DESC";
    }
    if (params.filter == "comment") {
      order = "count_comment DESC";
    }
    if (params.filter == "view") {
      order = "view DESC";
    }

    if (params.sort == "lastest") {
      order = order + ", activity_id DESC";
    }
    if (params.sort == "upcoming") {
      order = order + ", date ASC";
    }

    return await Activity.sequelize.query(`
       SELECT
        al.*, districts.city_name, ( SELECT false ) 
        AS is_joined,
        ( SELECT false ) 
        AS is_requested,
        ( SELECT false ) 
        AS is_favorited,
        ( SELECT 0 ) 
        AS count_request_join,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.date >= '${now}'
        ORDER BY
            ${order}
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
  }
};

exports.search_activity_raw_login = async (params) => {
  console.log(params);
  Users.hasMany(ActivityComment, { foreignKey: "user_id" });
  Activity.belongsTo(Users, {
    foreignKey: "author_id",
  });
  let filter = params.filter;
  let arr = filter;
  let obj = arr.reduce((ac, a) => ({ ...ac, [a]: "" }), {});

  let city_where = "";
  let keyword_where = "";
  let category_where = "";
  let order = "";
  if (params.keyword != 0 || params.keyword != "" || params.keyword != null) {
    keyword_where = `and LOWER(activities.title) like '%${params.keyword}%' `;
  }
  if (params.city_id.length > 0) {
    city_where = `and activities.city_id IN (${params.city_id})`;
  }
  if (params.category_id.length > 0) {
    category_where = `and activities.category_id IN (${params.category_id})`;
  }

  if (typeof obj.online === "undefined") {
    // console.log("yeah");
    if (typeof obj.trending != "undefined") {
      if (typeof obj.near != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof params.filter == "lastest") {
        order = `ORDER BY
          distance ASC, activity_id DESC, count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof obj.like != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          count_all DESC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.near != "undefined") {
      if (typeof obj.like) {
        order = `ORDER BY
          distance ASC, favorite DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          distance ASC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.like != "undefined") {
      order = `ORDER BY
          favorite DESC
          OFFSET ${params.page}*10`;
    } else if (typeof obj.share != "undefined") {
      order = `ORDER BY
          share DESC
          OFFSET ${params.page}*10`;
    } else {
      if (typeof params.filter == "lastest") {
        order = `ORDER BY
          activity_id DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          date ASC
          OFFSET ${params.page}*10`;
      }
    }
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    return await Activity.sequelize.query(`
        SELECT
        activities.*,
        districts.city_name,
        (SELECT calculate_distance(${params.long}, ${params.lat}, activities.long, activities.lat, 'K')) as distance,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id AND activity_joins.user_id = ${params.user_id} ) > 0 THEN
        TRUE
          WHEN activities.author_id = ${params.user_id} THEN
        TRUE ELSE FALSE
        END AS is_joined,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activity_join_approvals.activity_id = activities.activity_id AND activity_join_approvals.user_id = ${params.user_id} ) > 0 THEN
        TRUE
          WHEN activities.author_id = ${params.user_id} THEN
        TRUE ELSE FALSE
        END AS is_requested,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id AND activity_favorites.user_id = ${params.user_id} ) > 0 THEN
        TRUE ELSE FALSE
        END AS is_favorited,
        ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activities.activity_id = activity_join_approvals.activity_id ) AS count_request_join,
        users.username AS author_name,
        users.avatar,
        category AS category_name,
        COALESCE ( tag, '[]' ) AS tag,
        COALESCE ( activity_images.image, '[]' ) AS image,
        COALESCE ( list_user_join.username, '[]' ) AS list_user_join,
        ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) AS favorite,
        ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id ) AS user_joined,
        ( SELECT COUNT ( * ) FROM activity_comments WHERE activity_comments.activity_id = activities.activity_id ) AS count_comment,
        (
          ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + activities.VIEW 
        ) AS count_all
      FROM
        activities
        JOIN users ON users.user_id = activities.author_id
        LEFT JOIN districts ON districts.city_id = activities.city_id
        JOIN categories ON categories.category_id = activities.category_id
        LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'tag', activity_tags.tag ) ) AS tag FROM activity_tags WHERE activity_tags.activity_id = activities.activity_id ) activity_tags
        ON TRUE LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) activity_images
        ON TRUE LEFT JOIN LATERAL (
        SELECT
          json_agg ( json_build_object ( 'user_id', users.user_id, 'username', users.username, 'avatar', users.avatar ) ) AS username 
        FROM
          activity_joins
          JOIN users ON users.user_id = activity_joins.user_id
        WHERE
          activity_joins.activity_id = activities.activity_id
        ) list_user_join ON TRUE
      WHERE
        activities.DATE >= '${now}' and activities.active = true
        ${keyword_where}
        ${city_where}
        ${category_where}
        ${order}
      LIMIT 10;
      `);
  } else if (typeof obj.online) {
    // console.log("yeah");
    if (typeof obj.trending != "undefined") {
      if (typeof obj.near != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof params.filter == "lastest") {
        order = `ORDER BY
          distance ASC, activity_id DESC, count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof obj.like != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          count_all DESC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.near != "undefined") {
      if (typeof obj.like) {
        order = `ORDER BY
          distance ASC, favorite DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          distance ASC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.like != "undefined") {
      order = `ORDER BY
          favorite DESC
          OFFSET ${params.page}*10`;
    } else if (typeof obj.share != "undefined") {
      order = `ORDER BY
          share DESC
          OFFSET ${params.page}*10`;
    } else {
      if (typeof params.filter == "lastest") {
        order = `ORDER BY
          activity_id DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          date ASC
          OFFSET ${params.page}*10`;
      }
    }
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    return await Activity.sequelize.query(`
        SELECT
        activities.*,
        districts.city_name,
        (SELECT calculate_distance(${params.long}, ${params.lat}, activities.long, activities.lat, 'K')) as distance,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id AND activity_joins.user_id = ${params.user_id} ) > 0 THEN
        TRUE
          WHEN activities.author_id = ${params.user_id} THEN
        TRUE ELSE FALSE
        END AS is_joined,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activity_join_approvals.activity_id = activities.activity_id AND activity_join_approvals.user_id = ${params.user_id} ) > 0 THEN
        TRUE
          WHEN activities.author_id = ${params.user_id} THEN
        TRUE ELSE FALSE
        END AS is_requested,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id AND activity_favorites.user_id = ${params.user_id} ) > 0 THEN
        TRUE ELSE FALSE
        END AS is_favorited,
        ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activities.activity_id = activity_join_approvals.activity_id ) AS count_request_join,
        users.username AS author_name,
        users.avatar,
        category AS category_name,
        COALESCE ( tag, '[]' ) AS tag,
        COALESCE ( activity_images.image, '[]' ) AS image,
        COALESCE ( list_user_join.username, '[]' ) AS list_user_join,
        ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) AS favorite,
        ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id ) AS user_joined,
        ( SELECT COUNT ( * ) FROM activity_comments WHERE activity_comments.activity_id = activities.activity_id ) AS count_comment,
        (
          ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + activities.VIEW 
        ) AS count_all
      FROM
        activities
        JOIN users ON users.user_id = activities.author_id
        LEFT JOIN districts ON districts.city_id = activities.city_id
        JOIN categories ON categories.category_id = activities.category_id
        LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'tag', activity_tags.tag ) ) AS tag FROM activity_tags WHERE activity_tags.activity_id = activities.activity_id ) activity_tags
        ON TRUE LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) activity_images
        ON TRUE LEFT JOIN LATERAL (
        SELECT
          json_agg ( json_build_object ( 'user_id', users.user_id, 'username', users.username, 'avatar', users.avatar ) ) AS username 
        FROM
          activity_joins
          JOIN users ON users.user_id = activity_joins.user_id
        WHERE
          activity_joins.activity_id = activities.activity_id
        ) list_user_join ON TRUE
      WHERE
        activities.DATE >= '${now}' and activities.active = true
        and activities.online = true
        ${keyword_where}
        ${city_where}
        ${category_where}
        ${order}
      LIMIT 10;
      `);
  } else if (typeof obj.offline) {
    // console.log("yeah");
    if (typeof obj.trending != "undefined") {
      if (typeof obj.near != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof params.filter == "lastest") {
        order = `ORDER BY
          distance ASC, activity_id DESC, count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof obj.like != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          count_all DESC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.near != "undefined") {
      if (typeof obj.like) {
        order = `ORDER BY
          distance ASC, favorite DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          distance ASC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.like != "undefined") {
      order = `ORDER BY
          favorite DESC
          OFFSET ${params.page}*10`;
    } else if (typeof obj.share != "undefined") {
      order = `ORDER BY
          share DESC
          OFFSET ${params.page}*10`;
    } else {
      if (typeof params.filter == "lastest") {
        order = `ORDER BY
          activity_id DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          date ASC
          OFFSET ${params.page}*10`;
      }
    }
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    return await Activity.sequelize.query(`
        SELECT
        activities.*,
        districts.city_name,
        (SELECT calculate_distance(${params.long}, ${params.lat}, activities.long, activities.lat, 'K')) as distance,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id AND activity_joins.user_id = ${params.user_id} ) > 0 THEN
        TRUE
          WHEN activities.author_id = ${params.user_id} THEN
        TRUE ELSE FALSE
        END AS is_joined,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activity_join_approvals.activity_id = activities.activity_id AND activity_join_approvals.user_id = ${params.user_id} ) > 0 THEN
        TRUE
          WHEN activities.author_id = ${params.user_id} THEN
        TRUE ELSE FALSE
        END AS is_requested,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id AND activity_favorites.user_id = ${params.user_id} ) > 0 THEN
        TRUE ELSE FALSE
        END AS is_favorited,
        ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activities.activity_id = activity_join_approvals.activity_id ) AS count_request_join,
        users.username AS author_name,
        users.avatar,
        category AS category_name,
        COALESCE ( tag, '[]' ) AS tag,
        COALESCE ( activity_images.image, '[]' ) AS image,
        COALESCE ( list_user_join.username, '[]' ) AS list_user_join,
        ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) AS favorite,
        ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id ) AS user_joined,
        ( SELECT COUNT ( * ) FROM activity_comments WHERE activity_comments.activity_id = activities.activity_id ) AS count_comment,
        (
          ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + activities.VIEW 
        ) AS count_all
      FROM
        activities
        JOIN users ON users.user_id = activities.author_id
        LEFT JOIN districts ON districts.city_id = activities.city_id
        JOIN categories ON categories.category_id = activities.category_id
        LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'tag', activity_tags.tag ) ) AS tag FROM activity_tags WHERE activity_tags.activity_id = activities.activity_id ) activity_tags
        ON TRUE LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) activity_images
        ON TRUE LEFT JOIN LATERAL (
        SELECT
          json_agg ( json_build_object ( 'user_id', users.user_id, 'username', users.username, 'avatar', users.avatar ) ) AS username 
        FROM
          activity_joins
          JOIN users ON users.user_id = activity_joins.user_id
        WHERE
          activity_joins.activity_id = activities.activity_id
        ) list_user_join ON TRUE
      WHERE
        activities.DATE >= '${now}' and activities.active = true
        and activities.online = false
        ${keyword_where}
        ${city_where}
        ${category_where}
        ${order}
      LIMIT 10;
      `);
  } else {
    throw "jos yeah";
  }
  throw "jos";
  // else if (params.filter == "trending") {
  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";
  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  //         WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  //         WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //       COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //       ORDER BY
  //           count_all DESC,distance ASC
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // } else if (
  //   params.keyword != "" &&
  //   params.category_id != "" &&
  //   params.city_id != ""
  // ) {
  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";
  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  // 				WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //        COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id}) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //           and (al.title like '%${params.keyword}%' OR al.content like'%${params.keyword}%')
  //           and al.category_id IN (${params.category_id})
  //           and al.city_id IN (${params.city_id})
  //       ORDER BY
  //           ${order}
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // } else if (
  //   params.keyword != "" &&
  //   params.category_id == "" &&
  //   params.city_id == ""
  // ) {
  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";
  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  // 				WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //        COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id}) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //           and (al.title like '%${params.keyword}%' OR al.content like'%${params.keyword}%')
  //       ORDER BY
  //           ${order}
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // } else if (
  //   params.keyword != "" &&
  //   params.category_id != "" &&
  //   params.city_id == ""
  // ) {
  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";
  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  // 				WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //       COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id}) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //           and (al.title like '%${params.keyword}%' OR al.content like'%${params.keyword}%')
  //           and al.category_id IN (${params.category_id})
  //       ORDER BY
  //           ${order}
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // } else if (
  //   params.keyword != "" &&
  //   params.category_id == "" &&
  //   params.city_id != ""
  // ) {
  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";
  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  // 				WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //       COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id}) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //           and (al.title like '%${params.keyword}%' OR al.content like'%${params.keyword}%')
  //           and al.city_id IN (${params.city_id})
  //       ORDER BY
  //           ${order}
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // } else if (
  //   params.keyword == "" &&
  //   params.category_id == "" &&
  //   params.city_id != ""
  // ) {
  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";
  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  // 				WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //       COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id}) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //           and al.city_id IN (${params.city_id})
  //       ORDER BY
  //           ${order}
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // } else if (
  //   params.keyword == "" &&
  //   params.category_id != "" &&
  //   params.city_id == ""
  // ) {
  //   console.log("cek");

  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";

  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  // 				WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //        COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id}) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //           and al.category_id IN (${params.category_id})
  //       ORDER BY
  //           ${order}
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // } else if (
  //   params.keyword == "" &&
  //   params.category_id != "" &&
  //   params.city_id != ""
  // ) {
  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";
  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  // 				WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //       COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id}) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //           and al.category_id IN (${params.category_id})
  //           and al.city_id IN (${params.city_id})
  //       ORDER BY
  //           ${order}
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // } else {
  //   let now = moment().format("YYYY-MM-DD HH:mm:ss");
  //   let order = "activity_id DESC";
  //   if (params.filter == "near") {
  //     order = "distance ASC";
  //   }
  //   if (params.filter == "like") {
  //     order = "favorite DESC";
  //   }
  //   if (params.filter == "comment") {
  //     order = "count_comment DESC";
  //   }
  //   if (params.filter == "view") {
  //     order = "view DESC";
  //   }

  //   if (params.sort == "lastest") {
  //     order = order + ", activity_id DESC";
  //   }
  //   if (params.sort == "upcoming") {
  //     order = order + ", date ASC";
  //   }

  //   return await Activity.sequelize.query(`
  //      SELECT
  //       al.*, districts.city_name, CASE
  // 				WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_joined,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_join_approvals where activity_join_approvals.activity_id=al.activity_id and activity_join_approvals.user_id=${params.user_id})>0 THEN true
  //         WHEN al.author_id=${params.user_id} THEN true
  // 				ELSE false
  // 			END
  //       AS is_requested,
  //       CASE
  // 				WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id})>0 THEN true
  // 				ELSE false
  // 			END
  //       AS is_favorited,
  //       (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join,
  //       users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
  //       COALESCE( activity_images.image, '[]' ) as image,
  //        COALESCE( list_user_join.username, '[]' ) as list_user_join,
  //       (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
  //       (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
  //       (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
  //       (
  // 			(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
  // 			(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${params.user_id}) +
  // 			al.view
  // 			) as count_all
  //       FROM
  //           (
  //           SELECT
  //               *,
  //               round( CAST(float8 (
  //                   3959 * acos(
  //                       cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) )
  //                   )
  //               ) as numeric)*1.60934, 2)
  //               AS distance
  //           FROM
  //               activities
  //           ) al
  //           join users on users.user_id=al.author_id
  //           join districts on districts.city_id = al.city_id
  //           join categories on categories.category_id=al.category_id
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
  //               FROM   activity_tags
  //               WHERE  activity_tags.activity_id = al.activity_id
  //           ) activity_tags ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object('image', activity_images.image)) AS image
  //               FROM   activity_images
  //               WHERE  activity_images.activity_id = al.activity_id
  //           ) activity_images ON true
  //           LEFT   JOIN LATERAL (
  //               SELECT json_agg(json_build_object(
  //                 'user_id', users.user_id,
  //                 'username', users.username,
  //                 'avatar', users.avatar
  //               )) AS username
  //               FROM   activity_joins
  //               JOIN users on users.user_id=activity_joins.user_id
  //               WHERE  activity_joins.activity_id = al.activity_id
  //           ) list_user_join ON true
  //           where al.date >= '${now}'
  //       ORDER BY
  //           ${order}
  //           OFFSET ${params.page}*10
  //           LIMIT 10;
  //   `);
  // }
};

exports.search_activity_raw_nologin = async (params) => {
  console.log(params);
  // throw "a";
  Users.hasMany(ActivityComment, { foreignKey: "user_id" });
  Activity.belongsTo(Users, {
    foreignKey: "author_id",
  });
  let filter = params.filter;
  let arr = filter;
  let obj = arr.reduce((ac, a) => ({ ...ac, [a]: "" }), {});

  let city_where = "";
  let keyword_where = "";
  let category_where = "";
  let order = "";
  if (params.keyword != 0 || params.keyword != "" || params.keyword != null) {
    keyword_where = `and LOWER(activities.title) like '%${params.keyword}%' `;
  }
  if (params.city_id.length > 0) {
    city_where = `and activities.city_id IN (${params.city_id})`;
  }
  if (params.category_id.length > 0) {
    category_where = `and activities.category_id IN (${params.category_id})`;
  }

  // console.log(category_where);

  if (typeof obj.online === "undefined") {
    // console.log("yeah");
    if (typeof obj.trending != "undefined") {
      if (typeof obj.near != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof params.filter == "lastest") {
        order = `ORDER BY
          distance ASC, activity_id DESC, count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof obj.like != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          count_all DESC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.near != "undefined") {
      if (typeof obj.like) {
        order = `ORDER BY
          distance ASC, favorite DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          distance ASC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.like != "undefined") {
      order = `ORDER BY
          favorite DESC
          OFFSET ${params.page}*10`;
    } else if (typeof obj.share != "undefined") {
      order = `ORDER BY
          share DESC
          OFFSET ${params.page}*10`;
    } else {
      if (typeof params.filter == "lastest") {
        order = `ORDER BY
          activity_id DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          date ASC
          OFFSET ${params.page}*10`;
      }
    }
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    return await Activity.sequelize.query(`
        SELECT
        activities.*,
        districts.city_name,
        (SELECT calculate_distance(${params.long}, ${params.lat}, activities.long, activities.lat, 'K')) as distance,
        ( SELECT false )
        AS is_joined,
        ( SELECT false )
        AS is_requested,
        ( SELECT false )
        AS is_favorited,
        ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activities.activity_id = activity_join_approvals.activity_id ) AS count_request_join,
        users.username AS author_name,
        users.avatar,
        category AS category_name,
        COALESCE ( tag, '[]' ) AS tag,
        COALESCE ( activity_images.image, '[]' ) AS image,
        COALESCE ( list_user_join.username, '[]' ) AS list_user_join,
        ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) AS favorite,
        ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id ) AS user_joined,
        ( SELECT COUNT ( * ) FROM activity_comments WHERE activity_comments.activity_id = activities.activity_id ) AS count_comment,
        (
          ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + activities.VIEW 
        ) AS count_all
      FROM
        activities
        JOIN users ON users.user_id = activities.author_id
        LEFT JOIN districts ON districts.city_id = activities.city_id
        JOIN categories ON categories.category_id = activities.category_id
        LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'tag', activity_tags.tag ) ) AS tag FROM activity_tags WHERE activity_tags.activity_id = activities.activity_id ) activity_tags
        ON TRUE LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) activity_images
        ON TRUE LEFT JOIN LATERAL (
        SELECT
          json_agg ( json_build_object ( 'user_id', users.user_id, 'username', users.username, 'avatar', users.avatar ) ) AS username 
        FROM
          activity_joins
          JOIN users ON users.user_id = activity_joins.user_id
        WHERE
          activity_joins.activity_id = activities.activity_id
        ) list_user_join ON TRUE
      WHERE
        activities.DATE >= '${now}' and activities.active = true
        ${keyword_where}
        ${city_where}
        ${category_where}
        ${order}
      LIMIT 10;
      `);
  } else if (typeof obj.online) {
    // console.log("yeah");
    if (typeof obj.trending != "undefined") {
      if (typeof obj.near != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof params.filter == "lastest") {
        order = `ORDER BY
          distance ASC, activity_id DESC, count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof obj.like != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          count_all DESC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.near != "undefined") {
      if (typeof obj.like) {
        order = `ORDER BY
          distance ASC, favorite DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          distance ASC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.like != "undefined") {
      order = `ORDER BY
          favorite DESC
          OFFSET ${params.page}*10`;
    } else if (typeof obj.share != "undefined") {
      order = `ORDER BY
          share DESC
          OFFSET ${params.page}*10`;
    } else {
      if (typeof params.filter == "lastest") {
        order = `ORDER BY
          activity_id DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          date ASC
          OFFSET ${params.page}*10`;
      }
    }
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    return await Activity.sequelize.query(`
        SELECT
        activities.*,
        districts.city_name,
        (SELECT calculate_distance(${params.long}, ${params.lat}, activities.long, activities.lat, 'K')) as distance,
        ( SELECT false )
        AS is_joined,
        ( SELECT false )
        AS is_requested,
        ( SELECT false )
        AS is_favorited,
        ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activities.activity_id = activity_join_approvals.activity_id ) AS count_request_join,
        users.username AS author_name,
        users.avatar,
        category AS category_name,
        COALESCE ( tag, '[]' ) AS tag,
        COALESCE ( activity_images.image, '[]' ) AS image,
        COALESCE ( list_user_join.username, '[]' ) AS list_user_join,
        ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) AS favorite,
        ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id ) AS user_joined,
        ( SELECT COUNT ( * ) FROM activity_comments WHERE activity_comments.activity_id = activities.activity_id ) AS count_comment,
        (
          ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + activities.VIEW 
        ) AS count_all
      FROM
        activities
        JOIN users ON users.user_id = activities.author_id
        LEFT JOIN districts ON districts.city_id = activities.city_id
        JOIN categories ON categories.category_id = activities.category_id
        LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'tag', activity_tags.tag ) ) AS tag FROM activity_tags WHERE activity_tags.activity_id = activities.activity_id ) activity_tags
        ON TRUE LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) activity_images
        ON TRUE LEFT JOIN LATERAL (
        SELECT
          json_agg ( json_build_object ( 'user_id', users.user_id, 'username', users.username, 'avatar', users.avatar ) ) AS username 
        FROM
          activity_joins
          JOIN users ON users.user_id = activity_joins.user_id
        WHERE
          activity_joins.activity_id = activities.activity_id
        ) list_user_join ON TRUE
      WHERE
        activities.DATE >= '${now}' and activities.active = true
        and activities.online = true
        ${keyword_where}
        ${city_where}
        ${category_where}
        ${order}
      LIMIT 10;
      `);
  } else if (typeof obj.offline) {
    // console.log("yeah");
    if (typeof obj.trending != "undefined") {
      if (typeof obj.near != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof params.filter == "lastest") {
        order = `ORDER BY
          distance ASC, activity_id DESC, count_all DESC
          OFFSET ${params.page}*10`;
      } else if (typeof obj.like != "undefined") {
        order = `ORDER BY
          distance ASC,count_all DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          count_all DESC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.near != "undefined") {
      if (typeof obj.like) {
        order = `ORDER BY
          distance ASC, favorite DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          distance ASC
          OFFSET ${params.page}*10`;
      }
    } else if (typeof obj.like != "undefined") {
      order = `ORDER BY
          favorite DESC
          OFFSET ${params.page}*10`;
    } else if (typeof obj.share != "undefined") {
      order = `ORDER BY
          share DESC
          OFFSET ${params.page}*10`;
    } else {
      if (typeof params.filter == "lastest") {
        order = `ORDER BY
          activity_id DESC
          OFFSET ${params.page}*10`;
      } else {
        order = `ORDER BY
          date ASC
          OFFSET ${params.page}*10`;
      }
    }
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    return await Activity.sequelize.query(`
        SELECT
        activities.*,
        districts.city_name,
        (SELECT calculate_distance(${params.long}, ${params.lat}, activities.long, activities.lat, 'K')) as distance,
        ( SELECT false )
        AS is_joined,
        ( SELECT false )
        AS is_requested,
        ( SELECT false )
        AS is_favorited,
        ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activities.activity_id = activity_join_approvals.activity_id ) AS count_request_join,
        users.username AS author_name,
        users.avatar,
        category AS category_name,
        COALESCE ( tag, '[]' ) AS tag,
        COALESCE ( activity_images.image, '[]' ) AS image,
        COALESCE ( list_user_join.username, '[]' ) AS list_user_join,
        ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) AS favorite,
        ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id ) AS user_joined,
        ( SELECT COUNT ( * ) FROM activity_comments WHERE activity_comments.activity_id = activities.activity_id ) AS count_comment,
        (
          ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + activities.VIEW 
        ) AS count_all
      FROM
        activities
        JOIN users ON users.user_id = activities.author_id
        LEFT JOIN districts ON districts.city_id = activities.city_id
        JOIN categories ON categories.category_id = activities.category_id
        LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'tag', activity_tags.tag ) ) AS tag FROM activity_tags WHERE activity_tags.activity_id = activities.activity_id ) activity_tags
        ON TRUE LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) activity_images
        ON TRUE LEFT JOIN LATERAL (
        SELECT
          json_agg ( json_build_object ( 'user_id', users.user_id, 'username', users.username, 'avatar', users.avatar ) ) AS username 
        FROM
          activity_joins
          JOIN users ON users.user_id = activity_joins.user_id
        WHERE
          activity_joins.activity_id = activities.activity_id
        ) list_user_join ON TRUE
      WHERE
        activities.DATE >= '${now}' and activities.active = true
        and activities.online = false
        ${keyword_where}
        ${city_where}
        ${category_where}
        ${order}
      LIMIT 10;
      `);
  } else {
    throw "jos yeah";
  }
};
