const Activity = require("../../modules/activity/models/activity");
const Users = require("../../modules/auth/models/users");
const ActivityImage = require("../../modules/activity/models/activity_image");
const ActivityTag = require("../../modules/activity/models/activity_tag");
const ActivityComment = require("../../modules/activity/models/activity_comment");
const ActivityDiscussion = require("../../modules/activity/models/activity_discussion");
const ActivityFav = require("../../modules/activity/models/activity_favorites");
const Category = require("../../modules/activity/models/category");
const sequelize = require("sequelize");
var moment = require("moment");
const Op = require("sequelize").Op;

exports.search_by_interest = async params => {
  console.log(params);

  let now = moment().format("YYYY-MM-DD HH:mm:ss");
  let order = "activity_id DESC";
  if (params.sort == "near") {
    order = "distance ASC, activity_id DESC";
  }
  if (params.sort == "like") {
    order = "favorite DESC, activity_id DESC";
  }
  if (params.sort == "comment") {
    order = "count_comment DESC, activity_id DESC";
  }
  if (params.sort == "view") {
    order = "view DESC, activity_id DESC";
  }
  if (params.sort == "lastest") {
    order = "activity_id DESC";
  }

  return await Activity.sequelize.query(`
       SELECT
        al.*, users.username as author_name, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${params.lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${params.long} ) ) + sin( radians( ${params.lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
						LEFT JOIN activity_joins on activity_joins.activity_id=al.activity_id
            where al.active = true and  ( author_id=${params.user_id} or activity_joins.user_id=${params.user_id} )
        ORDER BY
            activity_id DESC
            OFFSET ${params.page}*10
            LIMIT 10;
    `);
};
