module.exports = {
  send: function(token, tittle, messagenotif, image = "") {
    var FCM = require("fcm-node");
    var serverKey =
      "AAAAB1y_orI:APA91bGZ6i0YCCeHxeMyfnM5Bz9i6-_3ZVjDHhjkyQ8aL-IO6rqFjXd6D54BiR7iIOhjsphSMwR5Wcatek92EAo5-dv2fIe-T863VH1Ss138aAY_0XcV0QSm_SwN_cZsm_UQA1aNQTlH"; //put your server key here
    var fcm = new FCM(serverKey);
    var message = {
      to: token,
      collapse_key: "jastipku",
      notification: {
        title: tittle,
        body: messagenotif,
        sound: "default",
        image: image
      },
      priority: "high",
      show_in_foreground: true,
      content_available: true,
      data: {
        my_key: "jastipku",
        my_another_key: "notification",
        sound: "default",
        image: image
      }
    };
    fcm.send(message, function(err, response) {
      if (err) {
        console.log(err);

        console.log("Something has gone wrong!");
      } else {
        console.log("Successfully sent with response: ", response);
      }
    });
  }
};
