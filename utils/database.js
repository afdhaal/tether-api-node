require("pg").defaults.parseInt8 = true;
const winston = require("winston");
const Sequelize = require("sequelize");
const timezone = "Asia/Jakarta";
require("moment").tz.setDefault(timezone);
console.log(process.env.HOST_DB);

const sequelize = new Sequelize(
  process.env.DB_NAME,
  process.env.USER_DB,
  process.env.PASS_DB,
  {
    host: process.env.HOST_DB,
    dialect: "postgres",
    dialectOptions: {
      useUTC: false
    },
    timezone: "+07:00"
    // logging: false
    // logging: winston.debug
  }
);

module.exports = sequelize;
