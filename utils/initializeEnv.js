const path = require("path");

var nodeEnv = process.env.NODE_ENV;
if (nodeEnv === "dev") {
  var envPath = path.resolve(".env.dev");
} else if (nodeEnv === "local") {
  var envPath = path.resolve(".env.local");
} else if (nodeEnv === "production") {
  var envPath = path.resolve(".env.prod");
}
//console.log(nodeEnv);
require("dotenv").config({ path: envPath });
