const nodemailer = require("nodemailer");
const moment = require("moment");

module.exports = {
  mailer_activity: async (email, activity, user) => {
    var transporter = nodemailer.createTransport({
      host: "smtp.zoho.com",
      port: 465,
      secure: true, // use SSL
      auth: {
        user: "no-reply@tether.co.id",
        pass: "kuranggreget@321",
      },
    });

    // setup e-mail data, even with unicode symbols
    var mailOptions = {
      from: '"Tether.co.id " <no-reply@tether.co.id>', // sender address (who sends)
      to: email, // list of receivers (who receives)
      subject: "Join activity : " + activity.title, // Subject line
      text: "Join activity", // plaintext body
      html: `<!DOCTYPE html>
            <html>
            <head>
            <meta charset="utf-8">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
            </head>
            <body style="font-size: 14px;">
            <p>Halo ${user.username},
            </p>
            <p>Terima kasih telah bergabung pada activity <b>${
              activity.title
            } di aplikasi Tether.</b>
            </p>
            <br>
            <p><b>Detail activity kamu adalah :</b></p>
            <p>Nama activity : <b>${activity.title}</b></p>
            <p>Tanggal: ${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("DD-MM-YYYY")}</p>
            <p>Jam: <b>${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("HH:mm:ss")}<b></p>
            <p>Lokasi: <b>${activity.address}<b></p>
            <br>
            <p>Dimohon untuk hadir tepat waktu pada activity tersebut, dan selalu lihat catatan terbaru dari author.
            </p>
            <p>Untuk informasi lebih lanjut kamu bisa hubungi author activity atau chat group pada menu diskusi.</p>
            <br>
            <p>Terima kasih</p>
            <hr>
            <p><b>Tether | Social Activity</b></p>
            <p>Jl. Amarta No.1 Seturan, Depok, Yogyakarta – Indonesia 88251</p>
            <p>P: 081228888263 | E: info@tether.co.id | W: www.tether.co.id</p>
            </br>
            <p>Follow our social media</p>
            <p>IG: @tether.id</p>
            <p>FB: tether.co.id</p>
        </body>
        </html>`, // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return console.log(error);
      }

      console.log("Message sent: " + info.response);
    });
  },
  mailer_activity_waiting: async (email, activity, user) => {
    var transporter = nodemailer.createTransport({
      host: "smtp.zoho.com",
      port: 465,
      secure: true, // use SSL
      auth: {
        user: "no-reply@tether.co.id",
        pass: "kuranggreget@321",
      },
    });

    // setup e-mail data, even with unicode symbols
    var mailOptions = {
      from: '"Tether.co.id " <no-reply@tether.co.id>', // sender address (who sends)
      to: email, // list of receivers (who receives)
      subject: "Request join activity : " + activity.title, // Subject line
      text: "Request join activity", // plaintext body
      html: `<!DOCTYPE html>
            <html>
            <head>
            <meta charset="utf-8">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
            </head>
            <body>
            <p>Halo ${user.username},
            </p>
            <p>Terima kasih telah request bergabung pada activity <b>${
              activity.title
            } di aplikasi Tether.</br>
            </p>
            <br>
            <p><b>Detail activity kamu adalah :</b></p>
            <p>Nama activity : <b>${activity.title}</b></p>
            <p>Tanggal: ${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("DD-MM-YYYY")}</p>
            <p>Jam: <b>${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("HH:mm:ss")}<b></p>
            <p>Lokasi: <b>${activity.address}<b></p>
            <br>
            <p>Silahkan menunggu persetujuan dari author.
            </p>
            <p>Terima kasih</p>
            <hr>
            <p><b>Tether | Social Activity</b></p>
            <p>Jl. Amarta No.1 Seturan, Depok, Yogyakarta – Indonesia 88251</p>
            <p>P: 081228888263 | E: info@tether.co.id | W: www.tether.co.id</p>
           <br>
            <p>Follow our social media</p>
            <p>IG: @tether.id</p>
            <p>FB: tether.co.id</p>
        </body>
        </html>`, // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return console.log(error);
      }

      console.log("Message sent: " + info.response);
    });
  },
  mailer_activity_leave: async (email, activity, user) => {
    var transporter = nodemailer.createTransport({
      host: "smtp.zoho.com",
      port: 465,
      secure: true, // use SSL
      auth: {
        user: "no-reply@tether.co.id",
        pass: "kuranggreget@321",
      },
    });

    // setup e-mail data, even with unicode symbols
    var mailOptions = {
      from: '"Tether.co.id " <no-reply@tether.co.id>', // sender address (who sends)
      to: email, // list of receivers (who receives)
      subject: "Keluar activity : " + activity.title, // Subject line
      text: "Keluar activity", // plaintext body
      html: `<!DOCTYPE html>
            <html>
            <head>
            <meta charset="utf-8">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
            </head>
            <body>
            <p>Halo ${user.username},
            </p>
            <p>Anda telah keluar dari activity <b>${
              activity.title
            } di aplikasi Tether.</br>
            </p>
            <br>
            <p><b>Detail activity kamu adalah :</b></p>
            <p>Nama activity : <b>${activity.title}</b></p>
            <p>Tanggal: ${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("DD-MM-YYYY")}</p>
            <p>Jam: <b>${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("HH:mm:ss")}<b></p>
            <p>Lokasi: <b>${activity.address}<b></p>
            <br>
            <p>Terima kasih</p>
            <hr>
            <p><b>Tether | Social Activity</b></p>
            <p>Jl. Amarta No.1 Seturan, Depok, Yogyakarta – Indonesia 88251</p>
            <p>P: 081228888263 | E: info@tether.co.id | W: www.tether.co.id</p>
            </br>
            <p>Follow our social media</p>
            <p>IG: @tether.id</p>
            <p>FB: tether.co.id</p>
        </body>
        </html>`, // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return console.log(error);
      }

      console.log("Message sent: " + info.response);
    });
  },
  approved_mailer: async (email, activity, user) => {
    var transporter = nodemailer.createTransport({
      host: "smtp.zoho.com",
      port: 465,
      secure: true, // use SSL
      auth: {
        user: "no-reply@tether.co.id",
        pass: "kuranggreget@321",
      },
    });

    // setup e-mail data, even with unicode symbols
    var mailOptions = {
      from: '"Tether.co.id " <no-reply@tether.co.id>', // sender address (who sends)
      to: email, // list of receivers (who receives)
      subject: "Join Activity : " + activity.title + " Disetujui", // Subject line
      text: "Join Activity Disetujui", // plaintext body
      html: `<!DOCTYPE html>
            <html>
            <head>
            <meta charset="utf-8">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
            </head>
            <body>
            <p>Halo ${user.username},
            </p>
            <p>Terima kasih telah bergabung pada activity <b>${
              activity.title
            }</b> di aplikasi Tether.
            </p>
            <br>
            <p><b>Detail activity kamu adalah :</b></p>
            </br>
            <p>Nama activity : <b>${activity.title}</b></p>
            <p>Tanggal: ${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("DD-MM-YYYY")}</p>
            <p>Jam: <b>${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("HH:mm:ss")}<b></p>
            <p>Lokasi: <b>${activity.address}<b></p>
            <p>Dimohon untuk hadir tepat waktu pada activity tersebut, dan selalu lihat catatan terbaru dari author.
            </p>
            <p>Untuk informasi lebih lanjut kamu bisa hubungi author activity atau chat group pada menu diskusi.</p>
            <br>
            <p>Terima kasih</p>
            <hr>
            <p><b>Tether | Social Activity</b></p>
            <p>Jl. Amarta No.1 Seturan, Depok, Yogyakarta – Indonesia 88251</p>
            <p>P: 081228888263 | E: info@tether.co.id | W: www.tether.co.id</p>
            </br>
            <p>Follow our social media</p>
            <p>IG: @tether.id</p>
            <p>FB: tether.co.id</p>
        </body>
        </html>`, // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return console.log(error);
      }

      console.log("Message sent: " + info.response);
    });
  },
  create_note: async (user, user_joined, activity, note) => {
    // console.log(`${user_joined.dataValues.email}`);
    // throw user_joined;
    var transporter = nodemailer.createTransport({
      host: "smtp.zoho.com",
      port: 465,
      secure: true, // use SSL
      auth: {
        user: "no-reply@tether.co.id",
        pass: "kuranggreget@321",
      },
    });

    // setup e-mail data, even with unicode symbols
    var mailOptions = {
      from: '"Tether.co.id" <no-reply@tether.co.id>', // sender address (who sends)
      to: `${user_joined.dataValues.email}`, // list of receivers (who receives)
      subject: "Catatan baru di activity : " + activity.title, // Subject line
      html: `<!DOCTYPE html>
            <html>
            <head>
            <meta charset="utf-8">
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
            </head>
            <body>
            <p>Halo ${user_joined.dataValues.username},
            </p>
            <p>
                Author <b>${
                  user.username
                }</b> baru saja mengupdate informasi catatan/notes.
            <p></p>
            </br>
            <p><b>Detail catatan :</b></p>
            <p>${note}</p>
            <p>Catatan tersebut terkait dengan activity = <b>${
              activity.title
            } di aplikasi Tether.</b></p>
            <hr>
            <p>Nama activity : <b>${activity.title}</b></p>
            <p>Tanggal: ${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("DD-MM-YYYY")}</p>
            <p>Jam: <b>${moment
              .tz(activity.date, "Asia/Jakarta")
              .format("HH:mm:ss")}<b></p>
            <p>Lokasi: <b>${activity.address}<b></p>
            <p>Dimohon untuk hadir tepat waktu pada activity tersebut, dan selalu lihat catatan terbaru dari author.
            <p>Untuk informasi lebih lanjut kamu bisa hubungi author activity atau chat group pada menu diskusi.</p>
            <br>
            <p>Terima kasih</p><hr>
            <p><b>Tether | Social Activity</b></p>
            <p><b>Jl. Amarta No.1 Seturan, Depok, Yogyakarta – Indonesia 88251</b></p>
            <p><b>P: 081228888263 | E: info@tether.co.id | W: www.tether.co.id</b></p>
            <br>
            <p><b>Follow our social media</b></p>
            <p><b>IG: @tether.id</b></p>
            <p><b>FB: tether.co.id</b></p>
        </body>
        </html>`, // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        return console.log(error);
      }

      console.log("Message sent: " + info.response);
    });
  },
};
// async..await is not allowed in global scope, must use a wrapper
async function main() {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport

  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

main().catch(console.error);
