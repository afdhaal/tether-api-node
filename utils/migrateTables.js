const sequelize = require("./database");

const user = require("../modules/auth/models/users");
const user_rating = require("../modules/auth/models/user_rating");
const user_follow = require("../modules/auth/models/user_follow");
const user_interest = require("../modules/auth/models/user_interest");
const activity = require("../modules/activity/models/activity");
const activity_image = require("../modules/activity/models/activity_image");
const activity_share = require("../modules/activity/models/activity_share");
const activity_favorite = require("../modules/activity/models/activity_favorites");
const activity_comment = require("../modules/activity/models/activity_comment");
const activity_discussion = require("../modules/activity/models/activity_discussion");
const activity_invitation = require("../modules/activity/models/activity_invitation");
const tag = require("../modules/activity/models/activity_tag");
const activity_join = require("../modules/activity/models/activity_join");
const activity_join_approval = require("../modules/activity/models/activity_join_approval");
const activity_join_blacklist = require("../modules/activity/models/activity_join_blacklist");
const activity_note = require("../modules/activity/models/activity_note");
// const event = require("../modules/event/models/event_image");
// const event_image = require("../modules/event/models/event");
const category = require("../modules/activity/models/category");
const location = require("../modules/locations/models/district");
const checker_version = require("../modules/auth/models/checker_version");
const maintenance = require("../modules/auth/models/maintenance");
const notification = require("../modules/notifications/models/notification");
const type_notification = require("../modules/notifications/models/type_notification");
const error_report = require("../modules/report/models/error_report");
const activity_report = require("../modules/report/models/report_activity");
const user_report = require("../modules/report/models/report_user");

//sync model sequelize
//aktifkan kalau ingin sinkron table baru atau alter table }

// sequelize
//   .sync({ alter: true })
//   .then((result) => {
//     //console.log(result);
//   })
//   .catch((err) => {
//     console.log(err);
//   });
