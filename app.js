const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const morgan = require("morgan");
const firebase = require("firebase-admin");
const helmet = require("helmet");
const fcm = require("./utils/fcmSend");
var cors = require("cors");
const path = require("path");
const admin = require("firebase-admin");
const winston = require("winston");
const bearerToken = require("express-bearer-token");
const compression = require("compression");
const fs = require("fs");

//firebase initialize
// const serviceAccount = require("./utils/firebaseadminsdk.json");
// admin.initializeApp({
//   credential: admin.credential.cert(serviceAccount)
// });
//checker running env
const initializeEnv = require("./utils/initializeEnv");
//initialize table
const initializeModel = require("./utils/migrateTables");
const port = process.env.PORT;
app.use(compression());
app.use(bodyParser.json({ limit: "2000mb" }));
app.use(
  bodyParser.urlencoded({
    limit: "2000mb",
    extended: true,
    parameterLimit: 1000000,
  })
);
app.use(bearerToken());
app.use(morgan("combined"));
app.use(bodyParser.json());
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(helmet());
app.disable("x-powered-by");
app.use(cors());

// routes;
const authRoutes = require("./modules/auth/routes/users");
const activityRoutes = require("./modules/activity/routes/activity");
const locationRoutes = require("./modules/locations/routes/locations");
const homeRoutes = require("./modules/homepage/routes/homepage");
const searchRoutes = require("./modules/search/routes/search");
const reporthRoutes = require("./modules/report/routes/report");
const notificationRoutes = require("./modules/notifications/routes/notification");
const eventRoutes = require("./modules/event/routes/event");
const bannerRoutes = require("./modules/banner/routes/banner");
// const chattingRoutes = require("./modules/chatting/routes/chatting");
// const paymentRoutes = require("./modules/payment/routes/payment");
// const callbackRoutes = require("./modules/callback/routes/callback");
// const TransactionRoutes = require("./modules/transaction/routes/transaction");
// const notificationRoutes = require("./modules/notification/routes/notification");
// const searchRoutes = require("./modules/search/routes/search");
// const adminRoutes = require("./modules/admin/routes/admin");
// const wishlistRoutes = require("./modules/wishlist/routes/wishlist");
// const topupRoutes = require("./modules/topup/routes/topup");
// const withdrawRoutes = require("./modules/withdraw/routes/withdraw");
// const withdrawBankRoutes = require("./modules/bank_withdraw/routes/bank_withdraw");

// //use routes
app.use("/api/users", authRoutes);
app.use("/api/activity", activityRoutes);
app.use("/api/locations", locationRoutes);
app.use("/api/home", homeRoutes);
app.use("/api/search", searchRoutes);
app.use("/api/report", reporthRoutes);
app.use("/api/notification", notificationRoutes);
app.use("/api/events", eventRoutes);
app.use("/api/banner", bannerRoutes);
// app.use("/event", eventRoutes);
// app.use("/news", newsRoutes);
// app.use("/product", productRoutes);
// app.use("/search", searchRoutes);
// app.use("/chat", chattingRoutes);
// app.use("/payment", paymentRoutes);
// app.use("/callback", callbackRoutes);
// app.use("/transaction", TransactionRoutes);
// app.use("/notification", notificationRoutes);
// app.use("/admin", adminRoutes);
// app.use("/wishlist", wishlistRoutes);
// app.use("/topup", topupRoutes);
// app.use("/withdraw", withdrawRoutes);
// app.use("/bankwithdraw", withdrawBankRoutes);

// //static url for image
// app.use("/news", express.static("./assets/news"));
// app.use("/po_image", express.static("./assets/po_image"));
app.use("/activity_image", express.static("./assets/activity_image"));
app.use("/avatar", express.static("./assets/avatar"));
// app.use("/avatar", express.static("./assets/avatar"));
// app.use("/chat", express.static("./assets/chat"));

// var CronJob = require('cron').CronJob;
// new CronJob('1 * * * * *', function() {
//   console.log('You will see this message every second');
// }, null, true, 'America/Los_Angeles');

// fcm.send(
//   "e-pKxefyAHA:APA91bGPbCDrVCAl8gYADijTu4wpgnyZ1DV4i_arg1TL3Gyzug9g35IJJEhN25H0hQVkTYVRzvj1ZTAQ6WaCQ-LGvVVzg5N4xtoxyPdysy-kvj6stgbKpHoPpy8KjKRwU3WiWkizQwPX",
//   "sehat ben, awas corona",
//   "aku pengen mulih"
// );

//const functions = require("firebase-functions");

app.get("/", (req, res) => {
  res.status(400).send({ message: "Nothing :p" });
});
app.use(function (req, res) {
  const response = {
    error: true,
    message: "" + process.env.API_HOST + "" + req.originalUrl + " Not Found",
    data: {},
  };
  res.status(404).send(response);
});

app.listen(port, () => {
  console.log("Jalan-jalan ke = " + port);
});
