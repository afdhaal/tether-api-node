"use strict";
const Users = require("../modules/auth/models/users");
const detailUser = require("../modules/auth/models/detail_user");
const Maintenance = require("../modules/auth/models/maintenance");
const Version = require("../modules/auth/models/checker_version");
const firebaseadmin = require("firebase-admin");
const jwt = require("jsonwebtoken");
const axios = require("axios");

exports.checkBanned = async (req, res, next) => {
  let maintenance = await Maintenance.findAll();
  let check_version = await Version.findAll();
  let status_maintenance = maintenance[0].dataValues.maintenance;
  let version = check_version[0].dataValues.version;
  const token = req.headers.token;
  let timestamp = new Date().getTime();
  const phone = req.body.phone;
  const tokenFirebase = req.body.tokenFirebase;
  //required fullname
  const fullName = req.body.fullName;
  const email = req.body.email;
  const url = "https://graph.accountkit.com/v1.1/me";
  let params = {
    access_token: token,
  };

  // firebaseadmin.auth().verifyIdToken
  if (
    fullName != null &&
    email != null &&
    (phone != null) & (tokenFirebase != null)
  ) {
    console.log("cuk");
    res.locals.status_maintenance = status_maintenance;
    next();
  } else {
    User.findOne({
      where: {
        token: token,
      },
    })
      .then((dataUser) => {
        if (dataUser.statusBanned == true) {
          const response = {
            error: true,
            message: "Account Banned",
            status_maintenance: status_maintenance,
            version: 0,
            data: {},
          };
          res.status(400).send(response);
        } else {
          console.log(dataUser);
          detailUser
            .findOne({
              where: {
                userId: dataUser.id,
              },
            })
            .then((dataDetail) => {
              console.log(dataDetail);
              res.locals.user = dataUser;
              res.locals.detailuser = dataDetail;
              res.locals.status_maintenance = status_maintenance;
              next();
            })
            .catch((err) => {
              console.error(err);
              const response = {
                error: true,
                message: "Token not verified",
                status_maintenance: status_maintenance,
                version: 0,
                data: {},
              };
              res.status(400).send(response);
            });
        }
      })
      .catch((err) => {
        console.error(err);
        const response = {
          error: true,
          message: "Token not verified",
          status_maintenance: status_maintenance,
          version: version,
          data: {},
        };
        res.status(400).send(response);
      });
  }
};
exports.isLogin = async (req, res, next) => {
  let maintenance = await Maintenance.findAll();
  let status_maintenance = maintenance[0].dataValues.maintenance;
  let check_version = await Version.findAll();
  let version = check_version[0].dataValues.version;
  // res.send("Token " + req.token);
  const token = req.token;
  let timestamp = new Date().getTime();
  Users.findOne({
    where: {
      token_app: token,
    },
  })
    .then((dataUser) => {
      console.log(dataUser.status_banned);

      if (dataUser && dataUser.status_banned == false) {
        res.locals.user = dataUser;
        res.locals.status_maintenance = status_maintenance;
        res.locals.version = version;
        next();
        console.log(dataUser);
      } else if (dataUser && dataUser.status_banned == true) {
        const response = {
          error: true,
          message: "Account Banned",
          status_maintenance: status_maintenance,
          version: version,
          data: {},
        };
        res.status(400).send(response);
      } else {
        const response = {
          error: true,
          message: "Token not verified !!",
          status_maintenance: status_maintenance,
          version: version,
          data: {},
        };
        res.status(400).send(response);
      }
    })
    .catch((err) => {
      const response = {
        error: true,
        message: "Token not verified !!",
        status_maintenance: status_maintenance,
        version: version,
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.noLogin = async (req, res, next) => {
  let maintenance = await Maintenance.findAll();
  let status_maintenance = maintenance[0].dataValues.maintenance;
  let check_version = await Version.findAll();
  let version = check_version[0].dataValues.version;
  res.locals.status_maintenance = status_maintenance;
  res.locals.version = version;
  next();
};

exports.check_maintenance = (req, res, next) => {};

exports.isAdmin = (req, res, next) => {
  console.log(req.headers);
  try {
    var decoded = jwt.verify(req.headers.token, process.env.JWT_SECRET);
    next();
  } catch (err) {
    const response = {
      error: true,
      message: "Token not verified !!",
      status_maintenance: status_maintenance,
      version: version,
      data: {},
    };
    res.status(400).send(response);
  }

  // if (req.headers.token == "admin998980olkm0912") {
  //   next();
  // } else {
  // }
};
