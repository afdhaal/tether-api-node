const app = require("express");
const jwt = require("jsonwebtoken");
const base64Img = require("base64-img");
const Users = require("../../auth/models/users");
// const Activity = require("../models/activity");
// const Category = require("../models/category");
// const ActivityImage = require("../models/activity_image");
// const ActivityFav = require("../models/activity_favorites");
// const ActivityShare = require("../models/activity_share");
// const ActivityComment = require("../models/activity_comment");
// const ActivityDiscussion = require("../models/activity_discussion");
// const ActivityInvitation = require("../models/activity_invitation");
// const ActivityJoin = require("../models/activity_join");
// const Tag = require("../models/activity_tag");
// const City = require("../../locations/models/district");
const Notification = require("../../notifications/models/notification");
const TypeNotification = require("../../notifications/models/type_notification");
const responses = require("../../../utils/responses");
const Sequelize = require("sequelize");
const Op = require("sequelize").Op;
var slug = require("slug");
var print = console.log.bind(console, ">");
const fs = require("fs");
const path = require("path");
const fcm = require("../../../utils/fcmSend");
// const mailer = require("../../../helpers/mailer");
// const randomString = require("../../../helpers/genRandomString");
const admin = require("firebase-admin");

exports.homepage = (req, res, next) => {
  let send = {
    error: false,
    message: "events",
    status_maintenance: false,
    version: "1.0.2",
    data: {
      category: [
        {
          category_id: 2,
          category: "Sosial",
          description: "Sosial",
          image: "Sosial",
          createdAt: "2020-02-14T07:59:32.689Z",
          updatedAt: "2020-02-14T07:59:32.689Z",
          count: 1,
        },
        {
          category_id: 4,
          category: "Belajar",
          description: "Belajar",
          image: "Belajar",
          createdAt: "2020-02-27T07:39:01.872Z",
          updatedAt: "2020-02-27T07:39:01.872Z",
          count: 1,
        },
        {
          category_id: 5,
          category: "Games",
          description: "Games",
          image: "Games",
          createdAt: "2020-02-27T07:39:10.743Z",
          updatedAt: "2020-02-27T07:39:10.743Z",
          count: 0,
        },
        {
          category_id: 6,
          category: "Kesenian",
          description: "Kesenian",
          image: "Kesenian",
          createdAt: "2020-02-27T07:39:22.405Z",
          updatedAt: "2020-02-27T07:39:22.405Z",
          count: 0,
        },
        {
          category_id: 7,
          category: "Kuliner",
          description: "Kuliner",
          image: "Kuliner",
          createdAt: "2020-02-27T07:39:47.712Z",
          updatedAt: "2020-02-27T07:39:47.712Z",
          count: 0,
        },
        {
          category_id: 8,
          category: "Otomotif",
          description: "Otomotif",
          image: "Otomotif",
          createdAt: "2020-02-27T07:40:08.186Z",
          updatedAt: "2020-02-27T07:40:08.186Z",
          count: 0,
        },
        {
          category_id: 9,
          category: "Rohani",
          description: "Rohani",
          image: "Rohani",
          createdAt: "2020-02-27T07:40:18.190Z",
          updatedAt: "2020-02-27T07:40:18.190Z",
          count: 0,
        },
        {
          category_id: 1,
          category: "Olahraga",
          description: "Sport",
          image: "Sport",
          createdAt: "2020-02-11T09:22:42.512Z",
          updatedAt: "2020-02-11T09:22:42.512Z",
          count: 0,
        },
        {
          category_id: 10,
          category: "Traveling",
          description: "Traveling",
          image: "Traveling",
          createdAt: "2020-02-27T07:40:26.285Z",
          updatedAt: "2020-02-27T07:40:26.285Z",
          count: 0,
        },
        {
          category_id: 3,
          category: "Belanja",
          description: "Belanja",
          image: "Belanja",
          createdAt: "2020-02-14T07:59:59.734Z",
          updatedAt: "2020-02-14T07:59:59.734Z",
          count: 0,
        },
      ],
      trending: [
        {
          event_id: 57,
          title: "Membersihkan pantai depok (jogja) 1",
          content:
            "Mari kita bersama-sama membersihkan pantai depok jogja yg sudah mulai kotor",
          lat: -8.014049020289818,
          long: 110.29239311814308,
          date: "2020-05-25T07:00:00.000Z",
          category_id: 2,
          public: true,
          slug: "Membersihkan-pantai-depok-jogja-1586873739670",
          createdAt: "2020-04-14T14:15:39.677Z",
          updatedAt: "2020-05-05T09:52:23.897Z",
          city_id: 39,

          note: "",
          view: 77,
          share: 0,
          address:
            "Jl. Pantai Depok, Pantai, Parangtritis, Kec. Kretek, Bantul, Daerah Istimewa Yogyakarta 55772, Indonesia",
          city_name: "Bantul",
          is_joined: false,
          is_favorited: false,
          category_name: "Sosial",
          tag: [],
          image: [
            {
              image:
                "http://devapi.tether.co.id:3132/activity_image/1586873739678.png",
            },
          ],
          favorite: 0,
          user_joined: 6,
          count_comment: 3,
          count_all: 77,
        },
        {
          event_id: 57,
          title: "Membersihkan pantai depok (jogja) 2",
          content:
            "Mari kita bersama-sama membersihkan pantai depok jogja yg sudah mulai kotor",
          lat: -8.014049020289818,
          long: 110.29239311814308,
          date: "2020-05-25T07:00:00.000Z",
          category_id: 2,
          public: true,
          slug: "Membersihkan-pantai-depok-jogja-1586873739670",
          createdAt: "2020-04-14T14:15:39.677Z",
          updatedAt: "2020-05-05T09:52:23.897Z",
          city_id: 39,

          note: "",
          view: 77,
          share: 0,
          address:
            "Jl. Pantai Depok, Pantai, Parangtritis, Kec. Kretek, Bantul, Daerah Istimewa Yogyakarta 55772, Indonesia",
          city_name: "Bantul",
          is_joined: false,
          is_favorited: false,
          category_name: "Sosial",
          tag: [],
          image: [
            {
              image:
                "http://devapi.tether.co.id:3132/activity_image/1586873739678.png",
            },
          ],
          favorite: 0,
          user_joined: 6,
          count_comment: 3,
          count_all: 77,
        },
        {
          event_id: 57,
          title: "Membersihkan pantai depok (jogja) 3",
          content:
            "Mari kita bersama-sama membersihkan pantai depok jogja yg sudah mulai kotor",
          lat: -8.014049020289818,
          long: 110.29239311814308,
          date: "2020-05-25T07:00:00.000Z",
          category_id: 2,
          public: true,
          slug: "Membersihkan-pantai-depok-jogja-1586873739670",
          createdAt: "2020-04-14T14:15:39.677Z",
          updatedAt: "2020-05-05T09:52:23.897Z",
          city_id: 39,

          note: "",
          view: 77,
          share: 0,
          address:
            "Jl. Pantai Depok, Pantai, Parangtritis, Kec. Kretek, Bantul, Daerah Istimewa Yogyakarta 55772, Indonesia",
          city_name: "Bantul",
          is_joined: false,
          is_favorited: false,
          category_name: "Sosial",
          tag: [],
          image: [
            {
              image:
                "http://devapi.tether.co.id:3132/activity_image/1586873739678.png",
            },
          ],
          favorite: 0,
          user_joined: 6,
          count_comment: 3,
          count_all: 77,
        },
      ],
      recommended: [],
      popular_hashtag: [
        {
          count: 1,
          tag: "flutter",
        },
        {
          count: 1,
          tag: "mabar",
        },
        {
          count: 1,
          tag: "melukis",
        },
        {
          count: 1,
          tag: "virtualrun",
        },
        {
          count: 1,
          tag: "kue",
        },
      ],
    },
  };
  console.log("jos");

  res.send(send);
};
