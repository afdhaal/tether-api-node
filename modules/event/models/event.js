const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

const Event = sequelize.define("event", {
  event_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  event: {
    type: Sequelize.STRING,
    allowNull: false
  },
  description: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  lat: {
    type: Sequelize.DOUBLE,
    defaultValue: 0
  },
  long: {
    type: Sequelize.DOUBLE,
    defaultValue: 0
  },
  prize: {
    type: Sequelize.TEXT,
    allowNull: false
  },
  date_from: {
    allowNull: true,
    type: Sequelize.DATE
  },
  date_end: {
    allowNull: true,
    type: Sequelize.DATE
  },
  author_id: {
    type: Sequelize.BIGINT
  },
  category_id: {
    allowNull: false,
    type: Sequelize.BIGINT
  },
  city_id: {
    allowNull: false,
    type: Sequelize.INTEGER
  },
  address: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  slug: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: true
  },
  slot: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  link_register: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  organizer: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  phone: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  email: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  view: {
    type: Sequelize.BIGINT,
    defaultValue: 0
  },
  share: {
    type: Sequelize.BIGINT,
    defaultValue: 0
  },
  active: {
    type: Sequelize.BOOLEAN,
    defaultValue: true
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

// Activity.belongsTo(Users, { foreignKey: "userId" });
// Activity.hasOne(Users, { as: "Initiator" });

// Users.hasMany(Activity, {
//   foreignKey: "author_id",
//   as: "author_id",
//   onDelete: "CASCADE"
// });

module.exports = Event;
