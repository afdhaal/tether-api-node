const express = require("express");
const eventController = require("../controllers/event");
const auth = require("../../../middleware/auth");
const router = express.Router();

//activity based
router.get("/", auth.noLogin, eventController.homepage);

module.exports = router;
