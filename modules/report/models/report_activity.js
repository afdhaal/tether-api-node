const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

const ReportActivity = sequelize.define("activity_report", {
  activity_report_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  message: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = ReportActivity;
