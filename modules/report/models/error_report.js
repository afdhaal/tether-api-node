const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

//Table primary User
const ErrorReport = sequelize.define("error_report", {
  cerror_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  message: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  solve: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = ErrorReport;
