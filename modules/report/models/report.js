const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

const Report = sequelize.define("report", {
  report_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  user_id: {
    type: Sequelize.BIGINT,
  },
  reported_user_id: {
    type: Sequelize.BIGINT,
    defaultValue: 0,
  },
  reported_activity_id: {
    type: Sequelize.BIGINT,
    defaultValue: 0,
  },
  report_id: {
    type: Sequelize.BIGINT,
  },
  type: {
    type: Sequelize.TEXT,
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
});

module.exports = Report;
