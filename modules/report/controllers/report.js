const app = require("express");
var async = require("async");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const ErrorReport = require("../models/error_report");
const ActivityReport = require("../models/report_activity");
const Report = require("../models/report");
const UserReport = require("../models/report_user");
const Sequelize = require("sequelize");

exports.error_report = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let user = res.locals.user;
  const { message } = req.body;
  ErrorReport.create({
    message: message,
  })
    .then((createReport) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        message: "Success send report",
        data: {},
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        message: err,
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.list_report_activity = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  // let user = res.locals.user;
  ActivityReport.findAll({})
    .then((activityReport) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List report activity",
        data: activityReport,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: err,
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.list_user_activity = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  // let user = res.locals.user;
  UserReport.findAll({})
    .then((userReport) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List report user",
        data: userReport,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        message: err,
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.report = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let user = res.locals.user;
  const { type, report_id, reported_user_id, reported_activity_id } = req.body;

  console.log({ type, report_id, reported_user_id, reported_activity_id });

  if (type == "user") {
    Report.findOne({
      where: {
        user_id: user.user_id,
        reported_user_id: reported_user_id,
        reported_activity_id: 0,
        report_id: report_id,
        type: type,
      },
    })
      .then((dataReport) => {
        if (dataReport) {
          const response = {
            error: false,
            status_maintenance: status_maintenance,
            version: version,
            message: "Already send report",
            data: {},
          };
          res.send(response);
        } else {
          Report.create({
            user_id: user.user_id,
            reported_user_id: reported_user_id,
            report_id: report_id,
            type: type,
          })
            .then((createReport) => {
              const response = {
                error: false,
                status_maintenance: status_maintenance,
                version: version,
                message: "Success send report",
                data: {},
              };
              res.send(response);
            })
            .catch((err) => {
              console.log(err);

              const response = {
                error: true,
                message: err,
                data: {},
              };
              res.status(400).send(response);
            });
        }
      })
      .catch((err) => {
        console.log(err);
        const response = {
          error: true,
          message: err,
          data: {},
        };
        res.status(400).send(response);
      });
  } else if (type == "activity") {
    Report.findOne({
      where: {
        user_id: user.user_id,
        reported_activity_id: reported_activity_id,
        report_id: report_id,
        type: type,
      },
    })
      .then((dataReport) => {
        if (dataReport) {
          const response = {
            error: false,
            status_maintenance: status_maintenance,
            version: version,
            message: "Already send report",
            data: {},
          };
          res.send(response);
        } else {
          Report.create({
            user_id: user.user_id,
            reported_activity_id: reported_activity_id,
            reported_user_id: 0,
            report_id: report_id,
            type: type,
          })
            .then((createReport) => {
              const response = {
                error: false,
                status_maintenance: status_maintenance,
                version: version,
                message: "Success send report",
                data: {},
              };
              res.send(response);
            })
            .catch((err) => {
              console.log(err);
              const response = {
                error: true,
                message: err,
                data: {},
              };
              res.status(400).send(response);
            });
        }
      })
      .catch((err) => {
        const response = {
          error: true,
          message: "Parameter type not found",
          data: {},
        };
        res.status(400).send(response);
      });
  } else {
    const response = {
      error: true,
      message: err,
      data: {},
    };
    res.status(400).send(response);
  }
};
