const express = require("express");
const reportControllers = require("../controllers/report");
const auth = require("../../../middleware/auth");
const router = express.Router();

router.post("/error_report", auth.noLogin, reportControllers.error_report);
router.post("/report", auth.isLogin, reportControllers.report);
router.get(
  "/type_report_activity",
  auth.noLogin,
  reportControllers.list_report_activity
);
router.get(
  "/type_report_user",
  auth.noLogin,
  reportControllers.list_user_activity
);

module.exports = router;
