const app = require("express");
var async = require("async");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const UsersInterest = require("../../auth/models/user_interest");
const Users = require("../../auth/models/users");
const searchHelper = require("../../../helper/query/search");
const Sequelize = require("sequelize");

exports.search_activity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;

	const token = req.token;
	if (token) {
		Users.findOne({
			where: {
				token_app: token,
			},
		})
			.then(async dataUser => {
				if (dataUser) {
					let {keyword, category_id, city_id, page, lat, long, sort, filter} = req.query;
					if (lat == 0 || long == 0 || lat === undefined || long === undefined) {
						long = 106.8249641;
						lat = -6.1753871;
					}
					let id_of_new_city_id = [];
					if (city_id != "") {
						let new_city_id = await new Function("return " + city_id + ";")();
						new_city_id.map(item => {
							// return item.city_id;
							id_of_new_city_id.push(item);
						});
					}

					let id_of_new_category_id = [];
					if (category_id != "") {
						let new_category_id = await new Function("return " + category_id + ";")();
						// Object.values(new_city_id.city_id);

						new_category_id.map(item => {
							console.log(item);
							// id_of_new_category_id = item.category_id;
							id_of_new_category_id.push(item);
						});
					}
					// console.log(id_of_new_category_id);
					// console.log(id_of_new_category_id);
					// throw "";

					let list_filter = [];
					if (filter != "") {
						let new_filter = await new Function("return " + filter + ";")();
						// list_filter = new_filter;
						// console.log(Object.values(filter.filter));
						// console.log(new_filter);
						new_filter.map(item => {
							// list_filter = item.filter;
							// console.log([]);
							list_filter.push(item);
						});
					}

					console.log(list_filter);
					// throw "jos";
					// let new_tag_get = new Function("return [" + tag + "];")();

					// console.log({ keyword, category_id, city_id });
					let params = {
						keyword: keyword,
						category_id: id_of_new_category_id,
						city_id: id_of_new_city_id,
						page: page,
						lat: lat,
						long: long,
						sort: sort,
						filter: list_filter,
						user_id: dataUser.user_id,
					};
					let data = await searchHelper.search_activity_raw_login(params);
					console.log(data);
					const paramsSend = {
						error: false,
						message: "List activity",
						status_maintenance: status_maintenance,
						version: version,
						page: page,
					};
					send_response(res, data[0], paramsSend);

					// res.send(response);
					// console.log(data);
				} else {
					const response = {
						error: true,
						message: "Token not verified !!",
						data: {},
					};
					res.status(400).send(response);
				}
			})
			.catch(err => {
				console.log(err);
				const response = {
					error: true,
					status_maintenance: status_maintenance,
					version: version,
					message: err,
					data: {},
				};
				res.status(400).send(response);
			});
	} else {
		let {keyword, category_id, city_id, page, lat, long, sort, filter} = req.query;
		if (lat == 0 || long == 0 || lat === undefined || long === undefined) {
			long = 106.8249641;
			lat = -6.1753871;
		}
		let id_of_new_city_id = [];
		if (city_id != "") {
			let new_city_id = await new Function("return " + city_id + ";")();
			new_city_id.map(item => {
				// return item.city_id;
				id_of_new_city_id.push(item);
			});
		}

		let id_of_new_category_id = [];
		if (category_id != "") {
			let new_category_id = await new Function("return " + category_id + ";")();
			// Object.values(new_city_id.city_id);

			new_category_id.map(item => {
				// id_of_new_category_id = item.category_id;
				id_of_new_category_id.push(item);
			});
		}

		let list_filter = [];
		if (filter != "") {
			let new_filter = await new Function("return " + filter + ";")();
			// list_filter = new_filter;
			// console.log(Object.values(filter.filter));
			// console.log(new_filter);
			new_filter.map(item => {
				// list_filter = item.filter;
				// console.log([]);
				list_filter.push(item);
			});
		}

		console.log(list_filter);
		// throw "jos";
		// let new_tag_get = new Function("return [" + tag + "];")();

		// console.log({ keyword, category_id, city_id });
		let params = {
			keyword: keyword,
			category_id: id_of_new_category_id,
			city_id: id_of_new_city_id,
			page: page,
			lat: lat,
			long: long,
			sort: sort,
			filter: list_filter,
		};
		let data = await searchHelper.search_activity_raw_nologin(params);
		console.log(data);

		// const response = {
		//   error: false,
		//   message: "List activity",
		//   status_maintenance: status_maintenance,
		//   version: version,
		//   page: page,
		//   data: data[0],
		// };
		// res.send(response);

		const paramsSend = {
			error: false,
			message: "List activity",
			status_maintenance: status_maintenance,
			version: version,
			page: page,
		};
		send_response(res, data[0], paramsSend);
		console.log(data);
	}
};

exports.search_activity_recomended = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let {keyword, category_id, city_id, page, lat, long, sort} = req.query;
	if (lat == 0 || long == 0 || lat === undefined || long === undefined) {
		long = 106.8249641;
		lat = -6.1753871;
	}
	if (page == "" || page === undefined) {
		page = 0;
	}
	// console.log({ keyword, category_id, city_id });
	let interest = await UsersInterest.findAll({
		where: {
			user_id: user.user_id,
		},
	}).then(async interest => {
		if (interest) {
			return [1, 2, 3, 4, 5, 6, 7, 9, 10];
		} else {
			return await interest.map(interest => interest.category_id);
		}
	});
	let params3 = {
		keyword: "",
		category_id: interest,
		city_id: "",
		page: page,
		lat: lat,
		long: long,
		sort: "view",
	};
	let interestActivity = await searchHelper.search_by_interest(params3);
	// console.log(data);

	const response = {
		error: false,
		message: "List Recomended activity",
		status_maintenance: status_maintenance,
		version: version,
		page: page,
		data: interestActivity[0],
	};
	res.send(response);
};

exports.search_activity_tag = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let {keyword, category_id, tag, page, lat, long, sort} = req.query;
	if (lat == 0 || long == 0 || lat === undefined || long === undefined) {
		long = 106.8249641;
		lat = -6.1753871;
	}
	if (page == "" || page === undefined) {
		page = 0;
	}
	// console.log({ keyword, category_id, city_id });
	let params = {
		keyword: "",
		category_id: "",
		city_id: "",
		page: page,
		lat: lat,
		long: long,
		sort: "",
		tag: tag,
	};
	let interestActivity = await searchHelper.search_by_tag(params);
	// console.log(data);

	const response = {
		error: false,
		message: "List Activity by Hashtag",
		status_maintenance: status_maintenance,
		version: version,
		page: page,
		data: interestActivity[0],
	};
	res.send(response);
};

async function send_response(res, response, params) {
	// console.log(response);
	let result = await response.map(function (obj) {
		console.log(obj);
		let distance = 0;
		if (obj.distance != null) {
			distance = obj.distance;
		} else {
		}
		return {
			activity_id: obj.activity_id,
			title: obj.title,
			content: obj.content,
			lat: obj.lat,
			long: obj.long,
			date: obj.date,
			author_id: obj.author_id,
			category_id: obj.category_id,
			city_id: obj.city_id,
			public: obj.public,
			approval: obj.approval,
			slug: obj.slug,
			question: obj.question,
			slot: obj.slot,
			note: obj.note,
			address: obj.address,
			type: obj.type,
			link: obj.link,
			telp: obj.telp,
			whatsapp: obj.whatsapp,
			instagram: obj.instagram,
			view: obj.view,
			share: obj.share,
			active: obj.active,
			createdAt: obj.createdAt,
			updatedAt: obj.updatedAt,
			online: obj.online,
			distance: `${distance.toFixed(2)}`,
			city_name: obj.city_name,
			is_joined: obj.is_joined,
			is_requested: obj.is_requested,
			is_favorited: obj.is_favorited,
			count_request_join: obj.count_request_join,
			author_name: obj.author_name,
			avatar: obj.avatar,
			category_name: obj.category_name,
			tag: obj.tag,
			image: obj.image,
			list_user_join: obj.list_user_join,
			favorite: obj.favorite,
			user_joined: obj.user_joined,
			count_comment: obj.count_comment,
			count_all: obj.count_all,
		};
	});
	const responseJSON = {
		error: params.error,
		message: params.message,
		status_maintenance: params.status_maintenance,
		version: params.version,
		page: params.page,
		data: result,
	};
	res.send(responseJSON);
}
