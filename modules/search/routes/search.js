const express = require("express");
const searchControllers = require("../controllers/search");
const auth = require("../../../middleware/auth");
const router = express.Router();

router.get("/activity", auth.noLogin, searchControllers.search_activity);
router.get(
  "/activity_recomended",
  auth.isLogin,
  searchControllers.search_activity_recomended
);
router.get(
  "/activity_tag",
  auth.noLogin,
  searchControllers.search_activity_tag
);

module.exports = router;
