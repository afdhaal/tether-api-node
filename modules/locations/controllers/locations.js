const app = require("express");
var async = require("async");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const Province = require("../models/province");
const District = require("../models/district");
const subDistrict = require("../models/subdistrict");
const Countries = require("../models/country");
const Sequelize = require("sequelize");

exports.district_list = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  District.findAll({ order: [["city_name", "ASC"]] })
    .then((dataCity) => {
      //console.log(dataLocation[0]);
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List city",
        data: dataCity,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.district_popular = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  District.sequelize
    .query(
      `
      Select *, (select count(*) from activities where activities.city_id=districts.city_id) as count
      from districts order by count desc limit 5
    `
    )
    .then((dataPopular) => {
      console.log(dataPopular[0]);
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List city popular",
        data: dataPopular[0],
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
  // District.findAll()
  //   .then(dataCity => {
  //     //console.log(dataLocation[0]);
  //     const response = {
  //       error: false,
  //       status_maintenance: status_maintenance,
  // version: version,
  //       message: "List city popular",
  //       data: dataCity
  //     };
  //     res.send(response);
  //   })
  //   .catch(err => {
  //     const response = {
  //       error: true,
  //       status_maintenance: status_maintenance,
  // version: version,
  //       message: "Something Wrong",
  //       data: {}
  //     };
  //     res.status(400).send(response);
  //   });
};
