const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

//Table primary User
const subDistrict = sequelize.define("subdistrict", {
  subdistrict_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  province_id: {
    type: Sequelize.INTEGER
  },
  province: {
    type: Sequelize.STRING
  },
  city_id: {
    type: Sequelize.INTEGER
  },
  city: {
    type: Sequelize.STRING
  },
  subdistrict_name: {
    type: Sequelize.STRING
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = subDistrict;
