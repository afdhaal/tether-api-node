const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

//Table primary User
const Province = sequelize.define("province", {
  province_id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  province: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = Province;
