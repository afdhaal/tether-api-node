const express = require("express");
const locationControllers = require("../controllers/locations");
const auth = require("../../../middleware/auth");
const router = express.Router();

router.get("/list_city", auth.noLogin, locationControllers.district_list);
router.get(
  "/list_city_popular",
  auth.noLogin,
  locationControllers.district_popular
);

module.exports = router;
