"use strict";
const app = require("express");
const bcrypt = require("bcrypt");
const base64Img = require("base64-img");
const Activity = require("../../activity/models/activity");
const ActivityRating = require("../../activity/models/activity_rating");
const Category = require("../../activity/models/category");
const Users = require("../models/users");
const UsersInterest = require("../models/user_interest");
const UsersFollow = require("../models/user_follow");
const Notification = require("../../notifications/models/notification");
const { check, validationResult } = require("express-validator");
const Sequelize = require("sequelize");
const md5 = require("js-md5");
const saltRounds = 10;
const fcm = require("../../../utils/fcmSend");
const Op = require("sequelize").Op;
const activityHelper = require("../../../helper/query/activity");
const Maintenance = require("../models/maintenance");
const Version = require("../models/checker_version");
// const mailer = require("../../../helpers/mailer");
// const randomString = require("../../../helpers/genRandomString");
const admin = require("firebase-admin");

exports.Register = async (req, res, next) => {
  //required login
  //   let timestamp = new Date().getTime();
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;

  const {
    fullname,
    username,
    email,
    link,
    bio,
    gender,
    avatar,
    uid_google,
    uid_facebook,
    token_fcm,
  } = req.body;
  console.log(fullname, username, email, uid_google, uid_facebook, token_fcm);

  if (fullname && username && email && uid_google) {
    try {
      const timestamp = new Date().getTime();
      const tempToken = timestamp.toString();
      const salt = bcrypt.genSaltSync(saltRounds);
      const token_app = bcrypt.hashSync(tempToken, salt);

      Users.findOne({
        where: {
          [Op.or]: [
            {
              username: {
                [Op.eq]: username,
              },
            },
            {
              email: {
                [Op.eq]: email,
              },
            },
            {
              uid_google: {
                [Op.eq]: uid_google,
              },
            },
          ],
        },
      })
        .then(async (dataUser) => {
          if (dataUser == null) {
            console.log("jos");
            Users.create({
              fullname: fullname,
              username: username,
              email: email,
              link: link,
              bio: bio,
              gender: gender,
              uid_google: uid_google,
              token_app: token_app,
              token_fcm: token_fcm,
            })
              .then(async (createdUser) => {
                var validUrl = require("valid-url");

                // var url = "http://bla.com";
                function isValidUrl(avatar) {
                  if (validUrl.isUri(avatar)) {
                    console.log("Looks like an URI");
                    return true;
                  } else {
                    console.log("Not a URI");
                    return false;
                  }
                }
                let checkValidUrl = await isValidUrl(avatar);
                if (checkValidUrl == false) {
                  base64Img.imgSync(
                    "data:image/png;base64," + avatar,
                    "assets/avatar",
                    timestamp + "_avatar"
                  );
                  Users.update(
                    {
                      avatar:
                        process.env.VIEW_HOST +
                        "/avatar/" +
                        timestamp +
                        "_avatar.png",
                    },
                    {
                      where: {
                        user_id: createdUser.user_id,
                      },
                    }
                  )
                    .then((updateAvatar) => {
                      const response = {
                        error: false,
                        status_maintenance: status_maintenance,
                        version: version,
                        message: "Success Create Users",
                        data: {
                          user_id: createdUser.user_id,
                          fullname: createdUser.fullname,
                          username: createdUser.username,
                          email: createdUser.email,
                          phone: createdUser.phone,
                          uid_phone: createdUser.uid_phone,
                          avatar:
                            process.env.VIEW_HOST +
                            "/avatar/" +
                            timestamp +
                            "_avatar.png",
                          gender: createdUser.gender,
                          bio: createdUser.bio,
                          link: createdUser.link,
                          birthdate: createdUser.birthdate,
                          background_profile: createdUser.background_profile,
                          verified_email: createdUser.verified_email,
                          uid_google: createdUser.uid_google,
                          uid_facebook: "",
                          token_fcm: createdUser.token_fcm,
                          token_app: createdUser.token_app,
                          activity_count: 0,
                          follower_count: 0,
                          following_count: 0,
                          uid_phone: createdUser.uid_phone,
                          status_banned: createdUser.status_banned,
                          createdAt: createdUser.createdAt,
                          updatedAt: createdUser.updatedAt,
                        },
                      };
                      res.send(response);
                    })
                    .catch((err) => {
                      const response = {
                        error: true,
                        status_maintenance: status_maintenance,
                        version: version,
                        message: "Something Wrong Avatar",
                        data: {},
                      };
                      res.status(400).send(response);
                    });
                } else if (checkValidUrl == true) {
                  Users.update(
                    {
                      avatar: avatar,
                    },
                    {
                      where: {
                        user_id: createdUser.user_id,
                      },
                    }
                  )
                    .then((updateAvatar) => {
                      const response = {
                        error: false,
                        status_maintenance: status_maintenance,
                        version: version,
                        message: "Success Create Users",
                        data: {
                          user_id: createdUser.user_id,
                          fullname: createdUser.fullname,
                          username: createdUser.username,
                          email: createdUser.email,
                          phone: createdUser.phone,
                          uid_phone: createdUser.uid_phone,
                          avatar: avatar,
                          gender: createdUser.gender,
                          bio: createdUser.bio,
                          link: createdUser.link,
                          birthdate: createdUser.birthdate,
                          background_profile: createdUser.background_profile,
                          verified_email: createdUser.verified_email,
                          uid_google: createdUser.uid_google,
                          uid_facebook: "",
                          token_fcm: createdUser.token_fcm,
                          token_app: createdUser.token_app,
                          activity_count: 0,
                          follower_count: 0,
                          following_count: 0,
                          uid_phone: createdUser.uid_phone,
                          status_banned: createdUser.status_banned,
                          createdAt: createdUser.createdAt,
                          updatedAt: createdUser.updatedAt,
                        },
                      };
                      res.send(response);
                    })
                    .catch((err) => {
                      const response = {
                        error: true,
                        status_maintenance: status_maintenance,
                        version: version,
                        message: "Something Wrong Avatar",
                        data: {},
                      };
                      res.status(400).send(response);
                    });
                } else {
                  const response = {
                    error: false,
                    status_maintenance: status_maintenance,
                    version: version,
                    message: "Success Create Users",
                    data: {
                      user_id: createdUser.user_id,
                      fullname: createdUser.fullname,
                      username: createdUser.username,
                      email: createdUser.email,
                      phone: createdUser.phone,
                      uid_phone: createdUser.uid_phone,
                      avatar: createdUser.avatar,
                      gender: createdUser.gender,
                      bio: createdUser.bio,
                      link: createdUser.link,
                      birthdate: createdUser.birthdate,
                      background_profile: createdUser.background_profile,
                      verified_email: createdUser.verified_email,
                      uid_google: createdUser.uid_google,
                      uid_facebook: "",
                      token_fcm: createdUser.token_fcm,
                      token_app: createdUser.token_app,
                      activity_count: 0,
                      follower_count: 0,
                      following_count: 0,
                      uid_phone: createdUser.uid_phone,
                      status_banned: createdUser.status_banned,
                      createdAt: createdUser.createdAt,
                      updatedAt: createdUser.updatedAt,
                    },
                  };
                  res.status(200).send(response);
                }
              })
              .catch((err) => {
                console.log(err);

                const response = {
                  error: true,
                  status_maintenance: status_maintenance,
                  version: version,
                  message: "Something Wrong",
                  data: {},
                };
                res.status(400).send(response);
              });
          } else {
            const response = {
              error: true,
              status_maintenance: status_maintenance,
              version: version,
              message: "Nickname or email already registered",
              data: {},
            };
            res.status(400).send(response);
          }
        })
        .catch((err) => {
          console.log(err);

          const response = {
            error: true,
            status_maintenance: status_maintenance,
            version: version,
            message: "Something Wrong",
            data: {},
          };
          res.status(400).send(response);
        });
    } catch (error) {
      console.log(error);
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    }
  } else if (fullname && username && email && uid_facebook) {
    try {
      //need verification facebook uid
      let timestamp = new Date().getTime();
      let tempToken = timestamp.toString();
      let salt = bcrypt.genSaltSync(saltRounds);
      let token_app = bcrypt.hashSync(tempToken, salt);
      Users.findOne({
        where: {
          [Op.or]: [
            {
              username: {
                [Op.eq]: username,
              },
            },
            {
              email: {
                [Op.eq]: email,
              },
            },
            {
              uid_facebook: {
                [Op.eq]: uid_facebook,
              },
            },
          ],
        },
      })
        .then(async (dataUser) => {
          if (dataUser == null) {
            console.log("jos");
            Users.create({
              fullname: fullname,
              username: username,
              email: email,
              link: link,
              bio: bio,
              gender: gender,
              uid_facebook: uid_facebook,
              token_app: token_app,
              token_fcm: token_fcm,
            })
              .then(async (createdUser) => {
                var validUrl = require("valid-url");

                // var url = "http://bla.com";
                function isValidUrl(avatar) {
                  if (validUrl.isUri(avatar)) {
                    console.log("Looks like an URI");
                    return true;
                  } else {
                    console.log("Not a URI");
                    return false;
                  }
                }
                let checkValidUrl = await isValidUrl(avatar);
                if (checkValidUrl == false) {
                  base64Img.imgSync(
                    "data:image/png;base64," + avatar,
                    "assets/avatar",
                    timestamp + "_avatar"
                  );
                  Users.update(
                    {
                      avatar:
                        process.env.VIEW_HOST +
                        "/avatar/" +
                        timestamp +
                        "_avatar.png",
                    },
                    {
                      where: {
                        user_id: createdUser.user_id,
                      },
                    }
                  )
                    .then((updateAvatar) => {
                      const response = {
                        error: false,
                        status_maintenance: status_maintenance,
                        version: version,
                        message: "Success Create Users",
                        data: {
                          user_id: createdUser.user_id,
                          fullname: createdUser.fullname,
                          username: createdUser.username,
                          email: createdUser.email,
                          phone: createdUser.phone,
                          uid_phone: createdUser.uid_phone,
                          avatar:
                            process.env.VIEW_HOST +
                            "/avatar/" +
                            timestamp +
                            "_avatar.png",
                          gender: createdUser.gender,
                          bio: createdUser.bio,
                          link: createdUser.link,
                          birthdate: createdUser.birthdate,
                          background_profile: createdUser.background_profile,
                          verified_email: createdUser.verified_email,
                          uid_google: createdUser.uid_google,
                          uid_facebook: "",
                          token_fcm: createdUser.token_fcm,
                          token_app: createdUser.token_app,
                          activity_count: 0,
                          follower_count: 0,
                          following_count: 0,
                          uid_phone: createdUser.uid_phone,
                          status_banned: createdUser.status_banned,
                          createdAt: createdUser.createdAt,
                          updatedAt: createdUser.updatedAt,
                        },
                      };
                      res.send(response);
                    })
                    .catch((err) => {
                      const response = {
                        error: true,
                        status_maintenance: status_maintenance,
                        version: version,
                        message: "Something Wrong Avatar",
                        data: {},
                      };
                      res.status(400).send(response);
                    });
                } else if (checkValidUrl == true) {
                  Users.update(
                    {
                      avatar: avatar,
                    },
                    {
                      where: {
                        user_id: createdUser.user_id,
                      },
                    }
                  )
                    .then((updateAvatar) => {
                      const response = {
                        error: false,
                        status_maintenance: status_maintenance,
                        version: version,
                        message: "Success Create Users",
                        data: {
                          user_id: createdUser.user_id,
                          fullname: createdUser.fullname,
                          username: createdUser.username,
                          email: createdUser.email,
                          phone: createdUser.phone,
                          uid_phone: createdUser.uid_phone,
                          avatar: avatar,
                          gender: createdUser.gender,
                          bio: createdUser.bio,
                          link: createdUser.link,
                          birthdate: createdUser.birthdate,
                          background_profile: createdUser.background_profile,
                          verified_email: createdUser.verified_email,
                          uid_google: createdUser.uid_google,
                          uid_facebook: "",
                          token_fcm: createdUser.token_fcm,
                          token_app: createdUser.token_app,
                          activity_count: 0,
                          follower_count: 0,
                          following_count: 0,
                          uid_phone: createdUser.uid_phone,
                          status_banned: createdUser.status_banned,
                          createdAt: createdUser.createdAt,
                          updatedAt: createdUser.updatedAt,
                        },
                      };
                      res.send(response);
                    })
                    .catch((err) => {
                      const response = {
                        error: true,
                        status_maintenance: status_maintenance,
                        version: version,
                        message: "Something Wrong Avatar",
                        data: {},
                      };
                      res.status(400).send(response);
                    });
                } else {
                  const response = {
                    error: false,
                    status_maintenance: status_maintenance,
                    version: version,
                    message: "Success Create Users",
                    data: {
                      user_id: createdUser.user_id,
                      fullname: createdUser.fullname,
                      username: createdUser.username,
                      email: createdUser.email,
                      phone: createdUser.phone,
                      uid_phone: createdUser.uid_phone,
                      avatar: createdUser.avatar,
                      gender: createdUser.gender,
                      bio: createdUser.bio,
                      link: createdUser.link,
                      birthdate: createdUser.birthdate,
                      background_profile: createdUser.background_profile,
                      verified_email: createdUser.verified_email,
                      uid_google: createdUser.uid_google,
                      uid_facebook: "",
                      token_fcm: createdUser.token_fcm,
                      token_app: createdUser.token_app,
                      activity_count: 0,
                      follower_count: 0,
                      following_count: 0,
                      uid_phone: createdUser.uid_phone,
                      status_banned: createdUser.status_banned,
                      createdAt: createdUser.createdAt,
                      updatedAt: createdUser.updatedAt,
                    },
                  };
                  res.status(200).send(response);
                }
              })
              .catch((err) => {
                console.log(err);

                const response = {
                  error: true,
                  status_maintenance: status_maintenance,
                  version: version,
                  message: "Something Wrong",
                  data: {},
                };
                res.status(400).send(response);
              });
          } else {
            const response = {
              error: true,
              status_maintenance: status_maintenance,
              version: version,
              message: "Nickname or email already registered",
              data: {},
            };
            res.status(400).send(response);
          }
        })
        .catch((err) => {
          console.log(err);

          const response = {
            error: true,
            status_maintenance: status_maintenance,
            version: version,
            message: "Something Wrong",
            data: {},
          };
          res.status(400).send(response);
        });
    } catch (error) {
      console.log(error);
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    }
  } else {
    const response = {
      error: true,
      status_maintenance: status_maintenance,
      version: version,
      message: "Missing Parameter",
      data: {},
    };
    res.status(400).send(response);
  }
  // if (fullname && username && email && password && phone && uid_phone) {
  //   try {
  //     const myPlaintextPassword = password;
  //     //hash password
  //     bcrypt.hash(myPlaintextPassword, saltRounds, async function (err, hash) {
  //       let timestamp = new Date().getTime();
  //       let tempToken = timestamp.toString();
  //       var salt = bcrypt.genSaltSync(saltRounds);
  //       var token_app = bcrypt.hashSync(tempToken, salt);
  //       Users.findOne({
  //         where: {
  //           [Op.or]: [
  //             {
  //               username: {
  //                 [Op.eq]: username,
  //               },
  //             },
  //             {
  //               email: {
  //                 [Op.eq]: email,
  //               },
  //             },
  //             {
  //               phone: {
  //                 [Op.eq]: phone,
  //               },
  //             },
  //           ],
  //         },
  //       })
  //         .then(async (dataUser) => {
  //           if (dataUser == null) {
  //             console.log("jos");
  //             Users.create({
  //               fullname: fullname,
  //               username: username,
  //               email: email,
  //               password: hash,
  //               phone: phone,
  //               uid_phone: uid_phone,
  //               token_app: token_app,
  //               token_fcm: token_fcm,
  //             })
  //               .then((createdUser) => {
  //                 const response = {
  //                   error: false,
  //                   status_maintenance: status_maintenance,
  //                   version: version,
  //                   message: "Success Create Users",
  //                   data: {
  //                     user_id: createdUser.user_id,
  //                     fullname: createdUser.fullname,
  //                     username: createdUser.username,
  //                     email: createdUser.email,
  //                     phone: createdUser.phone,
  //                     uid_phone: createdUser.uid_phone,
  //                     avatar: createdUser.avatar,
  //                     gender: createdUser.gender,
  //                     bio: createdUser.bio,
  //                     link: createdUser.bio,
  //                     birthdate: createdUser.birthdate,
  //                     background_profile: createdUser.background_profile,
  //                     verified_email: createdUser.verified_email,
  //                     token_fcm: createdUser.token_fcm,
  //                     token_app: createdUser.token_app,
  //                     activity_count: 0,
  //                     follower_count: 0,
  //                     following_count: 0,
  //                     uid_phone: createdUser.uid_phone,
  //                     status_banned: createdUser.status_banned,
  //                     createdAt: createdUser.createdAt,
  //                     updatedAt: createdUser.updatedAt,
  //                   },
  //                 };
  //                 res.status(200).send(response);
  //               })
  //               .catch((err) => {
  //                 console.log(err);

  //                 const response = {
  //                   error: true,
  //                   status_maintenance: status_maintenance,
  //                   version: version,
  //                   message: "Something Wrong",
  //                   data: {},
  //                 };
  //                 res.status(400).send(response);
  //               });
  //           } else {
  //             const response = {
  //               error: true,
  //               status_maintenance: status_maintenance,
  //               version: version,
  //               message: "Username, email or phone already registered",
  //               data: {},
  //             };
  //             res.status(400).send(response);
  //           }
  //         })
  //         .catch((err) => {
  //           console.log(err);

  //           const response = {
  //             error: true,
  //             status_maintenance: status_maintenance,
  //             version: version,
  //             message: "Something Wrong",
  //             data: {},
  //           };
  //           res.status(400).send(response);
  //         });
  //     });
  //   } catch (error) {
  //     const response = {
  //       error: true,
  //       status_maintenance: status_maintenance,
  //       version: version,
  //       message: "Something Wrong",
  //       data: {},
  //     };
  //     res.status(400).send(response);
  //   }
  // } else {
  //   const response = {
  //     error: true,
  //     status_maintenance: status_maintenance,
  //     version: version,
  //     message: "Some parameter missing",
  //     data: {},
  //   };
  //   res.status(400).send(response);
  // }
};

exports.Login = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const { email, uid_google, uid_facebook } = req.body;
  if (email && uid_google) {
    Users.findOne({
      where: {
        email: email,
        uid_google: uid_google,
      },
    }).then(async (userData) => {
      if (userData) {
        let activity_count = await Activity.findAndCountAll({
          where: {
            author_id: userData.user_id,
          },
          limit: 1,
        });
        let follower_count = await UsersFollow.findAndCountAll({
          where: {
            user_id_followed: userData.user_id,
          },
          limit: 1,
        });
        let following_count = await UsersFollow.findAndCountAll({
          where: {
            user_id: userData.user_id,
          },
          limit: 1,
        });
        //compare password for login using bcrypt
        // const result = bcrypt.compareSync(password, userData.password);
        if (userData) {
          let timestamp = new Date().getTime();
          let tempToken = timestamp.toString();
          let salt = bcrypt.genSaltSync(saltRounds);
          let token_app = bcrypt.hashSync(tempToken, salt);
          Users.update(
            {
              token_app: token_app,
            },
            {
              where: {
                user_id: userData.user_id,
              },
            }
          )
            .then((updateToken) => {
              const response = {
                error: false,
                status_maintenance: status_maintenance,
                version: version,
                message: "Success Login",
                data: {
                  user_id: userData.user_id,
                  fullname: userData.fullname,
                  username: userData.username,
                  email: userData.email,
                  phone: userData.phone,
                  uid_phone: userData.uid_phone,
                  uid_google: userData.uid_google,
                  uid_facebook: userData.uid_facebook,
                  avatar: userData.avatar,
                  gender: userData.gender,
                  bio: userData.bio,
                  link: userData.link,
                  birthdate: userData.birthdate,
                  background_profile: userData.background_profile,
                  verified_email: userData.verified_email,
                  token_fcm: userData.token_fcm,
                  token_app: token_app,
                  activity_count: activity_count.count,
                  follower_count: follower_count.count,
                  following_count: following_count.count,
                  status_banned: userData.status_banned,
                  createdAt: userData.createdAt,
                  updatedAt: userData.updatedAt,
                },
              };
              res.status(200).send(response);
            })
            .catch((err) => {
              console.log(err);
              const response = {
                error: true,
                status_maintenance: status_maintenance,
                version: version,
                message: "User not found",
                data: {},
              };
              res.status(400).send(response);
            });
        } else {
          const response = {
            error: true,
            status_maintenance: status_maintenance,
            version: version,
            message: "Email and uid not match",
            data: {},
          };
          res.status(400).send(response);
        }
      } else {
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "User not found",
          data: {},
        };
        res.status(400).send(response);
      }
    });
  } else if (email && uid_facebook) {
    Users.findOne({
      where: {
        email: email,
        uid_google: uid_facebook,
      },
    }).then(async (userData) => {
      if (userData) {
        let activity_count = await Activity.findAndCountAll({
          where: {
            author_id: userData.user_id,
          },
          limit: 1,
        });
        let follower_count = await UsersFollow.findAndCountAll({
          where: {
            user_id_followed: userData.user_id,
          },
          limit: 1,
        });
        let following_count = await UsersFollow.findAndCountAll({
          where: {
            user_id: userData.user_id,
          },
          limit: 1,
        });
        //compare password for login using bcrypt
        // const result = bcrypt.compareSync(password, userData.password);
        if (userData) {
          let timestamp = new Date().getTime();
          let tempToken = timestamp.toString();
          let salt = bcrypt.genSaltSync(saltRounds);
          let token_app = bcrypt.hashSync(tempToken, salt);
          Users.update(
            {
              token_app: token_app,
            },
            {
              where: {
                user_id: userData.user_id,
              },
            }
          )
            .then((updateToken) => {
              const response = {
                error: false,
                status_maintenance: status_maintenance,
                version: version,
                message: "Success Login",
                data: {
                  user_id: userData.user_id,
                  fullname: userData.fullname,
                  username: userData.username,
                  email: userData.email,
                  phone: userData.phone,
                  uid_phone: userData.uid_phone,
                  uid_google: userData.uid_google,
                  uid_facebook: userData.uid_facebook,
                  avatar: userData.avatar,
                  gender: userData.gender,
                  bio: userData.bio,
                  link: userData.link,
                  birthdate: userData.birthdate,
                  background_profile: userData.background_profile,
                  verified_email: userData.verified_email,
                  token_fcm: userData.token_fcm,
                  token_app: token_app,
                  activity_count: activity_count.count,
                  follower_count: follower_count.count,
                  following_count: following_count.count,
                  status_banned: userData.status_banned,
                  createdAt: userData.createdAt,
                  updatedAt: userData.updatedAt,
                },
              };
              res.status(200).send(response);
            })
            .catch((err) => {
              console.log(err);
              const response = {
                error: true,
                status_maintenance: status_maintenance,
                version: version,
                message: "User not found",
                data: {},
              };
              res.status(400).send(response);
            });
        } else {
          const response = {
            error: true,
            status_maintenance: status_maintenance,
            version: version,
            message: "Email and uid not match",
            data: {},
          };
          res.status(400).send(response);
        }
      } else {
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "User not found",
          data: {},
        };
        res.status(400).send(response);
      }
    });
  } else {
    const response = {
      error: true,
      status_maintenance: status_maintenance,
      version: version,
      message: "Some parameter missing",
      data: {},
    };
    res.status(400).send(response);
  }
};

exports.infoUser = async (req, res, next) => {
  let user = res.locals.user;
  let detailuser = res.locals.detailuser;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  console.log("version");

  console.log(version);
  let activity_count = await Activity.findAndCountAll({
    where: {
      author_id: user.user_id,
    },
    limit: 1,
  });
  let follower_count = await UsersFollow.findAndCountAll({
    where: {
      user_id_followed: user.user_id,
    },
    limit: 1,
  });
  let following_count = await UsersFollow.findAndCountAll({
    where: {
      user_id: user.user_id,
    },
    limit: 1,
  });
  let like = await ActivityRating.findAndCountAll({
    where: {
      user_id: user.user_id,
      rating: "like",
    },
    limit: 1,
  });
  let dislike = await ActivityRating.findAndCountAll({
    where: {
      user_id: user.user_id,
      rating: "dislike",
    },
    limit: 1,
  });
  if (user) {
    const response = {
      error: false,
      status_maintenance: status_maintenance,
      version: version,
      message: "Info User",
      data: {
        user_id: user.user_id,
        fullname: user.fullname,
        username: user.username,
        email: user.email,
        phone: user.phone,
        uid_phone: user.uid_phone,
        avatar: user.avatar,
        gender: user.gender,
        bio: user.bio,
        link: user.link,
        birthdate: user.birthdate,
        background_profile: user.background_profile,
        verified_email: user.verified_email,
        token_fcm: user.token_fcm,
        token_app: user.token_app,
        uid_phone: user.uid_phone,
        activity_count: activity_count.count,
        follower_count: follower_count.count,
        following_count: following_count.count,
        like: like.count,
        dislike: dislike.count,
        status_banned: user.status_banned,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt,
      },
    };
    res.status(200).send(response);
  } else {
    const response = {
      error: true,
      message: "Something Wrong",
      status_maintenance: status_maintenance,
      version: version,
      data: {},
    };
    res.status(400).send(response);
  }
};

exports.logout = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let new_tokenfirebase = req.body.new_tokenfirebase;
  User.update(
    {
      token_app: "",
    },
    {
      where: {
        id: user.id,
      },
    }
  )
    .then((userUpdate) => {
      if (userUpdate) {
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Success Logout",
          data: {},
        };
        res.send(response);
      } else {
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Something Wrong",
          data: {},
        };
        res.status(400).send(response);
      }
    })
    .catch((err) => {
      // console.log(err);
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};
exports.updateTokenFcm = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let new_tokenfcm = req.body.new_tokenfcm;
  Users.update(
    {
      token_fcm: new_tokenfcm,
    },
    {
      where: {
        user_id: user.user_id,
      },
    }
  )
    .then((userData) => {
      if (userData) {
        const response = {
          error: false,
          status_maintenance: status_maintenance,
          version: version,
          message: "Success Update Token Fcm",
          data: {},
        };
        res.send(response);
      } else {
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Something Wrong",
          data: {},
        };
        res.status(400).send(response);
      }
    })
    .catch((err) => {
      // console.log(err);
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.ChangePasswordOTP = (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let newpassword = req.body.newpassword;
  let uid_phone = req.body.uid_phone;
  //console.log({token : token, password:password});
  bcrypt.hash(newpassword, saltRounds, async function (err, hash) {
    // console.log(hash);
    Users.findOne({
      where: {
        uid_phone: uid_phone,
      },
    })
      .then((userData) => {
        if (userData) {
          Users.update(
            {
              password: hash,
              token_app: "",
            },
            {
              where: {
                user_id: userData.user_id,
              },
            }
          )
            .then((updatedPassword) => {
              const response = {
                error: false,
                status_maintenance: status_maintenance,
                version: version,
                message: "Success Change Password",
                data: {},
              };
              res.status(200).send(response);
            })
            .catch((err) => {
              // console.log(err);
              const response = {
                error: true,
                status_maintenance: status_maintenance,
                version: version,
                message: "Something Wrong",
                data: {},
              };
              res.status(400).send(response);
            });
        } else {
          const response = {
            error: true,
            status_maintenance: status_maintenance,
            version: version,
            message: "Authentication not valid",
            data: {},
          };
          res.status(400).send(response);
        }
      })
      .catch((err) => {
        // console.log(err);
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Something Wrong",
          data: {},
        };
        res.status(400).send(response);
      });
  });
};

exports.ChangePhoneOTP = (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let new_phone = req.body.new_phone;
  let uid_phone_old = req.body.uid_phone_old;
  let uid_phone_new = req.body.uid_phone_new;
  //console.log({token : token, password:password});
  Users.findOne({
    where: {
      uid_phone: uid_phone_old,
    },
  })
    .then((userData) => {
      if (userData) {
        Users.update(
          {
            uid_phone: uid_phone_new,
            phone: new_phone,
          },
          {
            where: {
              user_id: userData.user_id,
            },
          }
        )
          .then((updatedPhone) => {
            const response = {
              error: false,
              status_maintenance: status_maintenance,
              version: version,
              message: "Success Change Phone Number",
              data: {},
            };
            res.status(200).send(response);
          })
          .catch((err) => {
            console.log(err);
            const response = {
              error: true,
              status_maintenance: status_maintenance,
              version: version,
              message: "Authentication not valid",
              data: {},
            };
            res.status(400).send(response);
          });
      } else {
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Authentication not valid",
          data: {},
        };
        res.status(400).send(response);
      }
    })
    .catch((err) => {
      // console.log(err);
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};
exports.edit_profile = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const timestamp = new Date().getTime();
  const avatar = req.body.avatar;
  let date = req.body.birthdate;
  if (date == "Invalid date" || date == undefined) {
    date = null;
  }
  // console.log(date);
  // throw "tes";
  let avatarname;
  Users.update(
    {
      fullname: req.body.fullname,
      bio: req.body.bio,
      link: req.body.link,
      birthdate: date,
      gender: req.body.gender,
    },
    {
      where: {
        user_id: user.user_id,
      },
    }
  )
    .then((updateUser) => {
      if (avatar) {
        base64Img.imgSync(
          "data:image/png;base64," + avatar,
          "assets/avatar",
          timestamp + "_avatar"
        );
        avatarname =
          process.env.VIEW_HOST + "/avatar/" + timestamp + "_avatar.png";

        Users.update(
          {
            avatar: avatarname,
          },
          {
            where: {
              user_id: user.user_id,
            },
          }
        )
          .then((updateAvatar) => {
            const response = {
              error: false,
              status_maintenance: status_maintenance,
              version: version,
              message: "Success Edit Profile",
              data: {},
            };
            res.send(response);
          })
          .catch((err) => {
            const response = {
              error: true,
              status_maintenance: status_maintenance,
              version: version,
              message: "Something Wrong",
              data: {},
            };
            res.status(400).send(response);
          });
      } else {
        const response = {
          error: false,
          status_maintenance: status_maintenance,
          version: version,
          message: "Success Edit Profile",
          data: {},
        };
        res.send(response);
      }
    })
    .catch((err) => {
      console.log(err);
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.updateAvatar = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const timestamp = new Date().getTime();
  const avatar = req.body.avatar;
  let avatarname;
  if (avatar) {
    base64Img.imgSync(
      "data:image/png;base64," + avatar,
      "assets/avatar",
      timestamp + "_avatar"
    );
    avatarname = process.env.VIEW_HOST + "/avatar/" + timestamp + "_avatar.png";
  }
  Users.update(
    {
      avatar: avatarname,
    },
    {
      where: {
        user_id: user.user_id,
      },
    }
  )
    .then((updateAvatar) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "Success Update Avatar",
        data: {},
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.add_interest = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  UsersInterest.findOne({
    where: {
      category_id: req.body.category_id,
      user_id: user.user_id,
    },
  })
    .then((dataInterest) => {
      if (dataInterest) {
        UsersInterest.destroy({
          where: {
            category_id: req.body.category_id,
            user_id: user.user_id,
          },
        })
          .then((createInterest) => {
            const response = {
              error: false,
              status_maintenance: status_maintenance,
              version: version,
              message: "Remove Interest Success",
              data: {},
            };
            res.send(response);
          })
          .catch((err) => {
            const response = {
              error: true,
              status_maintenance: status_maintenance,
              version: version,
              message: "Something Wrong",
              data: {},
            };
            res.status(400).send(response);
          });
      } else {
        UsersInterest.create({
          category_id: req.body.category_id,
          user_id: user.user_id,
        })
          .then((createInterest) => {
            const response = {
              error: false,
              status_maintenance: status_maintenance,
              version: version,
              message: "Add Interest Success",
              data: {},
            };
            res.send(response);
          })
          .catch((err) => {
            const response = {
              error: true,
              status_maintenance: status_maintenance,
              version: version,
              message: "Something Wrong",
              data: {},
            };
            res.status(400).send(response);
          });
      }
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.add_subscribe = async (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const { subscribe } = req.body;

  // console.log(subscribe);

  subscribe.forEach(async (element) => {
    console.log(element);
    await UsersInterest.findOne({
      where: {
        category_id: element.category_id,
        user_id: user.user_id,
      },
    })
      .then((searchInsterest) => {
        if (searchInsterest) {
        } else {
          UsersInterest.create({
            category_id: element.category_id,
            user_id: user.user_id,
          });
        }
      })
      .catch((err) => {
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Something Wrong",
          data: {},
        };
        res.status(400).send(response);
      });
  });

  const response = {
    error: false,
    status_maintenance: status_maintenance,
    version: version,
    message: "Success Add Subscribe Interest",
    data: {},
  };
  res.send(response);

  // UsersInterest.findOne({
  //   where: {
  //     category_id: req.body.category_id,
  //     user_id: user.user_id
  //   }
  // })
  //   .then(dataInterest => {
  //     if (dataInterest) {
  //       UsersInterest.destroy({
  //         where: {
  //           category_id: req.body.category_id,
  //           user_id: user.user_id
  //         }
  //       })
  //         .then(createInterest => {
  //           const response = {
  //             error: false,
  //             status_maintenance: status_maintenance,
  // version: version,
  //             message: "Remove Interest Success",
  //             data: {}
  //           };
  //           res.send(response);
  //         })
  //         .catch(err => {
  //           const response = {
  //             error: true,
  //             status_maintenance: status_maintenance,
  // version: version,
  //             message: "Something Wrong",
  //             data: {}
  //           };
  //           res.status(400).send(response);
  //         });
  //     } else {
  //       UsersInterest.create({
  //         category_id: req.body.category_id,
  //         user_id: user.user_id
  //       })
  //         .then(createInterest => {
  //           const response = {
  //             error: false,
  //             status_maintenance: status_maintenance,
  // version: version,
  //             message: "Add Interest Success",
  //             data: {}
  //           };
  //           res.send(response);
  //         })
  //         .catch(err => {
  //           const response = {
  //             error: true,
  //             status_maintenance: status_maintenance,
  // version: version,
  //             message: "Something Wrong",
  //             data: {}
  //           };
  //           res.status(400).send(response);
  //         });
  //     }
  //   })
  //   .catch(err => {
  //     const response = {
  //       error: true,
  //       status_maintenance: status_maintenance,
  // version: version,
  //       message: "Something Wrong",
  //       data: {}
  //     };
  //     res.status(400).send(response);
  //   });
};

exports.list_interest = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const { page } = req.query;
  Users.hasMany(UsersInterest, { foreignKey: "user_id" });
  UsersInterest.belongsTo(Users, { foreignKey: "user_id" });

  Category.hasMany(UsersInterest, { foreignKey: "user_id" });
  UsersInterest.belongsTo(Category, { foreignKey: "user_id" });

  UsersInterest.findAll({
    where: {
      user_id: user.user_id,
    },
    include: [
      {
        model: Category,
        required: false,
        attributes: {
          exclude: [],
        },
      },
    ],
    order: [["user_interest_id", "DESC"]],
    offset: page * 10,
    limit: 10,
  })
    .then((dataInterest) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List Interest",
        data: dataInterest,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.visit_user = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let { user_id, long, lat } = req.query;
  if (lat == 0 || long == 0 || lat === undefined || long === undefined) {
    long = 106.8249641;
    lat = -6.1753871;
  }

  const token = req.token;
  if (token) {
    Users.findOne({
      where: {
        token_app: token,
      },
    })
      .then(async (dataToken) => {
        if (dataToken) {
          if (user_id) {
            let params = {
              keyword: "",
              category_id: "",
              city_id: "",
              page: 0,
              lat: lat,
              long: long,
              sort: "lastest",
              user_id: user_id,
            };
            let is_me = false;
            if (dataToken.user_id == user_id) {
              is_me = true;
            } else {
              is_me = false;
            }
            let is_following = await UsersFollow.findOne({
              where: {
                user_id: dataToken.user_id,
                user_id_followed: user_id,
              },
            }).then(async (dataFollow) => {
              if (dataFollow) {
                return true;
              } else {
                return false;
              }
            });
            let activity_list = await activityHelper.search_by_interest(params);
            let activity_count = await Activity.findAndCountAll({
              where: {
                author_id: user_id,
              },
            });
            let follower_count = await UsersFollow.findAndCountAll({
              where: {
                user_id_followed: user_id,
              },
            });
            let following_count = await UsersFollow.findAndCountAll({
              where: {
                user_id: user_id,
              },
            });
            let like = await ActivityRating.findAndCountAll({
              where: {
                user_id: user_id,
                rating: "like",
              },
              limit: 1,
            });
            let dislike = await ActivityRating.findAndCountAll({
              where: {
                user_id: user_id,
                rating: "dislike",
              },
              limit: 1,
            });
            Users.findOne({
              attributes: {
                exclude: [
                  "password",
                  "email",
                  "phone",
                  "token_fcm",
                  "uid_phone",
                  "token_app",
                ],
              },
              where: {
                user_id: user_id,
              },
            })
              .then((dataUser) => {
                // console.log(activity_list[0]);
                if (dataUser) {
                  const response = {
                    error: false,
                    status_maintenance: status_maintenance,
                    version: version,
                    message: "User Detail",
                    data: {
                      user_id: dataUser.user_id,
                      fullname: dataUser.fullname,
                      username: dataUser.username,
                      email: dataUser.email,
                      phone: dataUser.phone,
                      uid_phone: dataUser.uid_phone,
                      avatar: dataUser.avatar,
                      gender: dataUser.gender,
                      bio: dataUser.bio,
                      link: dataUser.bio,
                      birthdate: dataUser.birthdate,
                      background_profile: dataUser.background_profile,
                      status_banned: dataUser.status_banned,
                      createdAt: dataUser.createdAt,
                      updatedAt: dataUser.updatedAt,
                      post_count: activity_count.count,
                      follower_count: follower_count.count,
                      following_count: following_count.count,
                      like: like.count,
                      dislike: dislike.count,
                      is_following: is_following,
                      is_me: is_me,
                      activity: activity_list[0],
                    },
                  };
                  res.send(response);
                } else {
                  const response = {
                    error: true,
                    status_maintenance: status_maintenance,
                    version: version,
                    message: "User not found",
                    data: {},
                  };
                  res.status(400).send(response);
                }
              })
              .catch((err) => {
                console.log(err);
                const response = {
                  error: true,
                  status_maintenance: status_maintenance,
                  version: version,
                  message: "Something Wrong",
                  data: {},
                };
                res.status(400).send(response);
              });
          } else {
            const response = {
              error: true,
              status_maintenance: status_maintenance,
              version: version,
              message: "Parameter user_id required",
              data: {},
            };
            res.status(400).send(response);
          }
        } else {
          let params = {
            keyword: "",
            category_id: "",
            city_id: "",
            page: 0,
            lat: lat,
            long: long,
            sort: "lastest",
            user_id: user_id,
          };
          let is_me = false;
          let is_following = false;
          let activity_list = await activityHelper.search_by_interest(params);
          let activity_count = await Activity.findAndCountAll({
            where: {
              author_id: user_id,
            },
          });
          let follower_count = await UsersFollow.findAndCountAll({
            where: {
              user_id_followed: user_id,
            },
          });
          let following_count = await UsersFollow.findAndCountAll({
            where: {
              user_id: user_id,
            },
          });
          let like = await ActivityRating.findAndCountAll({
            where: {
              user_id: user_id,
              rating: "like",
            },
            limit: 1,
          });
          let dislike = await ActivityRating.findAndCountAll({
            where: {
              user_id: user_id,
              rating: "dislike",
            },
            limit: 1,
          });
          Users.findOne({
            attributes: {
              exclude: [
                "password",
                "email",
                "phone",
                "token_fcm",
                "uid_phone",
                "token_app",
              ],
            },
            where: {
              user_id: user_id,
            },
          })
            .then((dataUser) => {
              // console.log(activity_list[0]);
              if (dataUser) {
                const response = {
                  error: false,
                  status_maintenance: status_maintenance,
                  version: version,
                  message: "User Detail",
                  data: {
                    user_id: dataUser.user_id,
                    fullname: dataUser.fullname,
                    username: dataUser.username,
                    email: dataUser.email,
                    phone: dataUser.phone,
                    uid_phone: dataUser.uid_phone,
                    avatar: dataUser.avatar,
                    gender: dataUser.gender,
                    bio: dataUser.bio,
                    link: dataUser.bio,
                    birthdate: dataUser.birthdate,
                    background_profile: dataUser.background_profile,
                    status_banned: dataUser.status_banned,
                    createdAt: dataUser.createdAt,
                    updatedAt: dataUser.updatedAt,
                    post_count: activity_count.count,
                    follower_count: follower_count.count,
                    following_count: following_count.count,
                    like: like.count,
                    dislike: dislike.count,
                    is_following: is_following,
                    is_me: is_me,
                    lastest_activity: activity_list[0],
                  },
                };
                res.send(response);
              } else {
                const response = {
                  error: true,
                  status_maintenance: status_maintenance,
                  version: version,
                  message: "User not found",
                  data: {},
                };
                res.status(400).send(response);
              }
            })
            .catch((err) => {
              console.log(err);
              const response = {
                error: true,
                status_maintenance: status_maintenance,
                version: version,
                message: "Something Wrong",
                data: {},
              };
              res.status(400).send(response);
            });
        }
      })
      .catch((err) => {
        console.log(err);
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Something Wrong",
          data: {},
        };
        res.status(400).send(response);
      });
  } else {
    let params = {
      keyword: "",
      category_id: "",
      city_id: "",
      page: 0,
      lat: lat,
      long: long,
      sort: "lastest",
      user_id: user_id,
    };
    let is_me = false;
    let is_following = false;
    let activity_list = await activityHelper.search_by_interest(params);
    let activity_count = await Activity.findAndCountAll({
      where: {
        author_id: user_id,
      },
    });
    let follower_count = await UsersFollow.findAndCountAll({
      where: {
        user_id_followed: user_id,
      },
    });
    let following_count = await UsersFollow.findAndCountAll({
      where: {
        user_id: user_id,
      },
    });
    let like = await ActivityRating.findAndCountAll({
      where: {
        user_id: user_id,
        rating: "like",
      },
      limit: 1,
    });
    let dislike = await ActivityRating.findAndCountAll({
      where: {
        user_id: user_id,
        rating: "dislike",
      },
      limit: 1,
    });
    Users.findOne({
      attributes: {
        exclude: [
          "password",
          "email",
          "phone",
          "token_fcm",
          "uid_phone",
          "token_app",
        ],
      },
      where: {
        user_id: user_id,
      },
    })
      .then((dataUser) => {
        // console.log(activity_list[0]);
        if (dataUser) {
          const response = {
            error: false,
            status_maintenance: status_maintenance,
            version: version,
            message: "User Detail",
            data: {
              user_id: dataUser.user_id,
              fullname: dataUser.fullname,
              username: dataUser.username,
              email: dataUser.email,
              phone: dataUser.phone,
              uid_phone: dataUser.uid_phone,
              avatar: dataUser.avatar,
              gender: dataUser.gender,
              bio: dataUser.bio,
              link: dataUser.bio,
              birthdate: dataUser.birthdate,
              background_profile: dataUser.background_profile,
              status_banned: dataUser.status_banned,
              createdAt: dataUser.createdAt,
              updatedAt: dataUser.updatedAt,
              post_count: activity_count.count,
              follower_count: follower_count.count,
              following_count: following_count.count,
              like: like.count,
              dislike: dislike.count,
              is_following: is_following,
              is_me: is_me,
              lastest_activity: activity_list[0],
            },
          };
          res.send(response);
        } else {
          const response = {
            error: true,
            status_maintenance: status_maintenance,
            version: version,
            message: "User not found",
            data: {},
          };
          res.status(400).send(response);
        }
      })
      .catch((err) => {
        console.log(err);
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Something Wrong",
          data: {},
        };
        res.status(400).send(response);
      });
  }
};

exports.banned_account = (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let { uid_phone } = req.body;
  Users.findOne({
    where: {
      uid_phone: uid_phone,
    },
  })
    .then((userData) => {
      if (userData) {
        Users.update(
          {
            status_banned: true,
          },
          {
            where: {
              user_id: userData.user_id,
            },
          }
        )
          .then((updatedPassword) => {
            const response = {
              error: false,
              status_maintenance: status_maintenance,
              version: version,
              message: "Success Banned Account",
              data: {},
            };
            res.status(200).send(response);
          })
          .catch((err) => {
            // console.log(err);
            const response = {
              error: true,
              status_maintenance: status_maintenance,
              version: version,
              message: "Something Wrong",
              data: {},
            };
            res.status(400).send(response);
          });
      } else {
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "Authentication not valid",
          data: {},
        };
        res.status(400).send(response);
      }
    })
    .catch((err) => {
      // console.log(err);
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.follow_unfollow_user = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const { user_id } = req.body;
  // console.log(user_id);
  UsersFollow.findOne({
    where: {
      user_id: user.user_id,
      user_id_followed: user_id,
    },
  }).then((dataFollowed) => {
    if (dataFollowed) {
      UsersFollow.destroy({
        where: {
          user_id: user.user_id,
          user_id_followed: user_id,
        },
      })
        .then((FollowCreated) => {
          const response = {
            error: false,
            status_maintenance: status_maintenance,
            version: version,
            message: "Success Unfollow Account",
            data: {},
          };
          res.status(200).send(response);
        })
        .catch((err) => {
          // console.log(err);
          const response = {
            error: true,
            status_maintenance: status_maintenance,
            version: version,
            message: err,
            data: {},
          };
          res.status(400).send(response);
        });
    } else {
      UsersFollow.create({
        user_id: user.user_id,
        user_id_followed: user_id,
      })
        .then((FollowCreated) => {
          let username = user.username;
          Notification.create({
            user_id: user_id,
            other_user_id: user.user_id,
            type: "follow",
            activity_id: 0,
            title:
              username.replace(/\w\S*/g, function (txt) {
                return (
                  txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
                );
              }) + " Mengikuti Anda ",
            body: "",
          });

          Users.findOne({
            where: {
              user_id: user_id,
            },
          }).then((dataowner) => {
            let username = user.username;
            fcm.send(
              dataowner.token_fcm,
              username.replace(/\w\S*/g, function (txt) {
                return (
                  txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
                );
              }) + " Mengikuti Anda ",
              username.replace(/\w\S*/g, function (txt) {
                return (
                  txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase()
                );
              }) + " Mengikuti Anda "
            );
          });

          const response = {
            error: false,
            status_maintenance: status_maintenance,
            version: version,
            message: "Success Follow Account",
            data: {},
          };
          res.status(200).send(response);
        })
        .catch((err) => {
          console.log(err);
          const response = {
            error: true,
            status_maintenance: status_maintenance,
            version: version,
            message: "Something Wrong",
            data: {},
          };
          res.status(400).send(response);
        });
    }
  });
};

exports.list_user_follower = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const { user_id } = req.query;
  Users.hasMany(UsersFollow, { foreignKey: "user_id" });
  UsersFollow.belongsTo(Users, {
    foreignKey: "user_id",
  });
  UsersFollow.findAll({
    where: {
      user_id_followed: user_id,
    },
    include: [
      {
        model: Users,
        required: true,
        attributes: {
          exclude: [
            "password",
            "email",
            "phone",
            "token_fcm",
            "uid_phone",
            "token_app",
          ],
        },
      },
    ],
  })
    .then((dataUser) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List User Follower",
        data: dataUser,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.list_user_following = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const { user_id } = req.query;
  Users.hasMany(UsersFollow, { foreignKey: "user_id" });
  UsersFollow.belongsTo(Users, {
    foreignKey: "user_id_followed",
  });
  UsersFollow.findAll({
    where: {
      user_id: user_id,
    },
    include: [
      {
        model: Users,
        required: true,
        attributes: {
          exclude: [
            "password",
            "email",
            "phone",
            "token_fcm",
            "uid_phone",
            "token_app",
          ],
        },
      },
    ],
  })
    .then((dataUser) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List User Following",
        data: dataUser,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.checker = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const { username, email, phone } = req.query;

  Users.findOne({
    where: {
      [Op.or]: [
        {
          username: {
            [Op.eq]: username,
          },
        },
        {
          email: {
            [Op.eq]: email,
          },
        },
        {
          phone: {
            [Op.eq]: phone,
          },
        },
      ],
    },
  }).then((dataChecker) => {
    // console.log(dataChecker);
    if (dataChecker) {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "User already taken",
        data: {},
      };
      res.status(400).send(response);
    } else {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "User available",
        data: {},
      };
      res.send(response);
    }
  });
};

exports.tester_fcm = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  const { user_id } = req.body;
  Users.findOne({
    where: {
      user_id: user_id,
    },
  }).then((dataUser) => {
    fcm.send(
      dataUser.token_fcm,
      "Tester Notification ",
      "Tester Notification "
    );
    const response = {
      error: false,
      status_maintenance: status_maintenance,
      version: version,
      message: "Success Send Tester",
      data: {},
    };
    res.send(response);
  });
};

exports.searchUser = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let user = res.locals.user;
  const { username } = req.body;
  console.log(user);

  Users.findAll({
    where: {
      [Op.or]: [
        {
          username: {
            [Op.like]: `%${username}%`,
          },
        },
        {
          fullname: {
            [Op.like]: `%${username}%`,
          },
        },
      ],
      username: {
        [Op.ne]: `${user.username}`,
      },
      fullname: {
        [Op.ne]: `${user.username}`,
      },
    },
    attributes: {
      exclude: [
        "password",
        "email",
        "phone",
        "token_fcm",
        "uid_phone",
        "token_app",
      ],
    },
    limit: 10,
  })
    .then((dataUser) => {
      console.log(dataUser);
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "Result Search",
        data: dataUser,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.update_version = (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;

  Version.update(
    {
      version: req.body.version,
    },
    {
      where: { id: 1 },
    }
  ).then((update) => {
    const response = {
      error: false,
      status_maintenance: status_maintenance,
      version: version,
      message: "Success set version :" + req.body.version,
      data: {},
    };
    res.send(response);
  });
};

exports.maintenant_update = (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;

  Maintenance.update(
    {
      maintenance: req.body.maintenance,
    },
    {
      where: { id: 1 },
    }
  ).then((update) => {
    const response = {
      error: false,
      status_maintenance: status_maintenance,
      version: version,
      message: "Success set maintenance :" + req.body.maintenance,
      data: {},
    };
    res.send(response);
  });
};
