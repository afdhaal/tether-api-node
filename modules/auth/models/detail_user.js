const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

//Table primary User
const User = sequelize.define("detail_user", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  userId: {
    type: Sequelize.INTEGER,
    unique: true
  },
  fullName: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  sex: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  phone: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  fullAddress: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  email: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  identityCardNumber: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  birthdate: {
    type: Sequelize.DATEONLY,
    defaultValue: Sequelize.NOW
  },
  photoIdentityCard: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  selfieIdentityCard: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  address_id: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = User;
