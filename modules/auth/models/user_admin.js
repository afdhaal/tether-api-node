const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

const User = sequelize.define("user_admin", {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    fullName: {
        type: Sequelize.STRING,
        defaultValue: ""
    },
    email: {
        type: Sequelize.STRING,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING
    },
    typeUser: {
        type: Sequelize.STRING
    },
    createdAt: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
    },
    updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
    }
});

module.exports = User;