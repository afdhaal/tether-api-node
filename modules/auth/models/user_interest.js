const Sequelize = require("sequelize");
const Activity = require("../../activity/models/activity");
const sequelize = require("../../../utils/database");

const User_interest = sequelize.define("user_interest", {
  user_interest_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  category_id: {
    type: Sequelize.BIGINT,
    allowNull: false
  },
  user_id: {
    type: Sequelize.BIGINT,
    allowNull: false
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = User_interest;
