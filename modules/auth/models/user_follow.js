const Sequelize = require("sequelize");
const Activity = require("../../activity/models/activity");
const sequelize = require("../../../utils/database");

const User_follow = sequelize.define("user_follow", {
  follow_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  user_id: {
    type: Sequelize.BIGINT,
    allowNull: false
  },
  user_id_followed: {
    type: Sequelize.BIGINT,
    allowNull: false
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

// Users.hasMany(Activity, {
//   foreignKey: "author_id",
//   as: "author_id",
//   onDelete: "CASCADE"
// });

// Users.hasMany(Activity, { foreignKey: "author_id" });

// Users.associate = function(models) {
//   // associations can be defined here
//   Users.hasMany(Activity, {
//     foreignKey: "user_id",
//     as: "activity",
//     onDelete: "CASCADE"
//   });

//   // User.hasMany(models.Comment, {
//   //   foreignKey: "userId",
//   //   as: "comments",
//   //   onDelete: "CASCADE"
//   // });
// };

module.exports = User_follow;
