const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

//Table primary User
const Address = sequelize.define("address", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  userId: {
    type: Sequelize.INTEGER
  },
  recipent_name: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  full_address: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  province_id: {
    type: Sequelize.INTEGER
  },
  city_id: {
    type: Sequelize.INTEGER
  },
  subdistrict_id: {
    type: Sequelize.INTEGER
  },
  postal_code: {
    type: Sequelize.INTEGER,
    defaultValue: 0
  },
  phone: {
    type: Sequelize.STRING,
    defaultValue: ""
  },
  default: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = Address;
