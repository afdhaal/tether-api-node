const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

//Table primary User
const Version = sequelize.define("version", {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  version: {
    type: Sequelize.STRING
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = Version;
