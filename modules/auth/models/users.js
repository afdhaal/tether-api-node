const Sequelize = require("sequelize");
const Activity = require("../../activity/models/activity");
const ActivityJoin = require("../../activity/models/activity_join");
const sequelize = require("../../../utils/database");

//Table primary User
const Users = sequelize.define("user", {
  user_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  fullname: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  username: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
  email: {
    type: Sequelize.STRING,
    allowNull: false,
    unique: true,
  },
  phone: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  password: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  avatar: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  bio: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  link: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  birthdate: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  background_profile: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  verified_email: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  token_fcm: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  uid_phone: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  uid_google: {
    type: Sequelize.TEXT,
    type: Sequelize.STRING,
    defaultValue: "",
  },
  uid_facebook: {
    type: Sequelize.TEXT,
    type: Sequelize.STRING,
    defaultValue: "",
  },
  token_app: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  status_banned: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  gender: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
});

// Users.hasMany(Activity, {
//   foreignKey: "author_id",
//   as: "author_id",
//   onDelete: "CASCADE"
// });

// Users.hasMany(Activity, { foreignKey: "author_id" });

// Users.associate = function(models) {
//   // associations can be defined here
//   Users.hasMany(Activity, {
//     foreignKey: "user_id",
//     as: "activity",
//     onDelete: "CASCADE"
//   });

//   // User.hasMany(models.Comment, {
//   //   foreignKey: "userId",
//   //   as: "comments",
//   //   onDelete: "CASCADE"
//   // });
// };

Users.hasMany(Activity, {
  foreignKey: "author_id",
  as: "activity",
  onDelete: "CASCADE",
});
Users.hasMany(ActivityJoin, {
  foreignKey: "user_id",
  as: "activityjoin",
  onDelete: "CASCADE",
});

module.exports = Users;
