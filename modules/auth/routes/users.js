const express = require("express");
const authController = require("../controllers/users");
const ActivityVisitController = require("../../activity/controllers/activity_visit");
const auth = require("../../../middleware/auth");
const router = express.Router();

router.get("/checker", auth.noLogin, authController.checker);
router.post("/", auth.noLogin, authController.Register);
router.post("/login", auth.noLogin, authController.Login);
router.get("/infouser", auth.isLogin, authController.infoUser);
router.post("/updatefcm", auth.isLogin, authController.updateTokenFcm);
router.post("/edit_profile", auth.isLogin, authController.edit_profile);
router.post("/change_avatar", auth.isLogin, authController.updateAvatar);
router.post("/forgot_pass_otp", auth.noLogin, authController.ChangePasswordOTP);
router.post("/edit_phone", auth.noLogin, authController.ChangePhoneOTP);

//interest
router.post("/interest/add_remove", auth.isLogin, authController.add_interest);
router.get("/interest", auth.isLogin, authController.list_interest);
router.post("/interest/subscribe", auth.isLogin, authController.add_subscribe);

router.get(
  "/list_user_follower",
  auth.noLogin,
  authController.list_user_follower
);
router.get(
  "/list_user_following",
  auth.noLogin,
  authController.list_user_following
);

//visit user
router.get("/visit", auth.noLogin, authController.visit_user);
router.get(
  "/visit/list_activity",
  auth.noLogin,
  ActivityVisitController.list_activity_visit
);
router.get(
  "/visit/list_joined",
  auth.noLogin,
  ActivityVisitController.list_joined_visit
);

//follow unfollow user
router.post(
  "/follow_unfollow",
  auth.isLogin,
  authController.follow_unfollow_user
);

//banned account
router.post("/banned_account", auth.noLogin, authController.banned_account);

//search account
router.post("/search", auth.isLogin, authController.searchUser);

//tester notification
router.post("/tester", auth.noLogin, authController.tester_fcm);
router.post("/update_version", auth.noLogin, authController.update_version);
router.post(
  "/update_maintenance",
  auth.noLogin,
  authController.maintenant_update
);

module.exports = router;
