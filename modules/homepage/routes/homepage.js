const express = require("express");
const homeController = require("../controllers/homepage");
const auth = require("../../../middleware/auth");
const router = express.Router();

router.get("/", auth.noLogin, homeController.homepage);

module.exports = router;
