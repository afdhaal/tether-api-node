const Users = require("../../auth/models/users");
const UsersInterest = require("../../auth/models/user_interest");
const Activity = require("../../activity/models/activity");
const Category = require("../../activity/models/category");
const ActivityTag = require("../../activity/models/activity_tag");
const ActivityImage = require("../../activity/models/activity_image");
const ActivityFav = require("../../activity/models/activity_favorites");
const ActivityShare = require("../../activity/models/activity_share");
const ActivityComment = require("../../activity/models/activity_comment");
const NearbyHelper = require("../../../helper/query/nearby");
const searchHelper = require("../../../helper/query/search");
var moment = require("moment");
const sequelize = require("sequelize");

exports.homepage = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let { lat, long } = req.query;
  if (lat == 0 || long == 0 || lat === undefined || long === undefined) {
    long = 106.8249641;
    lat = -6.1753871;
  }
  let now = moment().format("YYYY-MM-DD HH:mm:ss");
  const params = {
    lat: lat,
    long: long,
    limit: 10,
  };
  let popular_hashtag = await ActivityTag.sequelize.query(`
    SELECT COUNT(*) as count ,tag from activity_tags GROUP BY tag order by count desc limit 5
  `);
  // let category = await Category.sequelize.query(`
  //   SELECT *, ( SELECT COUNT(*) from activities WHERE activities.category_id=categories.category_id and activities."date" > '${now}') as count from categories order by count desc
  // `);
  let category = await Category.sequelize.query(`
    SELECT * from categories order by category asc
  `);
  // console.log(trending);

  var user;
  const token = req.token;
  if (token) {
    Users.findOne({
      where: {
        token_app: token,
      },
    })
      .then(async (dataUser) => {
        // console.log("cuk");

        if (dataUser) {
          let params1 = {
            keyword: "",
            category_id: "",
            city_id: "",
            page: 0,
            lat: lat,
            long: long,
            sort: "lastest",
            filter: "near",
            user_id: dataUser.user_id,
          };
          let nearby = await searchHelper.search_activity_raw_login(params1);

          let params2 = {
            keyword: "",
            category_id: "",
            city_id: "",
            page: 0,
            lat: lat,
            long: long,
            filter: "view",
            sort: "lastest",
            user_id: dataUser.user_id,
          };

          let trending = await searchHelper.search_activity_raw_login(params2);

          let interest = await UsersInterest.findAll({
            where: {
              user_id: dataUser.user_id,
            },
          }).then(async (interest) => {
            return await interest.map((interest) => interest.category_id);
          });
          console.log(interest);

          let params3 = {
            keyword: "",
            category_id: interest,
            city_id: "",
            page: 0,
            lat: lat,
            long: long,
            sort: "view",
          };
          let interestActivity = [];
          if (interest.length != 0) {
            interestActivity = await searchHelper.search_by_interest(params3);
          } else {
            interestActivity[0] = [];
          }
          let myActivity = await Activity.sequelize.query(`
          SELECT  DISTINCT ON ( al.activity_id ) al.activity_id,
        al.*, districts.city_name, CASE
					WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${dataUser.user_id})>0 THEN true
					ELSE false
				END
        AS is_joined,
        CASE
					WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${dataUser.user_id})>0 THEN true
					ELSE false
				END
        AS is_favorited, users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (select count(*) from activity_join_approvals where al.activity_id=activity_join_approvals.activity_id) as count_request_join
        FROM
            (
            SELECT
                *,
                round( CAST(float8 (
                    3959 * acos(
                        cos( radians( ${lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${long} ) ) + sin( radians( ${lat} ) ) * sin( radians( lat ) ) 
                    )
                ) as numeric)*1.60934, 2)
                AS distance
            FROM
                activities
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
						LEFT JOIN activity_joins on activity_joins.activity_id=al.activity_id
            where al.active = true and al.date >= '${now}' and author_id=${dataUser.user_id} or (  al.date >= '${now}' and  activity_joins.user_id=${dataUser.user_id})
        ORDER BY
            activity_id DESC
            OFFSET 0*10
            LIMIT 10;
        `);
          console.log(interest);
          const response = {
            error: false,
            message: "homepage",
            status_maintenance: status_maintenance,
            version: version,
            data: {
              category: category[0],
              nearby: nearby[0],
              trending: trending[0],
              recommended: interestActivity[0],
              popular_hashtag: popular_hashtag[0],
              myActivity: myActivity[0],
            },
          };
          res.send(response);
        } else {
          console.log("cuk");
          // console.log(popular_hashtag[0]);

          // const response = {
          //   error: false,
          //   message: "homepage",
          //  status_maintenance: status_maintenance,
          // version: version,
          //   data: {
          //     nearby: nearby[0],
          //     trending: trending[0],
          //     recommended: [],
          //     popular_hashtag: popular_hashtag[0],
          //     myActivity: []
          //   }
          // };
          const response = {
            error: true,
            message: "Token not verified !!",
            data: {},
          };
          res.status(400).send(response);
        }
      })
      .catch((err) => {
        console.log(err);
        const response = {
          error: true,
          message: "Something Wrong",
          status_maintenance: status_maintenance,
          version: version,
          data: {},
        };
        res.status(400).send(response);
      });
  } else {
    let params1 = {
      keyword: "",
      category_id: "",
      city_id: "",
      page: 0,
      lat: lat,
      long: long,
      sort: "lastest",
      filter: "near",
    };
    let nearby = await searchHelper.search_activity_raw(params1);

    let params2 = {
      keyword: "",
      category_id: "",
      city_id: "",
      page: 0,
      lat: lat,
      long: long,
      filter: "view",
      sort: "lastest",
    };

    let trending = await searchHelper.search_activity_raw(params2);

    const response = {
      error: false,
      message: "homepage",
      status_maintenance: status_maintenance,
      version: version,
      data: {
        category: category[0],
        nearby: nearby[0],
        trending: trending[0],
        recommended: [],
        popular_hashtag: popular_hashtag[0],
        myActivity: [],
      },
    };
    res.send(response);
  }
};
