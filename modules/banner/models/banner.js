const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

const Banner = sequelize.define("banner", {
  banner_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  link: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  description: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
});

module.exports = Banner;
