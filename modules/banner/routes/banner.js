const express = require("express");
const bannerControllers = require("../controllers/banner");
const auth = require("../../../middleware/auth");
const router = express.Router();

router.get("/", auth.noLogin, bannerControllers.banner_list);

module.exports = router;
