const app = require("express");
var async = require("async");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const Banner = require("../models/banner");
const Countries = require("../models/banner");
const Sequelize = require("sequelize");

exports.banner_list = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  Banner.findAll()
    .then((dataBanner) => {
      //console.log(dataLocation[0]);
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List Banner",
        data: dataBanner,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.district_popular = (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  District.sequelize
    .query(
      `
      Select *, (select count(*) from activities where activities.city_id=districts.city_id) as count
      from districts order by count desc limit 5
    `
    )
    .then((dataPopular) => {
      console.log(dataPopular[0]);
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List city popular",
        data: dataPopular[0],
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
  // District.findAll()
  //   .then(dataCity => {
  //     //console.log(dataLocation[0]);
  //     const response = {
  //       error: false,
  //       status_maintenance: status_maintenance,
  // version: version,
  //       message: "List city popular",
  //       data: dataCity
  //     };
  //     res.send(response);
  //   })
  //   .catch(err => {
  //     const response = {
  //       error: true,
  //       status_maintenance: status_maintenance,
  // version: version,
  //       message: "Something Wrong",
  //       data: {}
  //     };
  //     res.status(400).send(response);
  //   });
};
