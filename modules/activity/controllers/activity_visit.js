const app = require("express");
const jwt = require("jsonwebtoken");
const base64Img = require("base64-img");
const Users = require("../../auth/models/users");
const Activity = require("../models/activity");
const Category = require("../models/category");
const ActivityImage = require("../models/activity_image");
const ActivityRating = require("../models/activity_rating");
const ActivityFav = require("../models/activity_favorites");
const ActivityShare = require("../models/activity_share");
const ActivityComment = require("../models/activity_comment");
const ActivityDiscussion = require("../models/activity_discussion");
const ActivityInvitation = require("../models/activity_invitation");
const ActivityJoin = require("../models/activity_join");
const ActivityNote = require("../models/activity_note");
const ActivityJoinApproval = require("../models/activity_join_approval");
const ActivityJoinBlacklist = require("../models/activity_join_blacklist");
const Tag = require("../models/activity_tag");
const City = require("../../locations/models/district");
const Notification = require("../../notifications/models/notification");
const TypeNotification = require("../../notifications/models/type_notification");
const responses = require("../../../utils/responses");
const Sequelize = require("sequelize");
const Op = require("sequelize").Op;
var slug = require("slug");
var print = console.log.bind(console, ">");
const fs = require("fs");
const path = require("path");
const fcm = require("../../../utils/fcmSend");
const nodemailer = require("../../../utils/nodemailer");
const admin = require("firebase-admin");
const moment = require("moment");

exports.list_activity_visit = async (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let user = res.locals.user;

  let { page, lat, long, user_id } = req.query;
  if (
    lat == 0 ||
    long == 0 ||
    lat === undefined ||
    long === undefined ||
    page === undefined
  ) {
    long = 106.8249641;
    lat = -6.1753871;
    if (page != 0 || page != undefined) {
      page = page;
    } else {
      page = 0;
    }
  }
  const token = req.token;
  if (token) {
    Users.findOne({
      where: {
        token_app: token,
      },
    })
      .then(async (dataUser) => {
        let now = moment().format("YYYY-MM-DD HH:mm:ss");
        let activity = await Activity.sequelize.query(`
        SELECT
        activities.*,
        districts.city_name,
        (SELECT calculate_distance(${long}, ${lat}, activities.long, activities.lat, 'K')) as distance,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id AND activity_joins.user_id = ${dataUser.user_id} ) > 0 THEN
        TRUE
          WHEN activities.author_id = ${dataUser.user_id} THEN
        TRUE ELSE FALSE
        END AS is_joined,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activity_join_approvals.activity_id = activities.activity_id AND activity_join_approvals.user_id = ${dataUser.user_id} ) > 0 THEN
        TRUE
          WHEN activities.author_id = ${dataUser.user_id} THEN
        TRUE ELSE FALSE
        END AS is_requested,
      CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id AND activity_favorites.user_id = ${dataUser.user_id} ) > 0 THEN
        TRUE ELSE FALSE
        END AS is_favorited,
        ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activities.activity_id = activity_join_approvals.activity_id ) AS count_request_join,
        users.username AS author_name,
        users.avatar,
        category AS category_name,
        COALESCE ( tag, '[]' ) AS tag,
        COALESCE ( activity_images.image, '[]' ) AS image,
        COALESCE ( list_user_join.username, '[]' ) AS list_user_join,
        ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) AS favorite,
        ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id ) AS user_joined,
        ( SELECT COUNT ( * ) FROM activity_comments WHERE activity_comments.activity_id = activities.activity_id ) AS count_comment,
        (
          ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + activities.VIEW 
        ) AS count_all
      FROM
        activities
        JOIN users ON users.user_id = activities.author_id
        LEFT JOIN districts ON districts.city_id = activities.city_id
        JOIN categories ON categories.category_id = activities.category_id
        LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'tag', activity_tags.tag ) ) AS tag FROM activity_tags WHERE activity_tags.activity_id = activities.activity_id ) activity_tags
        ON TRUE LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) activity_images
        ON TRUE LEFT JOIN LATERAL (
        SELECT
          json_agg ( json_build_object ( 'user_id', users.user_id, 'username', users.username, 'avatar', users.avatar ) ) AS username 
        FROM
          activity_joins
          JOIN users ON users.user_id = activity_joins.user_id
        WHERE
          activity_joins.activity_id = activities.activity_id
        ) list_user_join ON TRUE
      WHERE
        activities.active = true and activities.author_id= ${user_id}
        ORDER BY
        activity_id DESC
        OFFSET ${page}*10
      LIMIT 10;
      `);
        const response = {
          error: false,
          message: "List Activity Visit",
          data: activity[0],
        };
        res.status(200).send(response);
      })
      .catch((err) => {
        console.log(err);
        const response = {
          error: true,
          status_maintenance: status_maintenance,
          version: version,
          message: "User not found",
          data: {},
        };
        res.status(400).send(response);
      });
  } else {
    let now = moment().format("YYYY-MM-DD HH:mm:ss");
    let activity = await Activity.sequelize.query(`
        SELECT
        activities.*,
        districts.city_name,
        (SELECT calculate_distance(${long}, ${lat}, activities.long, activities.lat, 'K')) as distance,
        ( SELECT false )
        AS is_joined,
        ( SELECT false )
        AS is_requested,
        ( SELECT false )
        AS is_favorited,
        ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activities.activity_id = activity_join_approvals.activity_id ) AS count_request_join,
        users.username AS author_name,
        users.avatar,
        category AS category_name,
        COALESCE ( tag, '[]' ) AS tag,
        COALESCE ( activity_images.image, '[]' ) AS image,
        COALESCE ( list_user_join.username, '[]' ) AS list_user_join,
        ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) AS favorite,
        ( SELECT COUNT ( * ) FROM activity_joins WHERE activity_joins.activity_id = activities.activity_id ) AS user_joined,
        ( SELECT COUNT ( * ) FROM activity_comments WHERE activity_comments.activity_id = activities.activity_id ) AS count_comment,
        (
          ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + ( SELECT COUNT ( * ) FROM activity_favorites WHERE activity_favorites.activity_id = activities.activity_id ) + activities.VIEW
        ) AS count_all
      FROM
        activities
        JOIN users ON users.user_id = activities.author_id
        LEFT JOIN districts ON districts.city_id = activities.city_id
        JOIN categories ON categories.category_id = activities.category_id
        LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'tag', activity_tags.tag ) ) AS tag FROM activity_tags WHERE activity_tags.activity_id = activities.activity_id ) activity_tags
        ON TRUE LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) activity_images
        ON TRUE LEFT JOIN LATERAL (
        SELECT
          json_agg ( json_build_object ( 'user_id', users.user_id, 'username', users.username, 'avatar', users.avatar ) ) AS username
        FROM
          activity_joins
          JOIN users ON users.user_id = activity_joins.user_id
        WHERE
          activity_joins.activity_id = activities.activity_id
        ) list_user_join ON TRUE
      WHERE
        activities.active = true and activities.author_id= ${user_id}
        ORDER BY
        activity_id DESC
        OFFSET ${page}*10
      LIMIT 10;
      `);
    const response = {
      error: false,
      message: "List Activity Visit",
      data: activity[0],
    };
    res.status(200).send(response);
  }
};

exports.list_joined_visit = (req, res, next) => {
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let { lat, long, page, user_id } = req.query;
  if (
    lat == 0 ||
    long == 0 ||
    lat === undefined ||
    long === undefined ||
    page === undefined
  ) {
    long = 106.8249641;
    lat = -6.1753871;
    if (page != 0 || page != undefined) {
      page = page;
    } else {
      page = 0;
    }
  }

  const token = req.token;
  if (token) {
    Users.findOne({
      where: {
        token_app: token,
      },
    }).then(async (dataUser) => {
      Activity.sequelize
        .query(
          `
        SELECT
          al.*,districts.city_name, 
          CASE
              WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${dataUser.user_id})>0 THEN true
              ELSE false
            END
            AS is_joined,
            CASE
              WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${dataUser.user_id})>0 THEN true
              ELSE false
            END
            AS is_favorited, users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
          (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
          (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
          (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment
          FROM
              (
              SELECT
                  *,
                  round( CAST(float8 (
                      3959 * acos(
                          cos( radians( ${lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${long} ) ) + sin( radians( ${lat} ) ) * sin( radians( lat ) ) 
                      )
                  ) as numeric)*1.60934, 2)
                  AS distance
              FROM
                  activities
              ) al
              join users on users.user_id=al.author_id
              join districts on districts.city_id = al.city_id
              join categories on categories.category_id=al.category_id
              LEFT   JOIN LATERAL (
                  SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                  FROM   activity_tags
                  WHERE  activity_tags.activity_id = al.activity_id
              ) activity_tags ON true
              LEFT   JOIN LATERAL (
                  SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                  FROM   activity_images
                  WHERE  activity_images.activity_id = al.activity_id
              ) activity_images ON true
              LEFT JOIN activity_joins on activity_joins.activity_id=al.activity_id
              where activity_joins.user_id=${user_id}
          ORDER BY
              activity_id DESC
              OFFSET ${page}*10
              LIMIT 10;
        `
        )
        .then(async (joinedActivity) => {
          // console.log(myActivity);
          const response = {
            error: false,
            message: "List Activity Joined Visit",
            status_maintenance: status_maintenance,
            version: version,
            data: joinedActivity[0],
          };
          res.send(response);
        })
        .catch((err) => {
          console.log(err);
          const response = {
            error: true,
            message: "Something Wrong",
            status_maintenance: status_maintenance,
            version: version,
            data: {},
          };
          res.status(400).send(response);
        });
    });
  } else {
    Activity.sequelize
      .query(
        `
        SELECT
          al.*,districts.city_name, 
          ( SELECT false )
          AS is_joined,
          ( SELECT false )
          AS is_requested,
          ( SELECT false )
          AS is_favorited, users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
          (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
          (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
          (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment
          FROM
              (
              SELECT
                  *,
                  round( CAST(float8 (
                      3959 * acos(
                          cos( radians( ${lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${long} ) ) + sin( radians( ${lat} ) ) * sin( radians( lat ) ) 
                      )
                  ) as numeric)*1.60934, 2)
                  AS distance
              FROM
                  activities
              ) al
              join users on users.user_id=al.author_id
              join districts on districts.city_id = al.city_id
              join categories on categories.category_id=al.category_id
              LEFT   JOIN LATERAL (
                  SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                  FROM   activity_tags
                  WHERE  activity_tags.activity_id = al.activity_id
              ) activity_tags ON true
              LEFT   JOIN LATERAL (
                  SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                  FROM   activity_images
                  WHERE  activity_images.activity_id = al.activity_id
              ) activity_images ON true
              LEFT JOIN activity_joins on activity_joins.activity_id=al.activity_id
              where activity_joins.user_id=${user_id}
          ORDER BY
              activity_id DESC
              OFFSET ${page}*10
              LIMIT 10;
        `
      )
      .then(async (joinedActivity) => {
        // console.log(myActivity);
        const response = {
          error: false,
          message: "List Activity Joined Visit",
          status_maintenance: status_maintenance,
          version: version,
          data: joinedActivity[0],
        };
        res.send(response);
      })
      .catch((err) => {
        console.log(err);
        const response = {
          error: true,
          message: "Something Wrong",
          status_maintenance: status_maintenance,
          version: version,
          data: {},
        };
        res.status(400).send(response);
      });
  }
};
