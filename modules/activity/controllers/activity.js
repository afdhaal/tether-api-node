const app = require("express");
const jwt = require("jsonwebtoken");
const base64Img = require("base64-img");
const Users = require("../../auth/models/users");
const Activity = require("../models/activity");
const Category = require("../models/category");
const ActivityImage = require("../models/activity_image");
const ActivityRating = require("../models/activity_rating");
const ActivityFav = require("../models/activity_favorites");
const ActivityShare = require("../models/activity_share");
const ActivityComment = require("../models/activity_comment");
const ActivityDiscussion = require("../models/activity_discussion");
const ActivityInvitation = require("../models/activity_invitation");
const ActivityJoin = require("../models/activity_join");
const ActivityNote = require("../models/activity_note");
const ActivityJoinApproval = require("../models/activity_join_approval");
const ActivityJoinBlacklist = require("../models/activity_join_blacklist");
const Tag = require("../models/activity_tag");
const City = require("../../locations/models/district");
const Notification = require("../../notifications/models/notification");
const TypeNotification = require("../../notifications/models/type_notification");
const responses = require("../../../utils/responses");
const Sequelize = require("sequelize");
const Op = require("sequelize").Op;
var slug = require("slug");
var print = console.log.bind(console, ">");
const fs = require("fs");
const path = require("path");
const fcm = require("../../../utils/fcmSend");
const nodemailer = require("../../../utils/nodemailer");
// const mailer = require("../../../helpers/mailer");
// const randomString = require("../../../helpers/genRandomString");
const admin = require("firebase-admin");

exports.createActivity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let timestamp = new Date().getTime();
	let image = req.body.image;
	let tag = req.body.tag;
	let image_name = [];
	let loop_image = 1;
	console.log(tag);
	const {
		title,
		content,
		lat,
		long,
		slot,
		date,
		type,
		online,
		category_id,
		note,
		address,
		link,
		telp,
		whatsapp,
		instagram,
		city_id,
		approval,
		question,
	} = req.body;

	Activity.create({
		title: title,
		content: content,
		author_id: user.user_id,
		lat: lat,
		long: long,
		slot: slot,
		date: date,
		type: type,
		online: online,
		approval: approval,
		note: note,
		question: question,
		address: address,
		type: type,
		link: link,
		telp: telp,
		whatsapp: whatsapp,
		instagram: instagram,
		slug: slug(title + timestamp),
		category_id: category_id,
		tag: tag,
		city_id,
	})
		.then(async createActivity => {
			if (tag) {
				await tag.forEach(async element => {
					console.log(element);
					Tag.create({
						activity_id: createActivity.activity_id,
						user_id: user.user_id,
						tag: element.tag,
					});
				});
			}
			if (image) {
				await image.forEach(async element => {
					let timestamp_local = new Date().getTime();
					if (element.image != "") {
						base64Img.imgSync(
							"data:image/png;base64," + element.image,
							"assets/activity_image",
							timestamp_local,
						);
						let new_image = process.env.VIEW_HOST + "/activity_image/" + timestamp_local + ".png";
						ActivityImage.create({
							activity_id: createActivity.activity_id,
							user_id: user.user_id,
							image: process.env.VIEW_HOST + "/activity_image/" + timestamp_local + ".png",
						});
					}
					loop_image++;
				});
			}
			await TypeNotification.findOne({
				where: {type: "create_activity"},
			}).then(checkType => {
				// console.log(checkType);
				Notification.create({
					user_id: user.user_id,
					title: checkType.title,
					body: checkType.body,
					type: "create",
					activity_id: createActivity.activity_id,
				});
			});
			const response = {
				error: false,
				message: "Success Create Activity",
				status_maintenance: status_maintenance,
				version: version,
				data: createActivity,
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong " + err,
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.editActivity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let timestamp = new Date().getTime();
	let image = req.body.image;
	if (!image) {
		image = [];
	}
	let tag = req.body.tag;
	if (!tag) {
		tag = [];
	}
	let image_name = [];
	let loop_image = 1;
	// let new_image_get = new Function("return [" + image + "];")();
	// let new_tag_get = new Function("return [" + tag + "];")();
	// console.log(new_tag_get);
	// throw "jos";
	const {
		activity_id,
		title,
		content,
		lat,
		long,
		slot,
		date,
		type,
		online,
		link,
		address,
		telp,
		whatsapp,
		instagram,
		category_id,
		note,
		question,
		city_id,
	} = req.body;

	console.log(image.length);
	// throw "jos";

	Activity.findOne({
		where: {
			activity_id: activity_id,
			author_id: user.user_id,
		},
	})
		.then(dataActivity => {
			if (dataActivity) {
				if (tag.length == 0 && image.length == 0) {
					Activity.update(
						{
							title: title,
							content: content,
							author_id: user.user_id,
							lat: lat,
							long: long,
							slot: slot,
							date: date,
							address: address,
							type: type,
							online: online,
							link: link,
							telp: telp,
							whatsapp: whatsapp,
							instagram: instagram,
							note: note,
							question: question,
							category_id: category_id,
							city_id,
						},
						{
							where: {
								activity_id: activity_id,
							},
						},
					)
						.then(async updateActivity => {
							const response = {
								error: false,
								message: "Success Edit Activity",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.send(response);
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
				} else if (tag.length > 0 && image.length == 0) {
					Activity.update(
						{
							title: title,
							content: content,
							author_id: user.user_id,
							lat: lat,
							long: long,
							slot: slot,
							date: date,
							address: address,
							type: type,
							online: online,
							link: link,
							telp: telp,
							telp: telp,
							whatsapp: whatsapp,
							instagram: instagram,
							note: note,
							question: question,
							category_id: category_id,
							city_id,
						},
						{
							where: {
								activity_id: activity_id,
							},
						},
					)
						.then(async updateActivity => {
							Tag.destroy({where: {activity_id: activity_id}});
							await tag.forEach(async element => {
								Tag.create({
									activity_id: activity_id,
									user_id: user.user_id,
									tag: element.tag,
								});
							});

							const response = {
								error: false,
								message: "Success Edit Activity",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.send(response);
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
				} else if (tag.length == 0 && image.length > 0) {
					Activity.update(
						{
							title: title,
							content: content,
							author_id: user.user_id,
							lat: lat,
							long: long,
							slot: slot,
							date: date,
							address: address,
							type: type,
							online: online,
							link: link,
							telp: telp,
							whatsapp: whatsapp,
							instagram: instagram,
							note: note,
							question: question,
							category_id: category_id,
							city_id,
						},
						{
							where: {
								activity_id: activity_id,
							},
						},
					)
						.then(async updateActivity => {
							ActivityImage.destroy({where: {activity_id: activity_id}});
							await image.forEach(async element => {
								let timestamp_local = new Date().getTime();
								if (element.image != "") {
									base64Img.imgSync(
										"data:image/png;base64," + element.image,
										"assets/activity_image",
										timestamp_local,
									);
									let new_image =
										process.env.VIEW_HOST + "/activity_image/" + timestamp_local + ".png";
									ActivityImage.create({
										activity_id: activity_id,
										user_id: user.user_id,
										image: process.env.VIEW_HOST + "/activity_image/" + timestamp_local + ".png",
									});
								}
								loop_image++;
							});
							const response = {
								error: false,
								message: "Success Edit Activity",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.send(response);
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
				} else {
					Activity.update(
						{
							title: title,
							content: content,
							author_id: user.user_id,
							lat: lat,
							long: long,
							slot: slot,
							date: date,
							address: address,
							type: type,
							link: link,
							telp: telp,
							online: online,
							whatsapp: whatsapp,
							instagram: instagram,
							note: note,
							question: question,
							category_id: category_id,
							city_id,
						},
						{
							where: {
								activity_id: activity_id,
							},
						},
					)
						.then(async updateActivity => {
							Tag.destroy({where: {activity_id: activity_id}});
							await tag.forEach(async element => {
								Tag.create({
									activity_id: activity_id,
									user_id: user.user_id,
									tag: element.tag,
								});
							});
							// const pathimage = "./assets/activity_image/1581407428045.png";
							// fs.unlinkSync(pathimage);
							// let list_image = await ActivityImage.findAll({
							//   where: { activity_id: id }
							// });
							// // await list_image.forEach(element => {
							// //   let myStr = element.dataValues.image;
							// //   let strArray = myStr.split("/");
							// //   let pathimage = `../../../assets/activity_image/${strArray[4]}`;
							// //   fs.unlinkSync(pathimage);
							// //   // console.log();
							// // });
							// console.log(list_image);
							// throw "jos";
							ActivityImage.destroy({where: {activity_id: activity_id}});
							await image.forEach(async element => {
								let timestamp_local = new Date().getTime();
								if (element.image != "") {
									base64Img.imgSync(
										"data:image/png;base64," + element.image,
										"assets/activity_image",
										timestamp_local,
									);
									let new_image =
										process.env.VIEW_HOST + "/activity_image/" + timestamp_local + ".png";
									ActivityImage.create({
										activity_id: activity_id,
										user_id: user.user_id,
										image: process.env.VIEW_HOST + "/activity_image/" + timestamp_local + ".png",
									});
								}
								loop_image++;
							});
							const response = {
								error: false,
								message: "Success Edit Activity",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.send(response);
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
				}
			} else {
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.nonActiveActivity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id} = req.body;
	// console.log(activity_id);

	Activity.findOne({
		where: {
			activity_id: activity_id,
			author_id: user.user_id,
		},
	})
		.then(dataActivity => {
			if (dataActivity) {
				Activity.update(
					{
						active: false,
					},
					{
						where: {
							activity_id: activity_id,
							author_id: user.user_id,
						},
					},
				)
					.then(updatedActivity => {
						const response = {
							error: false,
							message: "Success Deactive Activity",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.send(response);
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			} else {
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.getAllMyActivity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let {lat, long, page} = req.query;
	if (lat == 0 || long == 0 || lat === undefined || long === undefined || page === undefined) {
		long = 106.8249641;
		lat = -6.1753871;
		page = 0;
	}
	Activity.sequelize
		.query(
			`
      SELECT
      al.*,districts.city_name, CASE
					WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${user.user_id})>0 THEN true
					ELSE false
				END
        AS is_joined,
        CASE
					WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${user.user_id})>0 THEN true
					ELSE false
				END
        AS is_favorited, users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
      (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
      (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
      (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment
      FROM
          (
          SELECT
              *,
              round( CAST(float8 (
                  3959 * acos(
                      cos( radians( ${lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${long} ) ) + sin( radians( ${lat} ) ) * sin( radians( lat ) ) 
                  )
              ) as numeric)*1.60934, 2)
              AS distance
          FROM
              activities
          ) al
          join users on users.user_id=al.author_id
          join districts on districts.city_id = al.city_id
          join categories on categories.category_id=al.category_id
          LEFT   JOIN LATERAL (
              SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
              FROM   activity_tags
              WHERE  activity_tags.activity_id = al.activity_id
          ) activity_tags ON true
          LEFT   JOIN LATERAL (
              SELECT json_agg(json_build_object('image', activity_images.image)) AS image
              FROM   activity_images
              WHERE  activity_images.activity_id = al.activity_id
          ) activity_images ON true
          where al.author_id=${user.user_id}
      ORDER BY
          activity_id DESC
          OFFSET ${page}*10
          LIMIT 10;
    `,
		)
		.then(async myActivity => {
			console.log(myActivity);
			const response = {
				error: false,
				message: "List My Activity",
				status_maintenance: status_maintenance,
				version: version,
				data: myActivity[0],
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.activityInvitation = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let {activity_id, user_id} = req.body;
	ActivityInvitation.findOne({
		where: {
			user_id: user.user_id,
			activity_id: activity_id,
			invited_user_id: user_id,
		},
	})
		.then(dataInvitation => {
			if (dataInvitation) {
				const response = {
					error: true,
					message: "Already invite",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			} else {
				ActivityInvitation.create({
					user_id: user.user_id,
					activity_id: activity_id,
					invited_user_id: user_id,
				})
					.then(createdInvitation => {
						const response = {
							error: false,
							message: "Success send invitation",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.send(response);
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.listMyInvitation = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let page = req.query.page;
	var moment = require("moment");
	let now = moment().format("YYYY-MM-DD HH:mm:ss");

	ActivityInvitation.sequelize
		.query(
			`
    SELECT
        al.*, districts.city_name, CASE
					WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${user.user_id})>0 THEN true
					ELSE false
				END
        AS is_joined,
        CASE
					WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${user.user_id})>0 THEN true
					ELSE false
				END
        AS is_favorited,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,
        COALESCE( activity_images.image, '[]' ) as image,
        COALESCE( list_user_join.username, '[]' ) as list_user_join,
        (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
        (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
        (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment,
        (
				(select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				(SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) +
				al.view
				) as count_all
        FROM
            (
            SELECT
               activities.*, activity_invitations.activity_invite_id,invited_user_id,
               users.username as invitefromusername, users.avatar as invitefromavatar, activity_invitations.approve as approve
            FROM
                activity_invitations
            JOIN  activities on activities.activity_id=activity_invitations.activity_id
            JOIN  users on users.user_id=activity_invitations.user_id
            ) al
            join users on users.user_id=al.author_id
            join districts on districts.city_id = al.city_id
            join categories on categories.category_id=al.category_id
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
                FROM   activity_tags
                WHERE  activity_tags.activity_id = al.activity_id
            ) activity_tags ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object('image', activity_images.image)) AS image
                FROM   activity_images
                WHERE  activity_images.activity_id = al.activity_id
            ) activity_images ON true
            LEFT   JOIN LATERAL (
                SELECT json_agg(json_build_object(
                  'user_id', users.user_id,
                  'username', users.username,
                  'avatar', users.avatar
                )) AS username
                FROM   activity_joins
                JOIN users on users.user_id=activity_joins.user_id
                WHERE  activity_joins.activity_id = al.activity_id
            ) list_user_join ON true
            where al.invited_user_id= ${user.user_id}
            OFFSET ${page}*10
            LIMIT 10;
    `,
		)
		.then(dataInvitation => {
			// res.send();
			const response = {
				error: false,
				message: "List Invitation",
				status_maintenance: status_maintenance,
				version: version,
				data: dataInvitation[0],
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.approve_invitation = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let activity_invite_id = req.body.activity_invite_id;
	// ActivityJoin.create({
	//   activity_id :
	// });
	ActivityInvitation.findOne({
		where: {
			activity_invite_id: activity_invite_id,
			invited_user_id: user.user_id,
		},
	})
		.then(dataInvitation => {
			ActivityJoin.create({
				activity_id: dataInvitation.activity_id,
				user_id: user.user_id,
			})
				.then(joinActivity => {
					ActivityInvitation.destroy({
						where: {
							activity_invite_id: activity_invite_id,
							invited_user_id: user.user_id,
						},
					})
						.then(destroyInvitation => {
							const response = {
								error: false,
								message: "Success Approve",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.send(response);
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
				})
				.catch(err => {
					console.log(err);
					const response = {
						error: true,
						message: "Something Wrong",
						status_maintenance: status_maintenance,
						version: version,
						data: {},
					};
					res.status(400).send(response);
				});
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.delete_invitation = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let activity_invite_id = req.body.activity_invite_id;
	ActivityInvitation.destroy({
		where: {
			activity_invite_id: activity_invite_id,
			invited_user_id: user.user_id,
		},
	})
		.then(DestroyInvitation => {
			// console.log(err);
			const response = {
				error: false,
				message: "Success Delete Invitation",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.getAllJoinedActivity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let {lat, long, page} = req.query;
	if (lat == 0 || long == 0 || lat === undefined || long === undefined || page === undefined) {
		long = 106.8249641;
		lat = -6.1753871;
		page = 0;
	}
	Activity.sequelize
		.query(
			`
    SELECT
      al.*,districts.city_name, CASE
					WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${user.user_id})>0 THEN true
					ELSE false
				END
        AS is_joined,
        CASE
					WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${user.user_id})>0 THEN true
					ELSE false
				END
        AS is_favorited, users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
      (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
      (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
      (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment
      FROM
          (
          SELECT
              *,
              round( CAST(float8 (
                  3959 * acos(
                      cos( radians( ${lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${long} ) ) + sin( radians( ${lat} ) ) * sin( radians( lat ) ) 
                  )
              ) as numeric)*1.60934, 2)
              AS distance
          FROM
              activities
          ) al
          join users on users.user_id=al.author_id
          join districts on districts.city_id = al.city_id
          join categories on categories.category_id=al.category_id
          LEFT   JOIN LATERAL (
              SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
              FROM   activity_tags
              WHERE  activity_tags.activity_id = al.activity_id
          ) activity_tags ON true
          LEFT   JOIN LATERAL (
              SELECT json_agg(json_build_object('image', activity_images.image)) AS image
              FROM   activity_images
              WHERE  activity_images.activity_id = al.activity_id
          ) activity_images ON true
          LEFT JOIN activity_joins on activity_joins.activity_id=al.activity_id
          where activity_joins.user_id=${user.user_id}
      ORDER BY
          activity_id DESC
          OFFSET ${page}*10
          LIMIT 10;
    `,
		)
		.then(async joinedActivity => {
			// console.log(myActivity);
			const response = {
				error: false,
				message: "List My Activity",
				status_maintenance: status_maintenance,
				version: version,
				data: joinedActivity[0],
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.getActivityComment = async (req, res, next) => {
	let timestamp = new Date().getTime();
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	const activity_id = req.query.activity_id;
	Users.hasMany(ActivityComment, {foreignKey: "user_id"});
	ActivityComment.belongsTo(Users, {
		foreignKey: "user_id",
	});
	ActivityComment.findAll({
		where: {
			activity_id: activity_id,
		},
		include: [
			{
				model: Users,
				required: true,
				attributes: {
					exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
				},
			},
		],
		order: [["activity_comment_id", "DESC"]],
		offset: 0 * 10,
		limit: 5,
	})
		.then(async dataComment => {
			let dataActivity = await Activity.findOne({
				where: {
					activity_id: activity_id,
				},
			});
			// console.log(dataActivity);

			Activity.update(
				{
					view: dataActivity.view + 1,
				},
				{
					where: {
						activity_id: activity_id,
					},
				},
			);
			const response = {
				error: false,
				message: "List Comment Activity",
				status_maintenance: status_maintenance,
				version: version,
				data: dataComment,
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};
exports.getActivity = async (req, res, next) => {
	let timestamp = new Date().getTime();
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let slug = req.params.slug;
	const token = req.token;

	if (token) {
		Users.findOne({
			where: {
				token_app: token,
			},
		})
			.then(dataUsers => {
				if (dataUsers) {
					ActivityJoin.findOne({
						where: {
							user_id: dataUsers.user_id,
						},
					})
						.then(dataJoin => {
							if (dataJoin) {
								console.log(dataJoin);
								Activity.findOne({
									where: {
										slug: slug,
									},
								})
									.then(async dataActivity => {
										//   console.log(dataActivity);
										Activity.update(
											{
												view: dataActivity.view + 1,
											},
											{
												where: {
													slug: slug,
												},
											},
										);
										if (dataActivity) {
											let author = await Users.findOne({
												where: {user_id: dataActivity.author_id},
											});
											console.log(author);
											// throw "y";
											let category = await Category.findOne({
												where: {category_id: dataActivity.category_id},
											});
											let image = await ActivityImage.findAll({
												attributes: ["image"],
												where: {activity_id: dataActivity.activity_id},
											});
											let favorite = await ActivityFav.findAndCountAll({
												where: {activity_id: dataActivity.activity_id},
											});
											let share = await ActivityShare.findAndCountAll({
												where: {activity_id: dataActivity.activity_id},
											});
											let comment = await ActivityComment.findAndCountAll({
												where: {activity_id: dataActivity.activity_id},
											});

											let user_joined = await ActivityJoin.findAndCountAll({
												where: {activity_id: dataActivity.activity_id},
											});

											let is_favorited = await ActivityFav.findAndCountAll({
												where: {activity_id: dataActivity.activity_id, user_id: dataUsers.user_id},
											});

											let is_joined = await ActivityJoin.findAndCountAll({
												where: {activity_id: dataActivity.activity_id, user_id: dataUsers.user_id},
											});

											console.log(is_favorited);

											Users.hasMany(ActivityComment, {foreignKey: "user_id"});
											ActivityComment.belongsTo(Users, {
												foreignKey: "user_id",
											});
											let listcomment = await ActivityComment.findAll({
												where: {
													activity_id: dataActivity.activity_id,
												},
												include: [
													{
														model: Users,
														required: true,
														attributes: {
															exclude: [
																"password",
																"email",
																"phone",
																"token_fcm",
																"uid_phone",
																"token_app",
															],
														},
													},
												],
												order: [["activity_comment_id", "DESC"]],
												offset: 0 * 10,
												limit: 10,
											});

											Users.hasMany(ActivityJoin, {foreignKey: "user_id"});
											ActivityJoin.belongsTo(Users, {
												foreignKey: "user_id",
											});

											let listjoined = await ActivityJoin.findAll({
												where: {
													activity_id: dataActivity.activity_id,
												},
												include: [
													{
														model: Users,
														required: true,
														attributes: {
															exclude: [
																"password",
																"email",
																"phone",
																"token_fcm",
																"uid_phone",
																"token_app",
															],
														},
													},
												],
												offset: 0 * 10,
												limit: 10,
											});
											let city = await City.findOne({
												where: {city_id: dataActivity.city_id},
											});
											let tag = await Tag.findAll({
												attributes: ["tag"],
												where: {activity_id: dataActivity.activity_id},
											});
											let note = await ActivityNote.findOne({
												where: {
													activity_id: dataActivity.activity_id,
													priority: true,
												},
											});
											if (note == null) {
												note = {
													activity_note_id: 0,
													activity_id: 0,
													user_id: 0,
													note: "",
													color: "",
													priority: true,
													createdAt: "0000-00-00T00:00:00.000Z",
													updatedAt: "0000-00-00T00:00:00.000Z",
												};
											}
											// console.log(city);
											const response = {
												error: false,
												message: "Activity Detail",
												status_maintenance: status_maintenance,
												version: version,
												data: {
													activity_id: dataActivity.activity_id,
													avatar: author.avatar,
													title: dataActivity.title,
													content: dataActivity.content,
													image: image,
													lat: dataActivity.lat,
													long: dataActivity.long,
													date: dataActivity.date,
													author_id: dataActivity.author_id,
													author_name: author.username,
													category_id: dataActivity.category_id,
													category_name: category.category,
													city_id: dataActivity.city_id,
													city_name: city.city_name + ", " + city.province,
													address: dataActivity.address,
													type: dataActivity.type,
													link: dataActivity.link,
													telp: dataActivity.telp,
													whatsapp: dataActivity.whatsapp,
													instagram: dataActivity.instagram,
													favorite: favorite.count,
													share: dataActivity.share,
													is_favorited: is_favorited.count == 1 ? true : false,
													is_joined: is_joined.count == 1 ? true : false,
													comment: comment.count,
													view: dataActivity.view,
													tag: tag,
													listcomment: listcomment,
													listjoined: listjoined,
													note: note,
													public: dataActivity.public,
													// joined: true,
													user_joined: user_joined.count,
													slot: dataActivity.slot,
													slug: dataActivity.slug,
													createdAt: dataActivity.createdAt,
												},
											};
											res.send(response);
										} else {
											const response = {
												error: true,
												message: "Activity Detail Not Found",
												status_maintenance: status_maintenance,
												version: version,
												data: {},
											};
											res.status(400).send(response);
										}
									})
									.catch(err => {
										console.log(err);
										const response = {
											error: true,
											message: "Something Wrong",
											status_maintenance: status_maintenance,
											version: version,
											data: {},
										};
										res.status(400).send(response);
									});
							} else {
								Activity.findOne({
									where: {
										slug: slug,
									},
								})
									.then(async dataActivity => {
										//   console.log(dataActivity);
										Activity.update(
											{
												view: dataActivity.view + 1,
											},
											{
												where: {
													slug: slug,
												},
											},
										);
										if (dataActivity) {
											let author = await Users.findOne({
												where: {user_id: dataActivity.author_id},
											});
											console.log(author);
											// throw "y";
											let category = await Category.findOne({
												where: {category_id: dataActivity.category_id},
											});
											let image = await ActivityImage.findAll({
												attributes: ["image"],
												where: {activity_id: dataActivity.activity_id},
											});
											let favorite = await ActivityFav.findAndCountAll({
												where: {activity_id: dataActivity.activity_id},
											});
											let share = await ActivityShare.findAndCountAll({
												where: {activity_id: dataActivity.activity_id},
											});
											let comment = await ActivityComment.findAndCountAll({
												where: {activity_id: dataActivity.activity_id},
											});

											let user_joined = await ActivityJoin.findAndCountAll({
												where: {activity_id: dataActivity.activity_id},
											});

											let is_favorited = await ActivityFav.findAndCountAll({
												where: {activity_id: dataActivity.activity_id, user_id: dataUsers.user_id},
											});

											let is_joined = await ActivityJoin.findAndCountAll({
												where: {activity_id: dataActivity.activity_id, user_id: dataUsers.user_id},
											});

											Users.hasMany(ActivityComment, {foreignKey: "user_id"});
											ActivityComment.belongsTo(Users, {
												foreignKey: "user_id",
											});
											let listcomment = await ActivityComment.findAll({
												where: {
													activity_id: dataActivity.activity_id,
												},
												include: [
													{
														model: Users,
														required: true,
														attributes: {
															exclude: [
																"password",
																"email",
																"phone",
																"token_fcm",
																"uid_phone",
																"token_app",
															],
														},
													},
												],
												order: [["activity_comment_id", "DESC"]],
												offset: 0 * 10,
												limit: 10,
											});

											Users.hasMany(ActivityJoin, {foreignKey: "user_id"});
											ActivityJoin.belongsTo(Users, {
												foreignKey: "user_id",
											});

											let listjoined = await ActivityJoin.findAll({
												where: {
													activity_id: dataActivity.activity_id,
												},
												include: [
													{
														model: Users,
														required: true,
														attributes: {
															exclude: [
																"password",
																"email",
																"phone",
																"token_fcm",
																"uid_phone",
																"token_app",
															],
														},
													},
												],
												offset: 0 * 10,
												limit: 10,
											});
											let city = await City.findOne({
												where: {city_id: dataActivity.city_id},
											});
											let tag = await Tag.findAll({
												attributes: ["tag"],
												where: {activity_id: dataActivity.activity_id},
											});
											let note = await ActivityNote.findOne({
												where: {
													activity_id: dataActivity.activity_id,
													priority: true,
												},
											});
											if (note == null) {
												note = {
													activity_note_id: 0,
													activity_id: 0,
													user_id: 0,
													note: "",
													color: "",
													priority: true,
													createdAt: "0000-00-00T00:00:00.000Z",
													updatedAt: "0000-00-00T00:00:00.000Z",
												};
											}
											// console.log(city);
											const response = {
												error: false,
												message: "Activity Detail",
												status_maintenance: status_maintenance,
												version: version,
												data: {
													activity_id: dataActivity.activity_id,
													avatar: author.avatar,
													title: dataActivity.title,
													content: dataActivity.content,
													image: image,
													lat: dataActivity.lat,
													long: dataActivity.long,
													date: dataActivity.date,
													author_id: dataActivity.author_id,
													author_name: author.username,
													category_id: dataActivity.category_id,
													category_name: category.category,
													city_id: dataActivity.city_id,
													city_name: city.city_name + ", " + city.province,
													address: dataActivity.address,
													type: dataActivity.type,
													link: dataActivity.link,
													telp: dataActivity.telp,
													whatsapp: dataActivity.whatsapp,
													instagram: dataActivity.instagram,
													favorite: favorite.count,
													is_favorited: is_favorited.count == 1 ? true : false,
													is_joined: is_joined.count == 1 ? true : false,
													share: dataActivity.share,
													comment: comment.count,
													view: dataActivity.view,
													tag: tag,
													listcomment: listcomment,
													listjoined: listjoined,
													note: note,
													public: dataActivity.public,
													// joined: false,
													user_joined: user_joined.count,
													slot: dataActivity.slot,
													slug: dataActivity.slug,
													createdAt: dataActivity.createdAt,
												},
											};
											res.send(response);
										} else {
											const response = {
												error: true,
												message: "Activity Detail Not Found",
												status_maintenance: status_maintenance,
												version: version,
												data: {},
											};
											res.status(400).send(response);
										}
									})
									.catch(err => {
										console.log(err);
										const response = {
											error: true,
											message: "Something Wrong",
											status_maintenance: status_maintenance,
											version: version,
											data: {},
										};
										res.status(400).send(response);
									});
							}
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
				} else {
					const response = {
						error: true,
						message: "Token not verified !!",
						data: {},
					};
					res.status(400).send(response);
				}
			})
			.catch(err => {
				console.log(err);
				const response = {
					error: true,
					message: "Something Wrong",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			});
	} else {
		Activity.findOne({
			where: {
				slug: slug,
			},
		})
			.then(async dataActivity => {
				//   console.log(dataActivity);
				Activity.update(
					{
						view: dataActivity.view + 1,
					},
					{
						where: {
							slug: slug,
						},
					},
				);
				if (dataActivity) {
					let author = await Users.findOne({
						where: {user_id: dataActivity.author_id},
					});
					console.log(author);
					// throw "y";
					let category = await Category.findOne({
						where: {category_id: dataActivity.category_id},
					});
					let image = await ActivityImage.findAll({
						attributes: ["image"],
						where: {activity_id: dataActivity.activity_id},
					});
					let favorite = await ActivityFav.findAndCountAll({
						where: {activity_id: dataActivity.activity_id},
					});
					let share = await ActivityShare.findAndCountAll({
						where: {activity_id: dataActivity.activity_id},
					});
					let comment = await ActivityComment.findAndCountAll({
						where: {activity_id: dataActivity.activity_id},
					});

					let user_joined = await ActivityJoin.findAndCountAll({
						where: {activity_id: dataActivity.activity_id},
					});

					Users.hasMany(ActivityComment, {foreignKey: "user_id"});
					ActivityComment.belongsTo(Users, {
						foreignKey: "user_id",
					});
					let listcomment = await ActivityComment.findAll({
						where: {
							activity_id: dataActivity.activity_id,
						},
						include: [
							{
								model: Users,
								required: true,
								attributes: {
									exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
								},
							},
						],
						order: [["activity_comment_id", "DESC"]],
						offset: 0 * 10,
						limit: 10,
					});

					Users.hasMany(ActivityJoin, {foreignKey: "user_id"});
					ActivityJoin.belongsTo(Users, {
						foreignKey: "user_id",
					});

					let listjoined = await ActivityJoin.findAll({
						where: {
							activity_id: dataActivity.activity_id,
						},
						include: [
							{
								model: Users,
								required: true,
								attributes: {
									exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
								},
							},
						],
						offset: 0 * 10,
						limit: 10,
					});
					let city = await City.findOne({
						where: {city_id: dataActivity.city_id},
					});
					let tag = await Tag.findAll({
						attributes: ["tag"],
						where: {activity_id: dataActivity.activity_id},
					});
					let note = await ActivityNote.findOne({
						where: {activity_id: dataActivity.activity_id, priority: true},
					});
					if (note == null) {
						note = {
							activity_note_id: 0,
							activity_id: 0,
							user_id: 0,
							note: "",
							color: "",
							priority: true,
							createdAt: "0000-00-00T00:00:00.000Z",
							updatedAt: "0000-00-00T00:00:00.000Z",
						};
					}
					// console.log(city);
					const response = {
						error: false,
						message: "Activity Detail",
						status_maintenance: status_maintenance,
						version: version,
						data: {
							activity_id: dataActivity.activity_id,
							title: dataActivity.title,
							content: dataActivity.content,
							image: image,
							lat: dataActivity.lat,
							long: dataActivity.long,
							date: dataActivity.date,
							author_id: dataActivity.author_id,
							author_name: author.username,
							avatar: author.avatar,
							category_id: dataActivity.category_id,
							category_name: category.category,
							city_id: dataActivity.city_id,
							city_name: city.city_name + ", " + city.province,
							address: dataActivity.address,
							type: dataActivity.type,
							link: dataActivity.link,
							telp: dataActivity.telp,
							whatsapp: dataActivity.whatsapp,
							instagram: dataActivity.instagram,
							favorite: favorite.count,
							is_favorited: false,
							share: dataActivity.share,
							comment: comment.count,
							view: dataActivity.view,
							tag: tag,
							listcomment: listcomment,
							listjoined: listjoined,
							note: note,
							public: dataActivity.public,
							// joined: false,
							is_joined: false,
							user_joined: user_joined.count,
							slot: dataActivity.slot,
							slug: dataActivity.slug,
							createdAt: dataActivity.createdAt,
						},
					};
					res.send(response);
				} else {
					const response = {
						error: true,
						message: "Activity Detail Not Found",
						status_maintenance: status_maintenance,
						version: version,
						data: {},
					};
					res.status(400).send(response);
				}
			})
			.catch(err => {
				console.log(err);
				const response = {
					error: true,
					message: "Something Wrong",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			});
	}
};

exports.joinActivity = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, message} = req.body;

	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(async dataActivity => {
			if (dataActivity) {
				//if approval true
				if (dataActivity.approval == true) {
					ActivityJoinBlacklist.findOne({
						where: {
							user_id: user.user_id,
							activity_id: activity_id,
						},
					})
						.then(dataBlacklist => {
							if (dataBlacklist) {
								const response = {
									error: true,
									message: "Blocked",
									status_maintenance: status_maintenance,
									version: version,
									data: {},
								};
								res.status(400).send(response);
							} else {
								ActivityJoin.findOne({
									where: {
										user_id: user.user_id,
										activity_id: activity_id,
									},
								}).then(async dataJoined => {
									if (dataJoined) {
										ActivityJoin.destroy({
											where: {
												activity_id: activity_id,
												user_id: user.user_id,
											},
										})
											.then(dataDestroyed => {
												ActivityJoinApproval.destroy({
													where: {
														activity_id: activity_id,
														user_id: user.user_id,
													},
												});
												nodemailer.mailer_activity_leave(user.email, dataActivity.title);
												const response = {
													error: false,
													message: "Success Leave Activity",
													status_maintenance: status_maintenance,
													version: version,
													data: {},
												};
												res.status(200).send(response);
											})
											.catch(err => {
												console.log(err);
												const response = {
													error: true,
													message: "Something Wrong",
													status_maintenance: status_maintenance,
													version: version,
													data: {},
												};
												res.status(400).send(response);
											});
									} else {
										ActivityJoinApproval.findOne({
											where: {
												user_id: user.user_id,
												activity_id: activity_id,
											},
										}).then(async dataJoinedApproval => {
											if (dataJoinedApproval) {
												const response = {
													error: false,
													message: "Already Request Join Activity",
													status_maintenance: status_maintenance,
													version: version,
													data: {},
												};
												res.status(400).send(response);
											} else {
												let slot_used = await ActivityJoin.findAndCountAll({
													where: {
														activity_id: activity_id,
													},
												});
												if (dataActivity.author_id == user.user_id) {
													const response = {
														error: true,
														message: "Owner Can't Joined",
														status_maintenance: status_maintenance,
														version: version,
														data: {},
													};
													res.status(400).send(response);
												} else if (dataActivity.slot == 0) {
													ActivityJoinApproval.create({
														activity_id: activity_id,
														user_id: user.user_id,
														message: message,
													})
														.then(joinActivity => {
															Users.findOne({
																where: {
																	user_id: dataActivity.author_id,
																},
															}).then(dataOwner => {
																fcm.send(
																	user.token_fcm,
																	"Permintaan Bergabung Sukses",
																	dataActivity.title,
																);
																nodemailer.mailer_activity_waiting(user.email, dataActivity, user);
																//notification for owner
																Notification.create({
																	user_id: dataOwner.user_id,
																	other_user_id: user.user_id,
																	title: "Permintaan Bergabung Activity",
																	body:
																		user.username +
																		" Ingin Bergabung Pada Activity " +
																		dataActivity.title,
																	type: "approval",
																	activity_id: activity_id,
																});
																fcm.send(
																	dataOwner.token_fcm,
																	"Permintaan Bergabung Activity",
																	dataActivity.title,
																);
															});
															const response = {
																error: false,
																message: "Request Succcess",
																status_maintenance: status_maintenance,
																version: version,
																data: {},
															};
															res.send(response);
														})
														.catch(err => {
															console.log(err);
															const response = {
																error: true,
																message: "Something Wrong",
																status_maintenance: status_maintenance,
																version: version,
																data: {},
															};
															res.status(400).send(response);
														});
												} else if (slot_used.count <= dataActivity.slot) {
													ActivityJoinApproval.create({
														activity_id: activity_id,
														user_id: user.user_id,
														message: message,
													})
														.then(joinActivity => {
															Users.findOne({
																where: {
																	user_id: dataActivity.author_id,
																},
															}).then(dataOwner => {
																nodemailer.mailer_activity_waiting(user.email, dataActivity, user);
																fcm.send(
																	user.token_fcm,
																	"Permintaan Bergabung Sukses",
																	dataActivity.title,
																);

																//notification for owner
																Notification.create({
																	user_id: dataOwner.user_id,
																	other_user_id: user.user_id,
																	title: "Permintaan Bergabung Activity",
																	body:
																		user.username +
																		" Ingin Bergabung Pada Activity " +
																		dataActivity.title,
																	type: "approval",
																	activity_id: activity_id,
																});
																fcm.send(
																	dataOwner.token_fcm,
																	"Permintaan Bergabung Activity",
																	dataActivity.title,
																);
															});
															const response = {
																error: false,
																message: "Request Succcess",
																status_maintenance: status_maintenance,
																version: version,
																data: {},
															};
															res.send(response);
														})
														.catch(err => {
															console.log(err);
															const response = {
																error: true,
																message: "Something Wrong",
																status_maintenance: status_maintenance,
																version: version,
																data: {},
															};
															res.status(400).send(response);
														});
												} else {
													const response = {
														error: true,
														message: "Slot full",
														status_maintenance: status_maintenance,
														version: version,
														data: {},
													};
													res.status(400).send(response);
												}
											}
										});
									}
								});
							}
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});

					//else not approval
				} else {
					ActivityJoinBlacklist.findOne({
						where: {
							user_id: user.user_id,
							activity_id: activity_id,
						},
					})
						.then(async dataBlocked => {
							if (dataBlocked) {
								const response = {
									error: true,
									message: "Blocked",
									status_maintenance: status_maintenance,
									version: version,
									data: {},
								};
								res.status(400).send(response);
							} else {
								ActivityJoin.findOne({
									where: {
										activity_id: activity_id,
										user_id: user.user_id,
									},
								}).then(async dataJoined => {
									if (dataJoined) {
										ActivityJoin.destroy({
											where: {
												activity_id: activity_id,
												user_id: user.user_id,
											},
										})
											.then(dataDestroyed => {
												nodemailer.mailer_activity_leave(user.email, dataActivity, user);
												const response = {
													error: false,
													message: "Success Leave Activity",
													status_maintenance: status_maintenance,
													version: version,
													data: {},
												};
												res.status(200).send(response);
											})
											.catch(err => {
												console.log(err);
												const response = {
													error: true,
													message: "Something Wrong",
													status_maintenance: status_maintenance,
													version: version,
													data: {},
												};
												res.status(400).send(response);
											});
									} else {
										let slot_used = await ActivityJoin.findAndCountAll({
											where: {
												activity_id: activity_id,
											},
										});
										if (dataActivity.author_id == user.user_id) {
											const response = {
												error: true,
												message: "Owner Can't Joined",
												status_maintenance: status_maintenance,
												version: version,
												data: {},
											};
											res.status(400).send(response);
										} else if (dataActivity.slot == 0) {
											ActivityJoin.create({
												activity_id: activity_id,
												user_id: user.user_id,
												approved: true,
											})
												.then(async joinActivity => {
													Users.findOne({
														where: {
															user_id: dataActivity.author_id,
														},
													}).then(dataOwner => {
														nodemailer.mailer_activity(user.email, dataActivity, user);
														//notification for user
														fcm.send(user.token_fcm, "Berhasil Bergabung", dataActivity.title);
														//notification for owner
														Notification.create({
															user_id: dataOwner.user_id,
															other_user_id: user.user_id,
															title: "User Baru Bergabung",
															body: user.username + " Bergabung di " + dataActivity.title,
															type: "joined",
															activity_id: activity_id,
														});
														fcm.send(
															dataOwner.token_fcm,
															"User Baru Bergabung",
															dataActivity.title,
														);
													});

													const response = {
														error: false,
														message: "Joined Succcess",
														status_maintenance: status_maintenance,
														version: version,
														data: {},
													};
													res.send(response);
												})
												.catch(err => {
													console.log(err);
													const response = {
														error: true,
														message: "Something Wrong",
														status_maintenance: status_maintenance,
														version: version,
														data: {},
													};
													res.status(400).send(response);
												});
										} else if (slot_used.count <= dataActivity.slot) {
											ActivityJoin.create({
												activity_id: activity_id,
												user_id: user.user_id,
												approved: true,
											})
												.then(joinActivity => {
													Users.findOne({
														where: {
															user_id: dataActivity.author_id,
														},
													}).then(dataOwner => {
														fcm.send(user.token_fcm, "Berhasil Bergabung", dataActivity.title);
														//mailer
														nodemailer.mailer_activity(user.email, dataActivity, user);
														//notification for owner
														Notification.create({
															user_id: dataOwner.user_id,
															other_user_id: user.user_id,
															title: "User Baru Bergabung",
															body: user.username + " Joined Activity " + dataActivity.title,
															type: "joined",
															activity_id: activity_id,
														});
														fcm.send(
															dataOwner.token_fcm,
															"User Baru Bergabung",
															dataActivity.title,
														);
													});
													const response = {
														error: false,
														message: "Joined Succcess",
														status_maintenance: status_maintenance,
														version: version,
														data: {},
													};
													res.send(response);
												})
												.catch(err => {
													console.log(err);
													const response = {
														error: true,
														message: "Something Wrong",
														status_maintenance: status_maintenance,
														version: version,
														data: {},
													};
													res.status(400).send(response);
												});
										} else {
											const response = {
												error: true,
												message: "Slot full",
												status_maintenance: status_maintenance,
												version: version,
												data: {},
											};
											res.status(400).send(response);
										}
									}
								});
							}
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
				}
			} else {
				console.log(err);
				const response = {
					error: true,
					message: "Activity Not Found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.listRequestJoinedActivity = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let page = req.query.page;
	if (page == null || (page == "" && page == undefined)) {
		page = 0;
	}
	Users.hasMany(ActivityJoinApproval, {foreignKey: "user_id"});
	ActivityJoinApproval.belongsTo(Users, {foreignKey: "user_id"});
	let activity_id = req.query.activity_id;
	ActivityJoinApproval.findAll({
		where: {
			activity_id: activity_id,
			approved: false,
		},
		include: [
			{
				model: Users,
				required: false,
				attributes: {
					exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
				},
			},
		],
		order: [["activity_join_id", "DESC"]],
		offset: page * 10,
		limit: 10,
	})
		.then(async dataJoined => {
			Activity.findOne({
				where: {
					activity_id: activity_id,
				},
			})
				.then(dataActivity => {
					if (dataActivity) {
						const response = {
							error: false,
							message: "List Joined",
							status_maintenance: status_maintenance,
							version: version,
							data: {
								slot: dataActivity.slot,
								total_request: dataJoined.length,
								list_user_request: dataJoined,
							},
						};
						res.send(response);
					} else {
						const response = {
							error: true,
							message: "Activity not found",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					}
				})
				.catch(err => {
					console.log(err);
					const response = {
						error: true,
						message: "Something Wrong",
						status_maintenance: status_maintenance,
						version: version,
						data: {},
					};
					res.status(400).send(response);
				});
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.approve_request = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, user_id} = req.body;
	ActivityJoinApproval.findOne({
		where: {
			user_id: user_id,
			activity_id: activity_id,
			approved: false,
		},
	})
		.then(async dataRequest => {
			if (dataRequest) {
				Activity.findOne({
					where: {
						activity_id: activity_id,
					},
				}).then(dataActivity => {
					if (dataActivity.author_id == user.user_id) {
						ActivityJoinApproval.update(
							{
								approved: true,
							},
							{
								where: {
									user_id: user_id,
									activity_id: activity_id,
								},
							},
						)
							.then(updated => {
								Users.findOne({
									where: {
										user_id: user_id,
									},
								}).then(dataUser => {
									ActivityJoin.create({
										user_id: user_id,
										activity_id: activity_id,
										approved: true,
									}).then(sucesscreate => {
										//mailer
										nodemailer.approved_mailer(dataUser.email, dataActivity, user);
										Notification.create({
											user_id: dataUser.user_id,
											other_user_id: dataUser.user_id,
											type: "approved_request",
											activity_id: activity_id,
											title: "Permintaan Join Activity Diterima",
											body: dataActivity.title,
										});
										fcm.send(
											dataUser.token_fcm,
											"Permintaan Join Activity Diterima ",
											dataActivity.title,
										);
									});
									const response = {
										error: true,
										message: "Success Approve Join",
										status_maintenance: status_maintenance,
										version: version,
										data: {},
									};
									res.send(response);
								});
							})
							.catch(err => {
								console.log(err);
								const response = {
									error: true,
									message: "Something Wrong",
									status_maintenance: status_maintenance,
									version: version,
									data: {},
								};
								res.status(400).send(response);
							});
					} else {
						const response = {
							error: true,
							message: "Who are you !",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					}
				});
			} else {
				const response = {
					error: true,
					message: "Nothing to Approve",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.decline_request = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, user_id} = req.body;
	ActivityJoinApproval.findOne({
		where: {
			user_id: user_id,
			activity_id: activity_id,
			approved: false,
		},
	})
		.then(async dataRequest => {
			if (dataRequest) {
				Activity.findOne({
					where: {
						activity_id: activity_id,
					},
				}).then(dataActivity => {
					if (dataActivity.author_id == user.user_id) {
						ActivityJoinApproval.destroy({
							where: {
								user_id: user_id,
								activity_id: activity_id,
							},
						})
							.then(updated => {
								Users.findOne({
									where: {
										user_id: user_id,
									},
								}).then(dataUser => {
									Notification.create({
										user_id: dataUser.user_id,
										other_user_id: dataUser.user_id,
										type: "decline_request",
										activity_id: activity_id,
										title: "Permintaan Join Activity Ditolak",
										body: dataActivity.title,
									});
									fcm.send(
										dataUser.token_fcm,
										"Permintaan Join Activity Ditolak",
										dataActivity.title,
									);
									const response = {
										error: true,
										message: "Success Decline Request",
										status_maintenance: status_maintenance,
										version: version,
										data: {},
									};
									res.send(response);
								});
							})
							.catch(err => {
								console.log(err);
								const response = {
									error: true,
									message: "Something Wrong",
									status_maintenance: status_maintenance,
									version: version,
									data: {},
								};
								res.status(400).send(response);
							});
					} else {
						const response = {
							error: true,
							message: "Who are you !",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					}
				});
			} else {
				const response = {
					error: true,
					message: "Nothing to Decline",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.kick_joined_activity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, user_id} = req.body;
	ActivityJoin.findOne({
		where: {
			user_id: user_id,
			activity_id: activity_id,
			approved: true,
		},
	})
		.then(async dataRequest => {
			if (dataRequest) {
				Activity.findOne({
					where: {
						activity_id: activity_id,
					},
				}).then(dataActivity => {
					if (dataActivity.author_id == user.user_id) {
						ActivityJoinApproval.destroy({
							where: {
								user_id: user_id,
								activity_id: activity_id,
							},
						});
						ActivityJoin.destroy({
							where: {
								user_id: user_id,
								activity_id: activity_id,
							},
						})
							.then(updated => {
								Users.findOne({
									where: {
										user_id: user_id,
									},
								}).then(dataUser => {
									Notification.create({
										user_id: dataUser.user_id,
										other_user_id: dataUser.user_id,
										type: "kicked_activity",
										activity_id: activity_id,
										title: "Kamu dikeluarkan dari Activity",
										body: dataActivity.title,
									});
									fcm.send(
										dataUser.token_fcm,
										"Kamu dikeluarkan dari Activity",
										dataActivity.title,
									);
									const response = {
										error: true,
										message: "Success Kick User",
										status_maintenance: status_maintenance,
										version: version,
										data: {},
									};
									res.send(response);
								});
							})
							.catch(err => {
								console.log(err);
								const response = {
									error: true,
									message: "Something Wrong",
									status_maintenance: status_maintenance,
									version: version,
									data: {},
								};
								res.status(400).send(response);
							});
					} else {
						const response = {
							error: true,
							message: "Who are you !",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					}
				});
			} else {
				const response = {
					error: true,
					message: "Nothing to Decline",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.kick_and_block_joined_activity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, user_id} = req.body;
	ActivityJoin.findOne({
		where: {
			user_id: user_id,
			activity_id: activity_id,
			approved: true,
		},
	})
		.then(async dataRequest => {
			if (dataRequest) {
				Activity.findOne({
					where: {
						activity_id: activity_id,
					},
				}).then(dataActivity => {
					if (dataActivity.author_id == user.user_id) {
						ActivityJoinBlacklist.create({
							user_id: user_id,
							activity_id: activity_id,
						});
						ActivityJoin.destroy({
							where: {
								user_id: user_id,
								activity_id: activity_id,
							},
						})
							.then(updated => {
								Users.findOne({
									where: {
										user_id: user_id,
									},
								}).then(dataUser => {
									Notification.create({
										user_id: dataUser.user_id,
										other_user_id: dataUser.user_id,
										type: "kicked_activity",
										activity_id: activity_id,
										title: "Kamu dikeluarkan dari Activity",
										body: dataActivity.title,
									});
									fcm.send(
										dataUser.token_fcm,
										"Kamu dikeluarkan dari Activity",
										dataActivity.title,
									);
									const response = {
										error: true,
										message: "Success Kick User",
										status_maintenance: status_maintenance,
										version: version,
										data: {},
									};
									res.send(response);
								});
							})
							.catch(err => {
								console.log(err);
								const response = {
									error: true,
									message: "Something Wrong",
									status_maintenance: status_maintenance,
									version: version,
									data: {},
								};
								res.status(400).send(response);
							});
					} else {
						const response = {
							error: true,
							message: "Who are you !",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					}
				});
			} else {
				const response = {
					error: true,
					message: "Nothing to Decline",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.listJoinedActivity = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	Users.hasMany(ActivityJoin, {foreignKey: "user_id"});
	ActivityJoin.belongsTo(Users, {foreignKey: "user_id"});
	let activity_id = req.query.activity_id;
	let page = req.query.page;
	if (page == null || (page == "" && page == undefined)) {
		page = 0;
	}
	ActivityJoin.findAll({
		where: {
			activity_id: activity_id,
		},
		include: [
			{
				model: Users,
				required: false,
				attributes: {
					exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
				},
			},
		],
		order: [["activity_join_id", "DESC"]],
		offset: page * 10,
		limit: 10,
		// attributes: {
		//   exclude: ["username"]
		// }
	})
		.then(async dataJoined => {
			Activity.findOne({
				where: {
					activity_id: activity_id,
				},
			})
				.then(dataActivity => {
					if (dataActivity) {
						const response = {
							error: false,
							message: "List Joined",
							status_maintenance: status_maintenance,
							version: version,
							data: {
								slot: dataActivity.slot,
								total_joined: dataJoined.length,
								list_user_join: dataJoined,
							},
						};
						res.send(response);
					} else {
						const response = {
							error: true,
							message: "Activity not found",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					}
				})
				.catch(err => {
					console.log(err);
					const response = {
						error: true,
						message: "Something Wrong",
						status_maintenance: status_maintenance,
						version: version,
						data: {},
					};
					res.status(400).send(response);
				});
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.listJoinedActivityGuest = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	// let user = res.locals.user;
	Users.hasMany(ActivityJoin, {foreignKey: "user_id"});
	ActivityJoin.belongsTo(Users, {foreignKey: "user_id"});
	let activity_id = req.query.activity_id;
	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(dataActivity => {
			if (dataActivity) {
				console.log(dataActivity);
				if (dataActivity.public == true) {
					ActivityJoin.findAll({
						where: {
							activity_id: activity_id,
						},
						include: [
							{
								model: Users,
								required: false,
								attributes: {
									exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
								},
							},
						],
					})
						.then(async dataJoined => {
							const response = {
								error: false,
								message: "List Joined",
								status_maintenance: status_maintenance,
								version: version,
								data: {
									slot: dataActivity.slot,
									total_joined: dataJoined.length,
									list_user_join: dataJoined,
								},
							};
							res.send(response);
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
				} else if (dataActivity.public == false) {
					ActivityJoin.findAll({
						where: {
							activity_id: activity_id,
						},
						include: [
							{
								model: Users,
								required: false,
								attributes: {
									exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
								},
							},
						],
					})
						.then(async dataJoined => {
							const response = {
								error: false,
								message: "List Joined",
								status_maintenance: status_maintenance,
								version: version,
								data: {
									slot: dataActivity.slot,
									total_joined: dataJoined.length,
									list_user_join: dataJoined,
								},
							};
							res.send(response);
						})
						.catch(err => {
							console.log(err);
							const response = {
								error: true,
								message: "Something Wrong",
								status_maintenance: status_maintenance,
								version: version,
								data: {},
							};
							res.status(400).send(response);
						});
					// ActivityJoin.findOne({
					//   where: {
					//     activity_id: activity_id
					//   }
					// })
					//   .then(checkJoined => {
					//     if (checkJoined) {
					//       ActivityJoin.findAll({
					//         where: {
					//           activity_id: activity_id
					//         },
					//         include: [
					//           {
					//             model: Users,
					//             required: false,
					//             attributes: {
					//               exclude: [
					//                 "password",
					//                 "email",
					//                 "phone",
					//                 "token_fcm",
					//                 "uid_phone",
					//                 "token_app"
					//               ]
					//             }
					//           }
					//         ]
					//       })
					//         .then(async dataJoined => {
					//           const response = {
					//             error: false,
					//             message: "List Joined",
					//             status_maintenance: status_maintenance,
					// version:version,
					//             data: {
					//               slot: dataActivity.slot,
					//               total_joined: dataJoined.length,
					//               list_user_join: dataJoined
					//             }
					//           };
					//           res.send(response);
					//         })
					//         .catch(err => {
					//           console.log(err);
					//           const response = {
					//             error: true,
					//             message: "Something Wrong",
					//             status_maintenance: status_maintenance,
					// version:version,
					//             data: {}
					//           };
					//           res.status(400).send(response);
					//         });
					//     } else {
					//       const response = {
					//         error: false,
					//         message: "Not Joined",
					//         status_maintenance: status_maintenance,
					// version:version,
					//         data: {}
					//       };
					//       res.status(400).send(response);
					//     }
					//   })
					//   .catch(err => {
					//     console.log(err);
					//     const response = {
					//       error: true,
					//       message: "Something Wrong",
					//       status_maintenance: status_maintenance,
					// version:version,
					//       data: {}
					//     };
					//     res.status(400).send(response);
					//   });
				}
			} else {
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.favoriteActivity = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let activity_id = req.body.activity_id;
	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(dataActivity => {
			if (dataActivity) {
				ActivityFav.findOne({
					where: {
						activity_id: activity_id,
						user_id: user.user_id,
					},
				})
					.then(dataFavorite => {
						if (dataFavorite) {
							ActivityFav.destroy({
								where: {
									activity_id: activity_id,
									user_id: user.user_id,
								},
							})
								.then(lovedActivity => {
									const response = {
										error: false,
										message: "Success Unfavorite Activity",
										status_maintenance: status_maintenance,
										version: version,
										data: {},
									};
									res.send(response);
								})
								.catch(err => {
									console.log(err);
									const response = {
										error: true,
										message: "Something Wrong",
										status_maintenance: status_maintenance,
										version: version,
										data: {},
									};
									res.status(400).send(response);
								});
						} else {
							ActivityFav.create({
								activity_id: activity_id,
								user_id: user.user_id,
							})
								.then(lovedActivity => {
									const response = {
										error: false,
										message: "Success Favorite Activity",
										status_maintenance: status_maintenance,
										version: version,
										data: {},
									};
									res.send(response);
								})
								.catch(err => {
									console.log(err);
									const response = {
										error: true,
										message: "Something Wrong",
										status_maintenance: status_maintenance,
										version: version,
										data: {},
									};
									res.status(400).send(response);
								});
						}
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			} else {
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.listFavorite = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let activity_id = req.body.activity_id;
	Users.hasMany(Activity, {foreignKey: "author_id"});
	Activity.belongsTo(Users, {foreignKey: "author_id"});

	Activity.hasMany(ActivityFav, {foreignKey: "activity_id"});
	ActivityFav.belongsTo(Activity, {foreignKey: "activity_id"});

	let {lat, long, page} = req.query;
	if (lat == 0 || long == 0 || lat === undefined || long === undefined || page === undefined) {
		long = 106.8249641;
		lat = -6.1753871;
		page = 0;
	}

	Activity.sequelize
		.query(
			`
      SELECT
      al.*,districts.city_name, CASE
					WHEN (SELECT count(*) from activity_joins where activity_joins.activity_id=al.activity_id and activity_joins.user_id=${user.user_id})>0 THEN true
					ELSE false
				END
        AS is_joined,
        CASE
					WHEN (SELECT count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id and activity_favorites.user_id=${user.user_id})>0 THEN true
					ELSE false
				END
        AS is_favorited,
        CASE
          WHEN ( SELECT COUNT ( * ) FROM activity_join_approvals WHERE activity_join_approvals.activity_id = al.activity_id AND activity_join_approvals.user_id = ${user.user_id} ) > 0 THEN
        TRUE
          WHEN al.author_id = ${user.user_id} THEN
        TRUE ELSE FALSE
        END AS is_requested,
        users.username as author_name, users.avatar, category as category_name,COALESCE(tag, '[]' ) as tag,COALESCE( activity_images.image, '[]' ) as image,
      (select count(*) from activity_favorites where activity_favorites.activity_id=al.activity_id) as favorite,
      (select count(*) from activity_joins where activity_joins.activity_id=al.activity_id) as user_joined,
      (select count(*) from activity_comments where activity_comments.activity_id=al.activity_id) as count_comment
      FROM
          (
          SELECT
              *,
              round( CAST(float8 (
                  3959 * acos(
                      cos( radians( ${lat} ) ) * cos( radians( lat ) ) * cos( radians( long ) - radians( ${long} ) ) + sin( radians( ${lat} ) ) * sin( radians( lat ) ) 
                  )
              ) as numeric)*1.60934, 2)
              AS distance
          FROM
              activities
          ) al
          join users on users.user_id=al.author_id
          join districts on districts.city_id = al.city_id
          join categories on categories.category_id=al.category_id
          LEFT   JOIN LATERAL (
              SELECT json_agg(json_build_object('tag', activity_tags.tag)) AS tag
              FROM   activity_tags
              WHERE  activity_tags.activity_id = al.activity_id
          ) activity_tags ON true
          LEFT   JOIN LATERAL (
              SELECT json_agg(json_build_object('image', activity_images.image)) AS image
              FROM   activity_images
              WHERE  activity_images.activity_id = al.activity_id
          ) activity_images ON true
          join activity_favorites on al.activity_id=activity_favorites.activity_id
          where activity_favorites.user_id=${user.user_id}
      ORDER BY
          activity_id DESC
          OFFSET ${page}*10
          LIMIT 10;
    `,
		)
		.then(favoriteActivity => {
			const response = {
				error: false,
				message: "List Favorite Activity",
				status_maintenance: status_maintenance,
				version: version,
				data: favoriteActivity[0],
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};
exports.commentActivity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, comment} = req.body;

	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	}).then(async dataActivity => {
		if (dataActivity) {
			await TypeNotification.findOne({
				where: {type: "success_comment"},
			}).then(dataType => {
				if (dataActivity.author_id != user.user_id) {
					Users.findOne({
						where: {
							user_id: dataActivity.author_id,
						},
					}).then(dataowner => {
						let username = user.username;
						Notification.create({
							user_id: dataowner.user_id,
							other_user_id: user.user_id,
							type: "comment",
							activity_id: activity_id,
							title:
								"Komentar Baru dari " +
								username.replace(/\w\S*/g, function (txt) {
									return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
								}),
							body: comment,
						});
						fcm.send(
							dataowner.token_fcm,
							"Komentar Baru dari " +
								username.replace(/\w\S*/g, function (txt) {
									return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
								}),
							comment,
						);
					});
				}
			});
			ActivityComment.create({
				activity_id: activity_id,
				user_id: user.user_id,
				comment: comment,
			})
				.then(async createComment => {
					ActivityComment.sequelize
						.query(
							`
                SELECT DISTINCT ON (user_id) user_id, * from activity_comments WHERE activity_id=${activity_id}
              `,
						)
						.then(async listUserJoined => {
							listUserJoined[0].forEach(async element => {
								if (element.user_id != dataActivity.author_id) {
									Users.findOne({
										where: {
											user_id: element.user_id,
										},
									}).then(async dataUser => {
										if (element.user_id == user.user_id) {
											// Notification.create({
											//   user_id: dataUser.user_id,
											//   title:
											//     "Anda telah berkomentar pada Activity " +
											//     dataActivity.title,
											//   body: comment
											// });
											// fcm.send(
											//   dataUser.token_fcm,
											//   "Anda telah berkomentar pada activity " +
											//     dataActivity.title,
											//   comment
											// );
										} else {
											let username = user.username;
											Notification.create({
												user_id: dataUser.user_id,
												title:
													"Komentar Baru dari " +
													username.replace(/\w\S*/g, function (txt) {
														return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
													}),
												body: comment,
												type: "comment",
												activity_id: activity_id,
											});
											fcm.send(
												dataUser.token_fcm,
												"Komentar Baru dari " +
													username.replace(/\w\S*/g, function (txt) {
														return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
													}),
												comment,
											);
										}
									});
								}
							});
							console.log(listUserJoined);
						});
					const response = {
						error: false,
						message: "Success Comment Activity",
						status_maintenance: status_maintenance,
						version: version,
						data: {},
					};
					res.send(response);
				})
				.catch(err => {
					console.log(err);
					const response = {
						error: true,
						message: "Something Wrong",
						status_maintenance: status_maintenance,
						version: version,
						data: {},
					};
					res.status(400).send(response);
				});
		} else {
			const response = {
				error: true,
				message: "Activity not Found",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		}
	});
};

exports.listcommentActivity = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, page} = req.query;
	Users.hasMany(ActivityComment, {foreignKey: "user_id"});
	ActivityComment.belongsTo(Users, {foreignKey: "user_id"});

	ActivityComment.findAll({
		attributes: ["activity_comment_id", "activity_id", "comment", "createdAt"],
		where: {
			activity_id: activity_id,
		},
		include: [
			{
				model: Users,
				required: false,
				attributes: {
					exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
				},
			},
		],
		order: [["activity_comment_id", "DESC"]],
		offset: page * 10,
		limit: 10,
	})
		.then(dataComment => {
			const response = {
				error: false,
				message: "List Comment Activity",
				status_maintenance: status_maintenance,
				version: version,
				data: dataComment,
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.discussionActivity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	let timestamp = new Date().getTime();
	const {activity_id, discussion, image, long, lat, loc_string} = req.body;

	let checkOwner = await Activity.findOne({
		where: {
			activity_id: activity_id,
			author_id: user.user_id,
		},
	});
	let checkJoined = await ActivityJoin.findOne({
		where: {
			activity_id: activity_id,
			user_id: user.user_id,
		},
	});
	if (condition) {
		base64Img.imgSync("data:image/png;base64," + image, "assets/avatar", timestamp + "_image");
	}
	if (checkOwner || checkJoined) {
		ActivityDiscussion.create({
			activity_id: activity_id,
			user_id: user.user_id,
			discussion: discussion,
			image: process.env.VIEW_HOST + "/avatar/" + timestamp + "_image.png",
			lat: lat,
			long: long,
			loc_string: loc_string,
		})
			.then(createComment => {
				ActivityComment.sequelize
					.query(
						`
                SELECT DISTINCT ON (user_id) user_id, * from activity_joins WHERE activity_id=${activity_id}
              `,
					)
					.then(async listUserJoined => {
						let dataActivity = await Activity.findOne({
							where: {
								activity_id: activity_id,
							},
						});
						if (dataActivity.author_id != user.user_id) {
							Users.findOne({
								where: {
									user_id: dataActivity.author_id,
								},
							}).then(dataowner => {
								let username = user.username;
								Notification.create({
									user_id: dataowner.user_id,
									other_user_id: user.user_id,
									type: "discussion",
									activity_id: activity_id,

									title:
										"Diskusi Baru dari " +
										username.replace(/\w\S*/g, function (txt) {
											return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
										}),
									body: discussion,
								});
								fcm.send(
									dataowner.token_fcm,
									"Diskusi Baru dari " +
										username.replace(/\w\S*/g, function (txt) {
											return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
										}),
									discussion,
								);
							});
						}
						// console.log(dataActivity);

						listUserJoined[0].forEach(async element => {
							if (element.user_id != dataActivity.author_id) {
								Users.findOne({
									where: {
										user_id: element.user_id,
									},
								}).then(async dataUser => {
									if (element.user_id == user.user_id) {
										// Notification.create({
										//   user_id: dataUser.user_id,
										//   other_user_id: dataActivity.author_id,
										//   title:
										//     "Anda telah berdiskusi pada Activity " +
										//     dataActivity.title,
										//   body: discussion
										// });
										// fcm.send(
										//   dataUser.token_fcm,
										//   "Anda telah berdiskusi pada activity " +
										//     dataActivity.title,
										//   discussion
										// );
									} else {
										let username = user.username;
										Notification.create({
											user_id: dataUser.user_id,
											other_user_id: user.user_id,
											title:
												"Diskusi Baru dari " +
												username.replace(/\w\S*/g, function (txt) {
													return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
												}),
											body: discussion,
											type: "discussion",
											activity_id: activity_id,
										});

										fcm.send(
											dataUser.token_fcm,
											"Diskusi Baru dari " +
												username.replace(/\w\S*/g, function (txt) {
													return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
												}),
											discussion,
										);
									}
								});
							}
						});
						console.log(listUserJoined);
					});
				const response = {
					error: false,
					message: "Success Create Discussion",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.send(response);
			})
			.catch(err => {
				console.log(err);
				const response = {
					error: true,
					message: "Something Wrong",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			});
	} else {
		const response = {
			error: true,
			message: "Not Joined",
			status_maintenance: status_maintenance,
			version: version,
			data: {},
		};
		res.status(400).send(response);
	}
};

exports.listDiscussionActivity = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, page} = req.query;
	Users.hasMany(ActivityDiscussion, {foreignKey: "user_id"});
	ActivityDiscussion.belongsTo(Users, {foreignKey: "user_id"});

	ActivityDiscussion.findAll({
		attributes: ["activity_discussion_id", "activity_id", "discussion", "createdAt"],
		include: [
			{
				model: Users,
				required: false,
				attributes: {
					exclude: ["password", "email", "phone", "token_fcm", "uid_phone", "token_app"],
				},
			},
		],
		where: {
			activity_id: activity_id,
		},
		order: [["activity_discussion_id", "DESC"]],
		offset: page * 10,
		limit: 10,
	})
		.then(dataDiscussion => {
			const response = {
				error: false,
				message: "List Discussion",
				status_maintenance: status_maintenance,
				version: version,
				data: dataDiscussion,
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.list_category = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	Category.findAll({})
		.then(dataCategory => {
			const response = {
				error: false,
				message: "List Category",
				status_maintenance: status_maintenance,
				version: version,
				data: dataCategory,
			};
			res.send(response);
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.shareActivity = (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	const {activity_id} = req.body;
	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(dataActivity => {
			console.log(dataActivity);

			if (dataActivity) {
				Activity.update(
					{
						share: dataActivity.share + 1,
					},
					{
						where: {
							activity_id: activity_id,
						},
					},
				)
					.then(updateShare => {
						const response = {
							error: false,
							message: "Success add +1 share",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.send(response);
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.create_note = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, note, color, priority} = req.body;
	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(dataActivity => {
			if (dataActivity.author_id == user.user_id) {
				ActivityNote.create({
					activity_id: activity_id,
					user_id: user.user_id,
					note: note,
					color: color,
					priority: priority,
				})
					.then(async createdNote => {
						ActivityJoin.findAll({
							where: {activity_id: activity_id},
						}).then(dataJoined => {
							dataJoined.forEach(element => {
								Users.findOne({
									where: {
										user_id: element.user_id,
									},
								}).then(dataUser => {
									nodemailer.create_note(user, dataUser, dataActivity, note);
								});
							});
						});
						const response = {
							error: false,
							message: "created note success",
							status_maintenance: status_maintenance,
							version: version,
							data: createdNote,
						};
						res.send(response);
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			} else {
				console.log(err);
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.update_note = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id, note, color, priority, activity_note_id} = req.body;
	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(dataActivity => {
			if (dataActivity.author_id == user.user_id) {
				ActivityNote.update(
					{
						activity_id: activity_id,
						user_id: user.user_id,
						note: note,
						color: color,
					},
					{
						where: {
							activity_note_id: activity_note_id,
						},
					},
				)
					.then(createdNote => {
						const response = {
							error: false,
							message: "update note success",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.send(response);
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			} else {
				console.log(err);
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.delete_note = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_note_id, activity_id} = req.body;
	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(dataActivity => {
			if (dataActivity.author_id == user.user_id) {
				ActivityNote.destroy({
					where: {
						activity_note_id: activity_note_id,
						user_id: user.user_id,
					},
				})
					.then(deleted => {
						const response = {
							error: false,
							message: "Delete note success",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.send(response);
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			} else {
				console.log(err);
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.set_priority_note = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_note_id, activity_id} = req.body;
	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(async dataActivity => {
			if (dataActivity.author_id == user.user_id) {
				let setallfalase = await ActivityNote.update(
					{
						priority: false,
					},
					{
						where: {
							activity_id: activity_id,
							user_id: user.user_id,
						},
					},
				);
				ActivityNote.update(
					{
						priority: true,
					},
					{
						where: {
							activity_note_id: activity_note_id,
							user_id: user.user_id,
						},
					},
				)
					.then(deleted => {
						const response = {
							error: false,
							message: "Set Priority success",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.send(response);
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			} else {
				console.log(err);
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.get_priority_note = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id} = req.query;
	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(async dataActivity => {
			if (dataActivity) {
				ActivityNote.findOne({
					where: {
						activity_id: activity_id,
						priority: true,
					},
				})
					.then(dataPriority => {
						if (dataPriority) {
							const response = {
								error: false,
								message: "Priority Note",
								status_maintenance: status_maintenance,
								version: version,
								data: dataPriority,
							};
							res.status(200).send(response);
						} else {
							const response = {
								error: false,
								message: "Priority Note",
								status_maintenance: status_maintenance,
								version: version,
								data: {
									activity_note_id: 0,
									activity_id: 0,
									user_id: 0,
									note: "",
									color: "",
									priority: true,
									createdAt: "0000-00-00T00:00:00.000Z",
									updatedAt: "0000-00-00T00:00:00.000Z",
								},
							};
							res.status(200).send(response);
						}
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			} else {
				console.log(err);
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.list_note_activity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	const {activity_id} = req.query;

	let checkJoined = await ActivityJoin.findOne({
		where: {
			user_id: user.user_id,
			activity_id: activity_id,
		},
	});

	Activity.findOne({
		where: {
			activity_id: activity_id,
		},
	})
		.then(dataActivity => {
			if (dataActivity.author_id == user.user_id || checkJoined) {
				ActivityNote.findAll({
					where: {
						activity_id: activity_id,
					},
				})
					.then(createdNote => {
						const response = {
							error: false,
							message: "list note",
							status_maintenance: status_maintenance,
							version: version,
							data: createdNote,
						};
						res.send(response);
					})
					.catch(err => {
						console.log(err);
						const response = {
							error: true,
							message: "Something Wrong",
							status_maintenance: status_maintenance,
							version: version,
							data: {},
						};
						res.status(400).send(response);
					});
			} else {
				const response = {
					error: true,
					message: "Activity not found",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.status(400).send(response);
			}
		})
		.catch(err => {
			console.log(err);
			const response = {
				error: true,
				message: "Something Wrong",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		});
};

exports.get_ended_activity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	var moment = require("moment");
	let now = moment().format("YYYY-MM-DD HH:mm:ss");

	let last_ended_joined = await Activity.sequelize.query(`
    SELECT
      *,
      COALESCE ( image.image, '[]' ) AS image
    FROM
      activity_joins
      JOIN activities ON activity_joins.activity_id = activities.activity_id
      LEFT JOIN LATERAL ( SELECT json_agg ( json_build_object ( 'image', activity_images.image ) ) AS image FROM activity_images WHERE activity_images.activity_id = activities.activity_id ) image ON TRUE 
    WHERE
      user_id = ${user.user_id}
      AND activities."createdAt" < '${now}'
    LIMIT 1;
  `);

	const response = {
		error: false,
		message: "list ended activity",
		status_maintenance: status_maintenance,
		version: version,
		data: last_ended_joined[0],
	};
	res.send(response);
};

exports.review_like_activity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	var moment = require("moment");
	let now = moment().format("YYYY-MM-DD HH:mm:ss");

	ActivityRating.findOne({
		where: {
			activity_id: req.body.activity_id,
			user_id: user.user_id,
		},
	}).then(dataRating => {
		if (dataRating) {
			const response = {
				error: false,
				message: "Activity has been rated",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		} else {
			ActivityRating.create({
				activity_id: req.body.activity_id,
				user_id: user.user_id,
				rating: "like",
			}).then(createdRating => {
				const response = {
					error: false,
					message: "Success like activity",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.send(response);
			});
		}
	});
};

exports.review_dislike_activity = async (req, res, next) => {
	let status_maintenance = res.locals.status_maintenance;
	let version = res.locals.version;
	let user = res.locals.user;
	var moment = require("moment");
	let now = moment().format("YYYY-MM-DD HH:mm:ss");

	ActivityRating.findOne({
		where: {
			activity_id: req.body.activity_id,
			user_id: user.user_id,
		},
	}).then(dataRating => {
		if (dataRating) {
			const response = {
				error: false,
				message: "Activity has been rated",
				status_maintenance: status_maintenance,
				version: version,
				data: {},
			};
			res.status(400).send(response);
		} else {
			ActivityRating.create({
				activity_id: req.body.activity_id,
				user_id: user.user_id,
				rating: "dislike",
			}).then(createdRating => {
				const response = {
					error: false,
					message: "Success like activity",
					status_maintenance: status_maintenance,
					version: version,
					data: {},
				};
				res.send(response);
			});
		}
	});
};
