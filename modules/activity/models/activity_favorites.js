const Sequelize = require("sequelize");
const Users = require("../../auth/models/users");
const sequelize = require("../../../utils/database");

const ActivityFavorite = sequelize.define("activity_favorite", {
  activity_favorite_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  activity_id: {
    type: Sequelize.BIGINT
  },
  user_id: {
    type: Sequelize.BIGINT
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = ActivityFavorite;
