const Sequelize = require("sequelize");
const Users = require("../../auth/models/users");
const sequelize = require("../../../utils/database");

const ActivityDiscussion = sequelize.define("activity_discussion", {
	activity_discussion_id: {
		type: Sequelize.BIGINT,
		autoIncrement: true,
		primaryKey: true,
		allowNull: false,
	},
	activity_id: {
		type: Sequelize.BIGINT,
	},
	user_id: {
		type: Sequelize.BIGINT,
	},
	discussion: {
		type: Sequelize.STRING,
		allowNull: true,
	},
	image: {
		type: Sequelize.STRING,
		allowNull: true,
	},
	long: {
		type: Sequelize.STRING,
		allowNull: true,
	},
	lat: {
		type: Sequelize.STRING,
		allowNull: true,
	},
	loc_string: {
		type: Sequelize.STRING,
		allowNull: true,
	},
	createdAt: {
		allowNull: true,
		type: Sequelize.DATE,
		defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
	},
	updatedAt: {
		allowNull: true,
		type: Sequelize.DATE,
		defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
	},
});

module.exports = ActivityDiscussion;
