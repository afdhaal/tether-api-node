const Sequelize = require("sequelize");
const Users = require("../../auth/models/users");
const sequelize = require("../../../utils/database");

const ActivityRating = sequelize.define("activity_rating", {
  activity_rating_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  activity_id: {
    type: Sequelize.BIGINT,
  },
  user_id: {
    type: Sequelize.BIGINT,
  },
  rating: {
    type: Sequelize.TEXT,
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
});

module.exports = ActivityRating;
