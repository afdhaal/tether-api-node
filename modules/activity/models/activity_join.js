const Sequelize = require("sequelize");
const Users = require("../../auth/models/users");
const sequelize = require("../../../utils/database");

const ActivityJoin = sequelize.define("activity_join", {
  activity_join_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  activity_id: {
    type: Sequelize.BIGINT
  },
  user_id: {
    type: Sequelize.BIGINT
  },
  approved: {
    type: Sequelize.BOOLEAN,
    defaultValue: false
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

ActivityJoin.associate = function(models) {
  // associations can be defined here
  // Activity.hasMany(Users, {
  //   foreignKey: "activityId",
  //   as: "comments",
  //   onDelete: "CASCADE"
  // });
  ActivityJoin.belongsTo(Users, {
    foreignKey: "user_id",
    as: "user_id",
    onDelete: "CASCADE"
  });
};

module.exports = ActivityJoin;
