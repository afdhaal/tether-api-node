const Sequelize = require("sequelize");
const Users = require("../../auth/models/users");
const sequelize = require("../../../utils/database");

const ActivityNote = sequelize.define("activity_note", {
  activity_note_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  activity_id: {
    type: Sequelize.BIGINT,
  },
  user_id: {
    type: Sequelize.BIGINT,
  },
  note: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  color: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  priority: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
});

module.exports = ActivityNote;
