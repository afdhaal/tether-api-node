const Sequelize = require("sequelize");
const Users = require("../../auth/models/users");
const sequelize = require("../../../utils/database");

//Table primary User
const Activity = sequelize.define("activity", {
  activity_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  title: {
    type: Sequelize.STRING,
  },
  content: {
    type: Sequelize.TEXT,
    allowNull: false,
  },
  lat: {
    type: Sequelize.DOUBLE,
    defaultValue: 0,
  },
  long: {
    type: Sequelize.DOUBLE,
    defaultValue: 0,
  },
  date: {
    allowNull: true,
    type: Sequelize.DATE,
  },
  date: {
    allowNull: true,
    type: Sequelize.DATE,
  },
  author_id: {
    allowNull: false,
    type: Sequelize.BIGINT,
  },
  category_id: {
    allowNull: false,
    type: Sequelize.BIGINT,
  },
  city_id: {
    allowNull: false,
    type: Sequelize.INTEGER,
  },
  public: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  approval: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  slug: {
    type: Sequelize.TEXT,
    allowNull: false,
    unique: true,
  },
  question: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  slot: {
    type: Sequelize.INTEGER,
    defaultValue: 0,
  },
  note: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  address: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  type: {
    type: Sequelize.TEXT,
    defaultValue: "activity",
  },
  link: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  telp: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  whatsapp: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  instagram: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  view: {
    type: Sequelize.BIGINT,
    defaultValue: 0,
  },
  share: {
    type: Sequelize.BIGINT,
    defaultValue: 0,
  },
  active: {
    type: Sequelize.BOOLEAN,
    defaultValue: true,
  },
  online: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
});

Activity.associate = function (models) {
  // associations can be defined here
  // Activity.hasMany(Users, {
  //   foreignKey: "activityId",
  //   as: "comments",
  //   onDelete: "CASCADE"
  // });
  Activity.belongsTo(Users, {
    foreignKey: "user_id",
    as: "author_id",
    onDelete: "CASCADE",
  });
};

// Activity.belongsTo(Users, { foreignKey: "userId" });
// Activity.hasOne(Users, { as: "Initiator" });

// Users.hasMany(Activity, {
//   foreignKey: "author_id",
//   as: "author_id",
//   onDelete: "CASCADE"
// });

module.exports = Activity;
