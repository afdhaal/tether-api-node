const Sequelize = require("sequelize");
const Users = require("../../auth/models/users");
const sequelize = require("../../../utils/database");

const ActivityJoinApproval = sequelize.define("activity_join_approval", {
  activity_join_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  activity_id: {
    type: Sequelize.BIGINT,
  },
  user_id: {
    type: Sequelize.BIGINT,
  },
  message: {
    type: Sequelize.TEXT,
  },
  approved: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  blocked: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
});

module.exports = ActivityJoinApproval;
