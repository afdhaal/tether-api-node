const express = require("express");
const activityController = require("../controllers/activity");
const auth = require("../../../middleware/auth");
const router = express.Router();

//activity based
router.post("/create", auth.isLogin, activityController.createActivity);
router.post("/deactive", auth.isLogin, activityController.nonActiveActivity);
router.get("/:slug", auth.noLogin, activityController.getActivity);
router.post("/edit", auth.isLogin, activityController.editActivity);
router.get(
  "/list/myactivity",
  auth.isLogin,
  activityController.getAllMyActivity
);
router.get(
  "/list/joinedactivity",
  auth.isLogin,
  activityController.getAllJoinedActivity
);
router.post("/join", auth.isLogin, activityController.joinActivity);

//category
router.get("/category/all", auth.noLogin, activityController.list_category);

//fav
router.post(
  "/favorite/add_remove",
  auth.isLogin,
  activityController.favoriteActivity
);
router.get("/favorite/list", auth.isLogin, activityController.listFavorite);

//joined activity
router.get(
  "/member/list_join",
  auth.isLogin,
  activityController.listJoinedActivity
);

router.get(
  "/member/view_joined",
  auth.noLogin,
  activityController.listJoinedActivityGuest
);

// request joined activity
router.get(
  "/member/list_request_join",
  auth.isLogin,
  activityController.listRequestJoinedActivity
);

router.post(
  "/approve_request",
  auth.isLogin,
  activityController.approve_request
);
router.post(
  "/decline_request",
  auth.isLogin,
  activityController.decline_request
);

router.post(
  "/kick_user",
  auth.isLogin,
  activityController.kick_joined_activity
);
router.post(
  "/kick_block_user",
  auth.isLogin,
  activityController.kick_and_block_joined_activity
);

router.post("/note/create_note", auth.isLogin, activityController.create_note);
router.post("/note/update_note", auth.isLogin, activityController.update_note);
router.post("/note/delete_note", auth.isLogin, activityController.delete_note);
router.post(
  "/note/set_priority",
  auth.isLogin,
  activityController.set_priority_note
);
router.get(
  "/note/priority",
  auth.noLogin,
  activityController.get_priority_note
);

router.get(
  "/note/list_note",
  auth.isLogin,
  activityController.list_note_activity
);

//comment public
router.post("/comment", auth.isLogin, activityController.commentActivity);
router.get(
  "/comment/last_comment",
  auth.noLogin,
  activityController.getActivityComment
);
router.get(
  "/comment/list_comment",
  auth.noLogin,
  activityController.listcommentActivity
);

//comment private
router.post("/discussion", auth.isLogin, activityController.discussionActivity);
router.get(
  "/discussion/list_discussion",
  auth.isLogin,
  activityController.listDiscussionActivity
);

router.post("/share", auth.noLogin, activityController.shareActivity);

//invitation
router.post("/invite", auth.isLogin, activityController.activityInvitation);
router.get(
  "/invitation/list",
  auth.isLogin,
  activityController.listMyInvitation
);
router.post(
  "/invite/accept",
  auth.isLogin,
  activityController.approve_invitation
);
router.post(
  "/invite/decline",
  auth.isLogin,
  activityController.delete_invitation
);

//get activity joined passed
router.get("/end/joined", auth.isLogin, activityController.get_ended_activity);
router.post("/end/like", auth.isLogin, activityController.review_like_activity);
router.post(
  "/end/dislike",
  auth.isLogin,
  activityController.review_dislike_activity
);

module.exports = router;
