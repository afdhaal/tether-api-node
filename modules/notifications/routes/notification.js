const express = require("express");
const NotificationControllers = require("../controllers/notification");
const auth = require("../../../middleware/auth");
const router = express.Router();

router.get(
  "/list_notification",
  auth.isLogin,
  NotificationControllers.list_notification
);
router.post(
  "/clear_notification",
  auth.isLogin,
  NotificationControllers.clear_notification
);
router.get(
  "/count_notification",
  auth.isLogin,
  NotificationControllers.count_notification
);
router.post(
  "/read_notification",
  auth.isLogin,
  NotificationControllers.read_notification
);
// router.get(
//   "/list_city_popular",
//   auth.noLogin,
//   locationControllers.district_popular
// );

module.exports = router;
