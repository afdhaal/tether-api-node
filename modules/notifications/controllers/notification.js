const app = require("express");
var async = require("async");
const axios = require("axios");
const jwt = require("jsonwebtoken");
const Users = require("../../auth/models/users");
const Notification = require("../models/notification");
const Invitation = require("../../activity/models/activity_invitation");
const Activity = require("../../activity/models/activity");
const Sequelize = require("sequelize");

exports.list_notification = async (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let page = req.query.page;
  if (page == null || (page == "" && page == undefined)) {
    page = 0;
  }
  Users.hasMany(Notification, { foreignKey: "user_id" });
  Notification.belongsTo(Users, {
    foreignKey: "other_user_id",
  });

  Activity.hasMany(Notification, { foreignKey: "activity_id" });
  Notification.belongsTo(Activity, {
    foreignKey: "activity_id",
  });

  Notification.findAll({
    where: {
      user_id: user.user_id,
    },
    include: [
      {
        model: Users,
        required: true,
        attributes: {
          exclude: [
            "password",
            "email",
            "phone",
            "token_fcm",
            "uid_phone",
            "token_app",
          ],
        },
      },
      {
        model: Activity,
        required: true,
        attributes: {
          exclude: [
            "password",
            "email",
            "phone",
            "token_fcm",
            "uid_phone",
            "token_app",
          ],
        },
      },
    ],
    order: [["notification_id", "DESC"]],
    offset: page * 10,
    limit: 10,
  })
    .then((dataNotification) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "List Notification",
        data: dataNotification,
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.clear_notification = async (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  Notification.destroy({
    where: {
      user_id: user.user_id,
    },
  })
    .then((dataNotification) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "Clear Notification",
        data: {},
      };
      res.send(response);
    })
    .catch((err) => {
      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};

exports.count_notification = async (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;

  let count_notification = await Notification.findAndCountAll({
    where: {
      user_id: user.user_id,
      read: false,
    },
  });

  let count_invitation = await Invitation.findAndCountAll({
    where: {
      invited_user_id: user.user_id,
    },
  });

  const response = {
    error: false,
    status_maintenance: status_maintenance,
    version: version,
    message: "Count Notification",
    data: {
      count_notification: count_notification.count,
      count_invitation: count_invitation.count,
    },
  };
  res.send(response);
};

exports.read_notification = async (req, res, next) => {
  let user = res.locals.user;
  let status_maintenance = res.locals.status_maintenance;
  let version = res.locals.version;
  let notification_id = req.body.notification_id;

  Notification.update(
    {
      read: true,
    },
    {
      where: {
        user_id: user.user_id,
        notification_id: notification_id,
      },
    }
  )
    .then((updateData) => {
      const response = {
        error: false,
        status_maintenance: status_maintenance,
        version: version,
        message: "Success Read",
        data: {},
      };
      res.send(response);
    })
    .catch((err) => {
      console.log(err);

      const response = {
        error: true,
        status_maintenance: status_maintenance,
        version: version,
        message: "Something Wrong",
        data: {},
      };
      res.status(400).send(response);
    });
};
