const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

const Notification = sequelize.define("notification", {
  notification_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false,
  },
  user_id: {
    type: Sequelize.BIGINT,
    defaultValue: 0,
  },
  other_user_id: {
    type: Sequelize.BIGINT,
    defaultValue: 0,
  },
  title: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  body: {
    type: Sequelize.TEXT,
    defaultValue: "",
  },
  read: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  type: {
    type: Sequelize.STRING,
    defaultValue: "",
  },
  activity_id: {
    type: Sequelize.BIGINT,
    defaultValue: 0,
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)"),
  },
});

module.exports = Notification;
