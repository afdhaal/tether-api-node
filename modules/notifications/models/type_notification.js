const Sequelize = require("sequelize");
const sequelize = require("../../../utils/database");

const TypeNotification = sequelize.define("type_notification", {
  type_notification_id: {
    type: Sequelize.BIGINT,
    autoIncrement: true,
    primaryKey: true,
    allowNull: false
  },
  type: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  title: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  body: {
    type: Sequelize.TEXT,
    defaultValue: ""
  },
  createdAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  },
  updatedAt: {
    allowNull: true,
    type: Sequelize.DATE,
    defaultValue: Sequelize.literal("CURRENT_TIMESTAMP(3)")
  }
});

module.exports = TypeNotification;
